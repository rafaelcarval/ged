"use strict";
var gedApp = angular.module('gedApp', ['angular-loading-bar', 'ui.router', 'ngAnimate', 'ngMask', 'ui.mask', 'angularFileUpload', 'ngTable', 'ngTableToCsv', 'ui.bootstrap.contextMenu']);
gedApp.config(function ($stateProvider, $urlRouterProvider, $locationProvider, cfpLoadingBarProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 70;
    $stateProvider
            .state('base', {
                url: "/ged/",
                views: {
                    'nav_bar': {
                        templateUrl: 'ged/view/partials/nav_bar.html',
                        controller: 'menuController'
                    },
                    'menu_lateral': {
                        templateUrl: 'ged/view/partials/menu_lateral.html',
                        controller: function ($state, $location, Sessao) {
                            var arr = $state.getCurrentPath();
                            var qdt = arr.length;
                            try {
                                Sessao.validarUrl(arr[qdt - 1].state.name);
                                if ($location.path() === '/') {
                                    $state.go('base.home');
                                }
                            } catch (e) {
                                $state.go('base.403');
                                console.log(e);
                            }
                        }
                    },
                    'palco': {
                        templateUrl: 'ged/view/partials/palco.html'
                    },
                    'footer': {
                        templateUrl: 'ged/view/partials/footer.html'
                    }
                }
            })
            .state('base.403', {
                url: "erro-403",
                templateUrl: "ged/view/partials/403.html",
                controller: 'home'
            })
            .state('base.home', {
                url: "home",
                templateUrl: "ged/view/listar/home.html",
                controller: 'home'
            })
            .state('base.cadastrar_empresa', {
                url: "cadastros/empresa/novo",
                templateUrl: "ged/view/add/add.html",
                controller: 'cadastroAtualizacaoEmpresaController'
            })
            .state('base.editar_empresa', {
                url: "cadastros/empresa/:acao/:id",
                templateUrl: "ged/view/add/add.html",
                controller: 'cadastroAtualizacaoEmpresaController'
            })
            .state('base.listar_empresa', {
                url: "cadastros/empresa/listar",
                templateUrl: "ged/view/listar/listar.html",
                controller: 'listarEmpresaController'
            })
            .state('base.editar_usuario', {
                url: "cadastros/usuario/:acao/:id",
                templateUrl: "ged/view/add/add.html",
                controller: 'cadastroAtualizacaoUsuaioController'
            })
            .state('base.novo_usuario', {
                url: "cadastros/usuario/novo",
                templateUrl: "ged/view/add/add.html",
                controller: 'cadastroAtualizacaoUsuaioController'
            })
            .state('base.listar_usuario', {
                url: "cadastros/usuario/listar",
                templateUrl: "ged/view/listar/listar.html",
                controller: 'listarUsuarioController'
            })
            .state('base.editar_departamento', {
                url: "cadastros/departamento/:acao/:id",
                templateUrl: "ged/view/add/add_list.html",
                controller: 'cadastroAtualizacaoDepartamentoController'
            })
            .state('base.novo_departamento', {
                url: "cadastros/departamento/novo",
                templateUrl: "ged/view/add/add_list.html",
                controller: 'cadastroAtualizacaoDepartamentoController'
            })
            .state('base.listar_tipoarquivo', {
                url: "cadastros/tipoarquivo/listar",
                templateUrl: "ged/view/listar/listar.html",
                controller: 'listarTipoArquivoController'
            })
            .state('base.editar_tipoarquivo', {
                url: "cadastros/tipoarquivo/:acao/:id",
                templateUrl: "ged/view/add/add_list.html",
                controller: 'cadastroAtualizacaoTipoArquivoController'
            })
            .state('base.novo_tipoarquivo', {
                url: "cadastros/tipoarquivo/novo",
                templateUrl: "ged/view/add/add_list.html",
                controller: 'cadastroAtualizacaoTipoArquivoController'
            })
            .state('base.raiz', {
                url: "arquivos/:codigoRaiz/:raiz",
                templateUrl: "ged/view/listar/listar-diretorio.html",
                controller: 'listarArquivos'
            })
            .state('base.sub', {
                url: "arquivos/:codigoRaiz/:raiz/:codigo1/:pasta1",
                templateUrl: "ged/view/listar/listar-diretorio.html",
                controller: 'listarArquivos'
            })
            .state('base.sub1', {
                url: "arquivos/:codigoRaiz/:raiz/:codigo1/:pasta1/:codigo2/:pasta2",
                templateUrl: "ged/view/listar/listar-diretorio.html",
                controller: 'listarArquivos'
            })
            .state('base.sub2', {
                url: "arquivos/:codigoRaiz/:raiz/:codigo1/:pasta1/:codigo2/:pasta2/:codigo3/:pasta3",
                templateUrl: "ged/view/listar/listar-diretorio.html",
                controller: 'listarArquivos'
            })
            .state('base.sub3', {
                url: "arquivos/:codigoRaiz/:raiz/:codigo1/:pasta1/:codigo2/:pasta2/:codigo3/:pasta3/:codigo4/:pasta4",
                templateUrl: "ged/view/listar/listar-diretorio.html",
                controller: 'listarArquivos'
            })
            .state('base.pesquisa', {
                url: "pesquisar/:pesquisa",
                templateUrl: "ged/view/listar/listar_new.html",
                controller: 'pesquisa'
            })
            .state('base.download', {
                url: "arquivos/download/:codigo1",
                templateUrl: "ged/view/partials/download.html",
                controller: 'listarArquivos'
            })
});
gedApp.filter('removeAccents', function removeAccents() {
    return function (source) {
        var accent = [
            /[\300-\306]/g, /[\340-\346]/g, // A, a
            /[\310-\313]/g, /[\350-\353]/g, // E, e
            /[\314-\317]/g, /[\354-\357]/g, // I, i
            /[\322-\330]/g, /[\362-\370]/g, // O, o
            /[\331-\334]/g, /[\371-\374]/g, // U, u
            /[\321]/g, /[\361]/g, // N, n
            /[\307]/g, /[\347]/g  // C, c
        ];
        var noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];
        for (var i = 0; i < accent.length; i++) {
            source = source.replace(accent[i], noaccent[i]);
        }
        return source;
    };
});
gedApp.filter('formatFileSize', function formatFileSize() {
    return function (bytes) {
        if (bytes == 0) {
            return '0 Bytes';
        }
        var k = 1000,
                dm = 2,
                sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
                i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    };
});