gedApp.directive('acessoMaster', function ($rootScope) {
    return {
        restrict: 'A',
        scope: {
            chamadaCallback: '&'
        },
        link: function (scope, element, attrs) {
            function botao() {
                element.attr('disabled', 'disabled');
                element.attr("ng-click");
                element.attr("ui-sref", "");
            }
            function link() {
                element.attr("href", "#");
                element.attr("ui-sref", "#");
                element.attr("ng-click", "#");
            }
            function editElement(element) {
                element.attr('disabled', 'disabled');
                if (Array.isArray(element) && element[0].localName === "a" && element.attr("href") !== "#") {
                    setTimeout(link, 200);
                } else if (element[0].localName === "button") {
                    setTimeout(botao, 200);
                }
                if (element.find('span').attr('class') === "fa fa-pencil" || element.find('span').attr('class') === "fa fa-close") {
                    element.find('span').attr("class", "block fa fa-lock");
                } else {
                    element.append('<span class="block fa fa-lock"></span>');
                }
            }
            var r = scope.chamadaCallback();
            var nivel = (r) ? 3 + r : 3;
            if ($rootScope.usuario.codtipousuario < nivel) {
                if (element[0].localName !== 'button' && element[0].localName !== 'a') {
                    element.find('a').remove();
                    element.append('<span class="block fa fa-lock"></span>');
                } else {
                    editElement(element);
                }

            }
        }
    };
});
gedApp.directive('acessoPresidente', function ($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            function link() {
                element.attr("href", "");
                element.attr("ui-sref", "");
            }
            function botao() {
                element.attr('disabled', 'disabled');
                element.attr("ng-click", "");
                element.attr("href", "");
                element.attr("ui-sref", "");
            }
            if ($rootScope.usuario.codtipousuario < 2) {
                element.attr('disabled', 'disabled');
                setTimeout(botao, 200);
                if (element[0].localName === "a") {
                    setTimeout(link, 300);
                }
                element.append('<span class="block fa fa-lock"></span>');
            }
        }
    };
});
gedApp.directive('permissaoPasta', function ($rootScope, $stateParams) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            function link() {
                element.attr("href", "");
                element.attr("ui-sref", "");
            }
            function botao() {
                element.attr('disabled', 'disabled');
                element.attr("ng-click", "");
                element.append('<span class="block fa fa-lock"></span>');
            }
            if (($stateParams.raiz.length > 1 || $stateParams.pasta1.length > 1) && (typeof $stateParams.pasta2 === 'undefined') && $rootScope.usuario.codtipousuario <= 2) {
                element.attr('disabled', 'disabled');
                setTimeout(botao, 200);
                if (element[0].localName === "div") {
                    element.remove();
                }
            }
        }
    };
});
gedApp.directive('removerPlus', function ($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if ($rootScope.usuario.codtipousuario <= 2) {
                element[0].remove();
            }
        }
    };
});
gedApp.directive('upArquivos', function () {
    return {
        restrict: 'A',
        scope: {'upArquivos': '='},
        link: function (scope, elm, attrs) {
            scope.$watch('upArquivos', function (nVal) {
                elm.val(nVal);
            });
            elm.bind('change', function () {
                console.log(document.getElementById('arquivos').files[0]);
                var numFiles = document.getElementById('arquivos').files.length;
                var label = document.getElementById('arquivos').files[0].name;
                informarNome(numFiles, label);
                function informarNome(numFiles, label) {
                    var input = document.getElementById('labelArquivos');
                    log = numFiles > 1 ? numFiles + ' Arquivos Selecionados' : label;
                    if (typeof input !== 'undefined') {
                        input.value = log;
                    } else {
                        if (log) {
                            alert(log);
                        }
                    }
                }
            });
        }
    };
});
gedApp.directive('limparArquivos', function () {
    return {
        restrict: 'A',
        scope: {'upArquivos': '='},
        link: function (scope, elm, attrs) {
            scope.$watch('upArquivos', function (nVal) {
                elm.val(nVal);
            });
            elm.bind('click', function () {
                var numFiles = 1;
                var label = "";
                informarNome(numFiles, label);
                function informarNome(numFiles, label) {
                    var input = document.getElementById('labelArquivos');
                    log = numFiles > 1 ? numFiles + ' Arquivos Selecionados' : label;
                    if (typeof input !== 'undefined') {
                        input.value = log;
                    } else {
                        if (log) {
                            console.log(log);
                        }
                    }
                }
            });
        }
    };
});
gedApp.directive('customClick', function ($state) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.$watch('customClick', function (nVal) {
                elm.val(nVal);
            });
            elm.bind('click', function () {
                $state.go(elm.attr('data-link'));
            });
        }
    };
});
gedApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
            if ((event.which === 27)) {
                element[0].value = "";
            }
        });
    };
});  