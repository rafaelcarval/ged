gedApp.factory('usuario', function ($http) {
    var classe = 'CrudRegistro';
    var table = 'ged_usuario';
    return{
        cadastrar: function (usuario) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'salvar', _p: usuario}
            });
        },
        getUsuario: function (codusuario) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'listar', _w: {codusuario: codusuario}}
            });
        },
        atualizar: function (usuario) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'atualizar', _p: usuario}
            });
        },
        listarVw: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_c: 'ListarUsuarios', _f: 'listar'}
            });
        },
        excluir: function (usuario) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'atualizar', _p: usuario}
            });
        },
        listarPreAprovacao: function (codempresa) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_c: classe, _t: 'ged_vw_usuario_pre', _f: 'listar', _w: {codempresa: codempresa}}
            });
        },
        listarDePreAprovacaoVw: function (codempresa) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_c: classe, _t: 'ged_vw_preaprovacao', _f: 'listar', _w: {codempresa: codempresa}}
            });
        }
    };
});
gedApp.factory('tipousuario', function ($http) {
    return{
        listar: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_vw_tipousuario', _c: 'CrudRegistro', _f: 'listar'}
            });
        }
    };
});
gedApp.factory('tipoArquivo', function ($http) {
    var table = 'ged_tipoarquivo';
    var classe = 'CrudRegistro';
    return{
        cadastrar: function (tipoaquivo) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'salvar', _p: tipoaquivo}
            });
        },
        cadastrar_preaprovacao: function (preaprovacao) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_preaprovacao', _c: classe, _f: 'salvar', _p: preaprovacao}
            });
        },
        atualizar: function (tipoaquivo) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'atualizar', _p: tipoaquivo}
            });
        },
        atualiza_preaprovacao: function (preaprovacao) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_preaprovacao', _c: classe, _f: 'atualizar', _p: preaprovacao}
            });
        },
        listarVw: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_vw_tipoarquivoempresa', _c: "ListarTipos", _f: 'listar'}
            });
        },
        getTipoAquivo: function (codtipoarquivo) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'listar', _w: {codtipoarquivo: codtipoarquivo}}
            });
        },
        listaVwAprovacao: function (codarquivo) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_vw_preaprovacao', _c: classe, _f: 'listar', _w: {codtipoarquivo: codarquivo}}
            });
        },
        listaAprovacao: function (codarquivo) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_preaprovacao', _c: classe, _f: 'listar', _w: {codtipoarquivo: codarquivo}}
            });
        },
        excluir: function (listaPreAprovacao) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_preaprovacao', _c: classe, _f: 'deletar', _p: listaPreAprovacao}
            });
        }
    };
});
gedApp.factory('departamento', function ($http) {
    var table = 'ged_departamento';
    var classe = 'CrudRegistro';
    return{
        cadastrar: function (departamento) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'salvar', _p: departamento}
            });
        },
        listarVw: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_vw_departamento', _c: 'ListarDepartamentos', _f: 'listar'}
            });
        },
        listar: function (codigo) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'listar', _w: {coddepartamento: codigo}}
            });
        },
        listarSelect: function (codempresa) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: "ged_vw_departamento_select", _c: classe, _f: 'listar', _w: {codempresa: codempresa}}
            });
        },
        atualizar: function (departamento) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'atualizar', _p: departamento}
            })
        },
        excluir: function (departamento) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'atualizar', _p: {coddepartamento: departamento, ativo: 0}}
            });
        }
    };
});
gedApp.factory('empresa', function ($http) {
    var table = 'ged_empresa';
    var classe = 'CrudRegistro';
    return{
        cadastrar: function (empresa) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_empresa', _c: 'CrudRegistro', _f: 'salvar', _p: empresa}
            });
        },
        atualizar: function (empresa) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'atualizar', _p: empresa}
            });
        },
        updateImagem: function (imagem) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_empresa', _c: 'CrudRegistro', _f: 'atualizar', _p: imagem}
            });
        },
        listar: function (whereEmpresa) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_empresa', _c: 'CrudRegistro', _f: 'listar', _w: whereEmpresa}
            });
        },
        listarVw: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_vw_empresa', _c: 'ListarEmpresas', _f: 'listar'}
            });
        },
        select: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: 'ged_vw_empresa_select', _c: 'CrudRegistro', _f: 'listar'}
            });
        },
        excluir: function (codigo) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_t: table, _c: classe, _f: 'atualizar', _p: {codempresa: codigo, status: 0}}
            });
        }
    };
});
gedApp.factory('cep', function ($http) {
    return{
        consultar: function (numeroCep) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_c: 'ConsultarCep', _f: 'getEndereco', _p: numeroCep}
            });
        },
        getEstados: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_c: 'CrudRegistro', _f: 'listar', _t: 'ged_vw_estado_select'}
            });
        },
        getCidades: function (uf) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_c: 'CrudRegistro', _f: 'listar', _t: 'ged_vw_cidade_select', _w: {uf: uf}}
            });
        }
    };

});
gedApp.factory('arvore', function ($http) {
    var table = 'ged_arvore';
    var classe = 'CrudRegistro';
    return{
        listar: function (arvoreConsulta) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                // data: {_c: classe, _f: 'listar', _t: 'ged_vw_arvore', _w:arvoreConsulta}
                data: {_c: 'Arvore', _f: 'listar', _p: arvoreConsulta}
            });
        },
        addPasta: function (arvoreCadastro) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                // data: {_c: classe, _f: 'listar', _t: 'ged_vw_arvore', _w:arvoreConsulta}
                data: {_c: classe, _t: table, _f: 'salvar', _p: arvoreCadastro}
            });
        }
    };
});
gedApp.factory('arquivo', function ($http, $rootScope, FileUploader) {
    return function () {
        return {
            upload: function (scope) {
                if (typeof this.objeto !== 'undefined') {
                    return this.objeto;
                } else {
                    this.objeto = scope.uploader = new FileUploader({
                        url: "ged/php/rest.php"
                    });
                    return  this.objeto;
                }
            },
            carregar: function (pasta, callback) {
                var complemento = '';
                if (typeof pasta === 'object') {
                    console.log(pasta);
                    complemento = 'get=' + JSON.stringify(pasta);
                } else {
                    complemento = "pasta=" + pasta;
                }
                for (x in this.objeto.queue) {
                    this.objeto.queue[x].url = "ged/php/rest.php?" + complemento;
                }
                this.upload(this.objeto).uploadAll();
                this.upload(this.objeto).onCompleteItem = function (fileItem, response, status, headers) {
                    callback(response);
                };
            },
            naoCarregados: function () {
                return this.upload(this.objeto).getNotUploadedItems().length;
            },
            erro: function () {
                this.upload.onErrorItem = function (fileItem, response, status, headers) {
                    console.info('onErrorItem', fileItem, response, status, headers);
                };
            },
            getRetorno: function () {
            },
            getQuantidade: function () {
                this.objeto.queue;
            },
            remover: function (nomeCompleto) {
                return $http({
                    url: "ged/php/rest.php",
                    method: 'POST',
                    data: {_c: 'Arquivo', _f: 'removerArquivo', _w: nomeCompleto}
                });
            },
            linkDownload: function (codigo) {
                return $http({
                    url: "ged/php/rest.php",
                    method: 'POST',
                    data: {_c: 'LinkDownload', _f: 'gerarLink', _w: codigo}
                });
            }
        };
    };
});
gedApp.factory('cnpj', function () {
    return{
        consultar: function (cnpj) {
            cnpj = cnpj.replace(/[^\d]+/g, '');
            if (cnpj == '') {
                return false;
            }
            if (cnpj.length != 14) {
                return false;
            }
            // Elimina CNPJs invalidos conhecidos
            if (cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222" || cnpj == "33333333333333" ||
                    cnpj == "44444444444444" || cnpj == "55555555555555" || cnpj == "66666666666666" ||
                    cnpj == "77777777777777" || cnpj == "88888888888888" || cnpj == "99999999999999") {
                return false;
            }
            tamanho = cnpj.length - 2
            numeros = cnpj.substring(0, tamanho);
            digitos = cnpj.substring(tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--) {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2) {
                    pos = 9;
                }
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0)) {
                return false;
            }
            tamanho = tamanho + 1;
            numeros = cnpj.substring(0, tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--) {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2) {
                    pos = 9;
                }
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1)) {
                return false;
            }
            return true;
        }
    };
});
gedApp.factory('dataCustom', function () {
    return{
        dataMysql: function () {
            var tzoffset = (new Date()).getTimezoneOffset() * 60000;
            var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1);
            return localISOTime.substring(0, 19).replace('T', ' ');
        }
    };
});
gedApp.factory('login', function ($http) {
    return {
        validarAcesso: function (login) {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_p: login, _f: 'setSession', _c: "Sessao"}
            });
        },
        getSession: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_f: 'getSession', _c: "Sessao"}
            });
        },
        unsetSession: function () {
            return $http({
                url: "ged/php/rest.php",
                method: 'POST',
                data: {_f: "unsetSession", _c: "Sessao"}
            });
        }
    };
});
gedApp.factory('Tabela', function ($compile) {
    return function (id, scope) {
        var botoes = [
            {
                text: 'Cópia',
                extend: 'copyHtml5',
                className: 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-xls ttip'
            },
            {
                text: 'CSV',
                extend: 'csvHtml5',
                className: 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-xls ttip',
                filename: 'export_csv'
            },
            {
                text: 'Excel',
                extend: 'excelHtml5',
                className: 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-xls ttip',
                filename: 'export_excel'
            },
            {
                text: 'PDF',
                extend: 'pdfHtml5',
                className: 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-xls ttip',
                filename: 'export_pdf',
                pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                exportOptions: {
                    columns: [0, 1, 2, 3]
                },
                customize: function (doc) {
                    doc.content.forEach(function (item) {
                        if (item.table) {
                            item.table.widths = [50, 120, 180, 150]
                        }
                    });
                    doc.defaultStyle.alignment = 'left';
                    doc.styles.tableHeader.alignment = 'left';
                    doc.defaultStyle.fontSize = 12;
                    doc.styles.tableHeader.fontSize = 10;
                    var objLayout = {};
                    objLayout['hLineWidth'] = function (i) {
                        return .5;
                    };
                    objLayout['vLineWidth'] = function (i) {
                        return .5;
                    };
                    objLayout['hLineColor'] = function (i) {
                        return '#aaa';
                    };
                    objLayout['vLineColor'] = function (i) {
                        return '#aaa';
                    };
                    objLayout['paddingLeft'] = function (i) {
                        return 4;
                    };
                    objLayout['paddingRight'] = function (i) {
                        return 4;
                    };
                    doc.content[0].layout = objLayout;
                }
            }];
        var language = {"sSearch": "Filtro: ",
            "sZeroRecords": "Nenhum registro encontrado",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior"
            }};
        var linhas;
        var colunas;
        var idTabela = id;
        if (typeof idTabela !== 'undefined') {
            if ($.fn.DataTable.isDataTable(idTabela)) {
                $(idTabela).DataTable().clear().destroy();
                $(idTabela).empty();
            }
        }
        return {
            setLinhas: function (linhasEntrada) {
                linhas = linhasEntrada;
            },
            setColunas: function (colunasEntrada) {
                colunas = colunasEntrada;
            },
            setIdTabela: function (id) {
                idTabela = id;
            },
            getTabela: function () {
                return  $(idTabela).dataTable({
                    "oLanguage": language,
                    "bLengthChange": false,
                    "bInfo": false,
                    "bAutoWidth": true,
                    data: linhas,
                    dom: 'Bfrtip',
                    buttons: botoes,
                    columns: colunas,
                    createdRow: function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())(scope);
                    }
                });
            }
        };
    };
});
