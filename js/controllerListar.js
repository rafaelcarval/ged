gedApp.controller('listarEmpresaController', function ($scope, empresa, $filter, ngTableParams, alerta) {
    $scope.titulo = "Listar";
    $scope.subTitulo = "Empresas ";
    $scope.tamanho = 12;
    $scope.classe = "listarEmpresa";
    $scope.editItem = "/ged/cadastros/empresa/editar/"; 
    $scope.btNovo = {link: "base.cadastrar_empresa", nome: "Novo"};
    var qdtZeros = "0000";
    var qdtItens = 10;
    $scope.tbody = [{}];
    $scope.getColumnName = function (column) {
        return column;
    };
    $scope.inicializarTBody = function () {
        var empresaOk = function (result) {
            if (typeof result.msg == 'undefined') {
                $scope.tbody = result;
                if (Array.isArray(result)) {
                    $scope.tbody = result;
                } else {
                    $scope.tbody = [result];
                }
                for (x in  $scope.tbody) {
                    $scope.tbody[x].codempresa = (qdtZeros + $scope.tbody[x].codempresa).slice(-4);
                    $scope.tbody[x].editar = $scope.tbody[x].codempresa;
                    $scope.tbody[x].excluir = $scope.tbody[x].codempresa;
                }
                $scope.colunas = Object.keys($scope.tbody[0]);
                var auxiliar = [];
                angular.forEach($scope.colunas, function (value, key) {
                    if (value != 'editar' && value != 'excluir') {
                        auxiliar.push({title: value, field: value, filter: {value: 'text'}, show: true, sortable: value});
                    } else {
                        auxiliar.push({title: value, field: value, show: true});
                    }
                });
                $scope.cols = auxiliar;
                $scope.ParamTable = new ngTableParams({
                    page: 1,
                    count: qdtItens,
                }, {
                    counts: ($scope.tbody.length > qdtItens ? [qdtItens, $scope.tbody.length] : []),
                    total: $scope.tbody.length,
                    getData: function ($defer, params) {
                        $scope.data = params.sorting() ? $filter('orderBy')($scope.tbody, params.orderBy()) : $scope.tbody;
                        $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        $defer.resolve($scope.data);
                    }
                });
            }
        }
        empresa.listarVw().success(empresaOk);
    };
    $scope.callFunction = function (funcao) {
        if (typeof funcao !== 'undefined' && typeof $scope[funcao] === 'function') {
            $scope[funcao]();
        }
    };
    $scope.excluir = function (codigo) {
        alerta.confirm("Deseja remover esta empresa? ", function (result) {
            if (result) {
                empresa.excluir(codigo).success(function () {
                    if (typeof result.error === 'undefined') {
                        alerta.show("Empresa removida com sucesso.", 'success', 2000);
                        $scope.inicializarTBody();
                    } else {
                        alerta.show("Falha ao remover o departamento. Erro: " + result.error, 'error', 2000);
                        $scope.inicializarTBody();
                    }
                });
            }
        });
    };
});
gedApp.controller('listarUsuarioController', function ($scope, $rootScope, dataCustom, usuario, $filter, ngTableParams, alerta) {
    $scope.titulo = "Listar";
    $scope.subTitulo = "Usuário";
    $scope.tamanho = 12;
    $scope.classe = "listarUsuario";
    $scope.editItem = "/ged/cadastros/usuario/editar/";
    $scope.btNovo = {link: "base.novo_usuario", nome: "Novo"};
    var qdtZeros = "0000";
    var qdtItens = 10;
    $scope.tbody = [{}];
    $scope.getColumnName = function (column) {
        return column;
    };
    $scope.inicializarTBody = function () {
        var empresaOk = function (result) {
            if (typeof result.msg === 'undefined') {
                if (Array.isArray(result)) {
                    $scope.tbody = result;
                } else {
                    $scope.tbody = [result];
                }
                for (var x in  $scope.tbody) {
                    $scope.tbody[x].codusuario = (qdtZeros + $scope.tbody[x].codusuario).slice(-4);
                    $scope.tbody[x].editar = $scope.tbody[x].codusuario;
                    $scope.tbody[x].excluir = $scope.tbody[x].codusuario;
                }
                $scope.colunas = Object.keys($scope.tbody[0]);
                var auxiliar = [];
                angular.forEach($scope.colunas, function (value, key) {
                    if (value !== 'editar' && value !== 'excluir') {
                        auxiliar.push({title: value, field: value, filter: {value: 'text'}, show: true, sortable: value});
                    } else {
                        auxiliar.push({title: value, field: value, show: true});
                    }
                });
                $scope.cols = auxiliar;
                $scope.ParamTable = new ngTableParams({
                    page: 1,
                    count: qdtItens
                }, {
                    counts: ($scope.tbody.length > qdtItens ? [qdtItens, $scope.tbody.length] : []),
                    total: $scope.tbody.length,
                    getData: function ($defer, params) {
                        $scope.data = params.sorting() ? $filter('orderBy')($scope.tbody, params.orderBy()) : $scope.tbody;
                        $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        $defer.resolve($scope.data);
                    }
                });
            }
        };
        usuario.listarVw().success(empresaOk);
    };
    $scope.callFunction = function (funcao) {
        if (typeof funcao !== 'undefined' && typeof $scope[funcao] === 'function') {
            $scope[funcao]();
        }
    };
    console.log($rootScope.usuario);
    $scope.excluir = function (codigo) {
        alerta.confirm("Deseja remover o item? ", function (result) {
            if (result) {
                var usuarioE = {codusuario: codigo, dtalteracao: dataCustom.dataMysql(), codusuariocadastro: $rootScope.usuario.codusuario, ativo: 0};
                usuario.excluir(usuarioE).success(function (result) {
                    if (typeof result.error === 'undefined') {
                        alerta.show("Usuario removido com sucesso.", 'success', 2000);
                        $scope.inicializarTBody();
                    } else {
                        alerta.show("Falha ao remover o usuario. Erro: " + result.error, 'error', 2000);
                        $scope.inicializarTBody();
                    }
                });
            }
        });
    };
});
gedApp.controller('listarTipoArquivoController', function ($scope, $rootScope, tipoArquivo, $filter, ngTableParams, alerta, crud) {
    $scope.titulo = "Listar";
    $scope.subTitulo = "Tipo de Arquivo";
    $scope.tamanho = 12;
    $scope.classe = "listarTipoArquivo";
    $scope.editItem = "/ged/cadastros/tipoarquivo/editar/";
    $scope.btNovo = {link: "base.novo_tipoarquivo", nome: "Novo"};
    var qdtZeros = "0000";
    var qdtItens = 10;
    $scope.tbody = [{}];
    $scope.getColumnName = function (column) {
        return column;
    };
    $scope.inicializarTBody = function () {
        var empresaOk = function (result) {
            if (typeof result.msg == 'undefined') {
                if (Array.isArray(result)) {
                    $scope.tbody = result;
                } else {
                    $scope.tbody = [result];
                }
                for (var x in  $scope.tbody) {
                    $scope.tbody[x].codtipoarquivo = (qdtZeros + $scope.tbody[x].codtipoarquivo).slice(-4);
                    $scope.tbody[x].editar = $scope.tbody[x].codtipoarquivo;
                    $scope.tbody[x].excluir = $scope.tbody[x].codtipoarquivo;
                }
                $scope.colunas = Object.keys($scope.tbody[0]);
                var auxiliar = [];
                angular.forEach($scope.colunas, function (value, key) {
                    if (value != 'editar' && value != 'excluir') {
                        auxiliar.push({title: value, field: value, filter: {'value': 'text'}, show: true, sortable: value});
                    } else {
                        auxiliar.push({title: value, field: value, show: true});
                    }
                });
                $scope.cols = auxiliar;
                $scope.ParamTable = new ngTableParams({
                    page: 1,
                    count: qdtItens,
                }, {
                    counts: ($scope.tbody.length > qdtItens ? [qdtItens, $scope.tbody.length] : []),
                    total: $scope.tbody.length,
                    getData: function ($defer, params) {
                        $scope.data = params.sorting() ? $filter('orderBy')($scope.tbody, params.orderBy()) : $scope.tbody;
                        $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        $defer.resolve($scope.data);
                    }
                });
            }
        };
        tipoArquivo.listarVw().success(empresaOk);
    };
    $scope.callFunction = function (funcao) {
        if (typeof funcao != 'undefined' && typeof $scope[funcao] == 'function') {
            $scope[funcao]();
        }
    };
    console.log($rootScope.usuario);
    $scope.excluir = function (id) {
        alerta.confirm('Deseja excluir este tipo de arquivo?', function (result) {
            if (result) {
                crud.update('ged_tipoarquivo', {codtipoarquivo: id, ativo: 0}).success(function (result) {
                    if (typeof result.error === 'undefined') {
                        alerta.show('Tipo de arquivo excluído com sucesso.', 'success', 2000);
                        $scope.inicializarTBody();
                    } else {
                        alerta.show(result.error, 'error', 2000);
                    }
                });
            } else {
                alerta.show('Nenhuma ação aplicada.', 'info', 2000);
            }
        });
    };
});
gedApp.controller('home', function ($scope, $rootScope, dataCustom, tipoArquivo, $filter, ngTableParams, alerta, crud, Tabela, $compile) {
    $scope.titulo = "Arquivos";
    $scope.subTitulo = "Aprovar";
    $scope.tamanho = 12;
    $scope.classe = "listarTipoArquivo";
    $scope.editItem = "/ged/cadastros/tipoarquivo/editar/";
    $scope.btNovo = {link: "/ged/cadastros/tipoarquivo/novo", nome: "Novo"};
    var qdtZeros = "0000";
    var qdtItens = 10;
    $scope.tbody = [{}];
    $scope.qtd = {};
    $scope.getColumnName = function (column) {
        return column;
    };
    $scope.inicializaContatores = function () {
        var andamento = function (result) {
            if (!result.msg) {
                $scope.qtd.andamento = result.quantidade;
            } else {
                $scope.qtd.andamento = 0;
            }
        };
        var reprovado = function (result) {
            if (!result.msg) {
                $scope.qtd.reprovados = result.quantidade;
            } else {
                $scope.qtd.reprovados = 0;
            }
        };
        var aprovado = function (result) {
            if (!result.msg) {
                $scope.qtd.aprovados = result.quantidade;
            } else {
                $scope.qtd.aprovados = 0;
            }
        };
        crud.listar('ged_vw_arquivo_quantidade', {codusuario: $rootScope.usuario.codusuario, codstatusarquivo: '1'}).success(andamento);
        crud.listar('ged_vw_arquivo_quantidade', {codusuario: $rootScope.usuario.codusuario, codstatusarquivo: '3'}).success(aprovado);
        crud.listar('ged_vw_arquivo_quantidade', {codusuario: $rootScope.usuario.codusuario, codstatusarquivo: '2'}).success(reprovado);
        crud.listar('ged_vw_espacoconsumido', {codempresa: $rootScope.usuario.codempresa}).success(function (result) {
            if (typeof result.error === 'undefined') {
                $scope.qtd.espacoconsumido = $scope.converterUnidade(result.tamanho);
            }
        });
    };
    $scope.converterUnidade = function (qdt) {
        var base = 1024 * 1024 * 1024;
        var calc = qdt / base;
        for (var x = 0; x < 3; x++) {
            if (calc >= 1) {
                var p = x;
                switch (p) {
                    case 0:
                        var unidade = "GB";
                        break;
                    case 1:
                        var unidade = "MB";
                        break;
                    case 2:
                        var unidade = "KB";
                        break;
                }
                return (calc.toFixed(3).replace('.', ',') + " " + unidade);
            }
            calc = calc * 1024;
            if (x === 3) {
                return ("0 kb");
            }
        }
        console.log(calc);
    };
    $scope.inicializarTBody = function (tipo) {
        var empresaOk = function (result) {
            if (typeof result.msg === 'undefined') {
                $scope.tbody = (Array.isArray(result)) ? result : [result];
                var columns = [
                    {title: "cod.", data: "codarquivo"},
                    {title: "data cadastro", data: "dtcadastro"},
                    {title: "descrição", data: "fantasia"},
                    {
                        title: "Arquivo",
                        data: "nome",
                        render: function (data, type, full, meta) {
                            return '<a href="" target="_self" class="remove">' + data + '</a>';
                        }
                    }
                ];
                if (tipo == 'andamento') {
                    columns.push(
                            {
                                title: "Aprovado",
                                data: null,
                                //defaultContent:'<a  href=""  class="edit"><span class="fa fa-pencil"></span></a>',                                
                                orderable: false,
                                render: function (data, type, full, meta) {
                                    return '<a href="#"  ng-click="aprovarArquivo(\'' + full.codarquivo + '\');" class="edit"><span class="fa fa-pencil"></span></a>';
                                }
                            },
                            {
                                title: "Reprovado",
                                data: "codarquivo",
                                //defaultContent:'<a href="#" class="remove"><span class="fa fa-close"></span></a>',
                                orderable: false,
                                render: function (data, type, full, meta) {
                                    return '<a href="#" ng-click="reprovarArquivo(\'' + full.codarquivo + '\');" class="remove"><span class="fa fa-close"></span></a>';
                                }
                            });
                }
                var table = new Tabela("#datatable", $scope);
                table.setLinhas($scope.tbody);
                table.setColunas(columns);
                table.getTabela();
            } else {
                $scope.tbody = [];
            }
        };
        switch (tipo) {
            case 'andamento':
                crud.listar('ged_vw_aqruivo_apr', {codusuario: $rootScope.usuario.codusuario, codstatusarquivo: '1'}).success(empresaOk);
                break;
            case 'aprovado':
                crud.listar('ged_vw_aqruivo_apr', {codusuario: $rootScope.usuario.codusuario, codstatusarquivo: '3'}).success(empresaOk);
                break;
            case 'reprovado':
                crud.listar('ged_vw_aqruivo_apr', {codusuario: $rootScope.usuario.codusuario, codstatusarquivo: '2'}).success(empresaOk);
                break;
            default:
                break;
        }
    };
    $scope.aprovarArquivo = function (id) {
        alerta.confirm('Dese aprovar esse aquivo?', function (result) {
            var obj = {codarquivo: id, codusuario: $rootScope.usuario.codusuario};
            var cond = {codarquivo: id, codusuario: $rootScope.usuario.codusuario};
            if (result) {
                crud.executar('AprovacaoArquivo', 'aprovar', {codarquivo: id}).success(function (result) {
                    if (typeof result.error === 'undefined') {
                        alerta.show("Aprovação pessoal ocorrida com sucesso!", 'success', 2000, 5);
                        $scope.inicializaContatores();
                        $scope.inicializarTBody('andamento');
                    } else {
                        alerta.show("Falha ao aprovar. Erro: " + result.error, 2000, 5);
                    }
                });
                alerta.input('Você pode ou não informar uma descrição.', {ok: 'Comentar'}, function (result) {
                    if (result && result.length >= 0) {
                        var com = cond;
                        com.comentario = result;
                        crud.insert('ged_comentarioaprovacao', com).success(function (result) {
                            if (typeof result.error === 'undefined') {
                                alerta.show("Comentário inserido com sucesso!", "success", 2000);
                            }
                        });
                    }
                }, 2);
            } else {
                alerta.show("Nenhuma ação aplicada.", "info", 2000);
            }
        });
    };
    $scope.reprovarArquivo = function (id) {
        alerta.confirm('Dese reprovar esse aquivo?', function (result) {
            var cond = {codarquivo: id, codusuario: $rootScope.usuario.codusuario};
            if (result) {
                alerta.input('Você deve informar uma descrição.', {ok: 'Comentar'}, function (result) {
                    if (result && result.length >= 15) {
                        var com = cond;
                        com.comentario = result;
                        crud.insert('ged_comentarioaprovacao', com).success(function (result) {
                            if (typeof result.error === 'undefined') {
                                alerta.show("Comentário inserido com sucesso!", "success", 2000);
                                crud.executar('AprovacaoArquivo', 'reprovar', {codarquivo: id}).success(function (result) {
                                    if (typeof result.error === 'undefined') {
                                        alerta.show("Arquivo reprovado com sucesso!", 'success', 2000, 5);
                                        $scope.inicializaContatores();
                                        $scope.inicializarTBody('andamento');
                                    } else {
                                        alerta.show("Falha ao reprovar. Erro: " + result.error, 2000, 5);
                                    }
                                });
                            }
                        });
                    } else if (result && result.length < 15) {
                        alerta.show("Sua descrição deve ter ao menos 15 caracteres.", "error", 2000);
                    } else {
                        alerta.show("Nenhuma ação aplicada.", "info", 2000);
                    }
                }, 2);
            } else {
                alerta.show("Nenhuma ação aplicada.", "info", 2000);
            }
        });
    };
    $scope.callFunction = function (funcao) {
        if (typeof funcao !== 'undefined' && typeof $scope[funcao] === 'function') {
            $scope[funcao]();
        }

    };
    $scope.excluir = function (codigo) {
        /* alerta.confirm("Deseja remover o item? ", function (result) {
         if(result){
         var usuarioE={codusuario:codigo, dtalteracao: dataCustom.dataMysql(),codusuariocadastro:$rootScope.usuario.codusuario ,ativo:0};
         usuario.excluir(usuarioE).success(function(result){
         if (typeof result.error === 'undefined') {
         alerta.show("Usuario removido com sucesso.", 'success', 2000);
         $scope.inicializarTBody();
         }else{
         alerta.show("Falha ao remover o usuario. Erro: "+result.error, 'error', 2000);
         $scope.inicializarTBody();
         }
         });
         }
         });*/
    };
});