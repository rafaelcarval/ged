gedApp.service("alerta", function () {
    this.show = function (message, color, time = 1500, position) {
        position = (position) ? position : 2;
        toastr.options = {
            "newestOnTop": true,
            "timeOut": time,
            "tapToDismiss": true
        };
        switch (position) {
            case 1:
                toastr.options.positionClass = "toast-top-left";
                break;
            case 2:
                toastr.options.positionClass = "toast-top-right";
                break;
            case 3:
                toastr.options.positionClass = "toast-bottom-right";
                break;
            case 4:
                toastr.options.positionClass = "toast-bottom-left";
                break;
            case 5:
                toastr.options.positionClass = "toast-top-center";
                break;
            case 6:
                toastr.options.positionClass = "toast-bottom-center";
                break;
            case 7:
                toastr.options.positionClass = "toast-top-full-width";
                break;
            case 8:
                toastr.options.positionClass = "toast-bottom-full-width";
                break;
            case 9:
                toastr.options.positionClass = "toast-center-screen";
                break;
        }
        switch (color) {
            case 'success':
                toastr.success(message);
                break;
            case 'warning':
                toastr.warning(message);
                break;
            case 'error':
                toastr.error(message);
                break;
            default:
                toastr.info(message);
                break;
    }
    },
            /**
             * @description Janela suspensa para confirmação
             * @param {string} mensagem
             * @param {function} f
             * @param {int} tamanho
             * @returns {boolean}
             */
            this.confirm = function (mensagem, f, tamanho) {
                switch (tamanho) {
                    case 1:
                        var t = 'samll';
                        break;
                    case 2:
                        var t = 'large';
                        break;
                    default:
                        var t = 'small';
                }
                bootbox.confirm({
                    message: mensagem,
                    size: t,
                    className: 'alertaConfirm',
                    buttons: {
                        confirm: {
                            label: 'Sim',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Não',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        f(result);
                    }
                });
            },
            this.input = function (mensagem, botao, f, tipo, tamanho) {
                if (typeof botao.cancel === 'undefined') {
                    botao.cancel = 'Cancelar';
                }
                switch (tipo) {
                    case 1:
                        var tp = 'text';
                        break;
                    case 2:
                        var tp = 'textarea';
                        break;
                    default:
                        var tp = 'text';
                        break;
                }
                switch (tamanho) {
                    case 1:
                        var t = 'small';
                        break;
                    case 2:
                        var t = 'medium';
                        break;
                    case 3:
                        var t = 'large';
                        break;
                    default:
                        var t = 'small';
                        break;
                }
                bootbox.prompt({
                    title: mensagem,
                    size: t,
                    className: 'alertaConfirm',
                    inputType: tp,
                    maxlength: 50,
                    buttons: {
                        confirm: {
                            label: botao.ok,
                            className: 'btn-success'
                        },
                        cancel: {
                            label: botao.cancel,
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        f(result);
                    }
                });
            };
});
gedApp.service("Sessao", function ($http, $rootScope, login, $stateParams) {
    var classe = 'Sessao';
    var url = 'ged/php/rest.php';
    this.validar = function () {
        login.getSession().success(function (result) {
            if (typeof result.error === 'undefined') {
                $rootScope.usuario = result;
                $rootScope.loginRoot = true;
            }
        });
    },
            this.delItensUpload = function (objetoLimpar) {
                return $http({
                    url: url,
                    method: 'POST',
                    data: {_c: classe, _f: 'delItemSession', _w: objetoLimpar}
                });
            },
            this.validarUrl = function (base) {
                var arrMaster = ['base.cadastrar_empresa'];
                var arrPresidente = [
                    'base.cadastrar_empresa',
                    'base.editar_usuario',
                    'base.listar_usuario',
                    'base.editar_departamento',
                    'base.listar_tipoarquivo',
                    'base.editar_tipoarquivo',
                    'base.novo_tipoarquivo'];
                var arrComum = [
                    'base.cadastrar_empresa',
                    'base.editar_empresa',
                    'base.listar_empresa',
                    'base.editar_usuario',
                    'base.novo_usuario',
                    'base.listar_usuario',
                    'base.editar_departamento',
                    'base.novo_departamento',
                    'base.listar_tipoarquivo',
                    'base.editar_tipoarquivo',
                    'base.novo_tipoarquivo'];
                var arrBase = [arrComum, arrPresidente, arrMaster];
                var pos = $rootScope.usuario.codtipousuario - 1;
                function validarBase(array, base) {
                    for (x in array) {
                        if (array[x] === base) {
                            throw "Você não tem permissão para acessar essa área do site";
                        }
                    }
                    return true;
                }
                return validarBase(arrBase[pos], base);
            },
            this.permissao = function () {
                if ($rootScope.usuario.codtipousuario >= 3) {
                    return null;
                } else if ($stateParams.pasta2 || $stateParams.pasta3 || $stateParams.pasta4) {
                    return null;
                } else {
                    return function () {};
                }
            };
});
gedApp.service('Slugify', function () {
    this.palavra = function (palavra) {
		
		var res = palavra.replace(/ /gi, function (x) {
			return x.replace(' ', '-');
		});		
		
		return res.toLowerCase();
		/*
		console.log('velha:'+palavra);
        //var com_acento = unescape(decodeURIComponent('áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇAEIOUC BDFGHJKLMNPQRSTVXYZW'))
        //var sem_acento = 'aaaaaeeeeiiiiooooouuuucaaaaaeeeeiiiiooooouuuucaeiouc-bdfghjklmnpqrstvxyzw';
		
        var nova = '';
        for (i = 0; i < palavra.length; i++) {
            if (com_acento.search(palavra.substr(i, 1)) >= 0) {
				console.log(sem_acento.substr(com_acento.search(palavra.substr(i, 1)), 1));
                nova += sem_acento.substr(com_acento.search(palavra.substr(i, 1)), 1);
            } else {
                nova += palavra.substr(i, 1);
            }
        }
		console.log('nova:'+nova);
        return nova;*/
    };
});
gedApp.service('Estrutura', function ($rootScope, $stateParams) {
    this.get = function () {
        delete $rootScope.estruturaPasta;
        $rootScope.base = $stateParams.codigoRaiz;
		console.log('state:');
		console.log($stateParams);
        if ($stateParams.pasta4) {
            $rootScope.estruturaPasta = [];
            $rootScope.estruturaPasta = [
                {pasta: $stateParams.raiz, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz}, type: 0},
                {pasta: $stateParams.pasta1, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1}, type: 1},
                {pasta: $stateParams.pasta2, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1, pasta2: $stateParams.pasta2, codigo2: $stateParams.codigo2}, type: 2},
                {pasta: $stateParams.pasta3, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1, pasta2: $stateParams.pasta2, codigo2: $stateParams.codigo2, pasta3: $stateParams.pasta3, codigo3: $stateParams.codigo3}, type: 3},
                {pasta: $stateParams.pasta4, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1, pasta2: $stateParams.pasta2, codigo2: $stateParams.codigo2, pasta3: $stateParams.pasta3, codigo3: $stateParams.codigo3, pasta4: $stateParams.pasta4, codigo4: $stateParams.codigo4}, type: 4}
            ];
            $rootScope.codpai = $stateParams.codigo4;
            $rootScope.url = $stateParams.codigoRaiz + '/' + $stateParams.raiz + '/' + $stateParams.codigo1 + '/' + $stateParams.pasta1 + '/' + $stateParams.codigo2 + '/' + $stateParams.pasta2 + '/' + $stateParams.codigo3 + '/' + $stateParams.pasta3 + '/' + $stateParams.codigo4 + '/' + $stateParams.pasta4;
        } else if ($stateParams.pasta3 && !$stateParams.pasta4) {
            $rootScope.estruturaPasta = [];
            $rootScope.estruturaPasta = [
                {pasta: $stateParams.raiz, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz}, type: 0},
                {pasta: $stateParams.pasta1, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1}, type: 1},
                {pasta: $stateParams.pasta2, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1, pasta2: $stateParams.pasta2, codigo2: $stateParams.codigo2}, type: 2},
                {pasta: $stateParams.pasta3, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1, pasta2: $stateParams.pasta2, codigo2: $stateParams.codigo2, pasta3: $stateParams.pasta3, codigo3: $stateParams.codigo3}, type: 3}
            ];
            $rootScope.codpai = $stateParams.codigo3;
            $rootScope.url = $stateParams.codigoRaiz + '/' + $stateParams.raiz + '/' + $stateParams.codigo1 + '/' + $stateParams.pasta1 + '/' + $stateParams.codigo2 + '/' + $stateParams.pasta2 + '/' + $stateParams.codigo3 + '/' + $stateParams.pasta3;
        } else if ($stateParams.pasta2 && !$stateParams.pasta3 && !$stateParams.pasta4) {
            $rootScope.estruturaPasta = [];
            $rootScope.estruturaPasta = [
                {pasta: $stateParams.raiz, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz}, type: 0},
                {pasta: $stateParams.pasta1, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1}, type: 1},
                {pasta: $stateParams.pasta2, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1, pasta2: $stateParams.pasta2, codigo2: $stateParams.codigo2}, type: 2}
            ];
            $rootScope.codpai = $stateParams.codigo2;
            $rootScope.url = $stateParams.codigoRaiz + '/' + $stateParams.raiz + '/' + $stateParams.codigo1 + '/' + $stateParams.pasta1 + '/' + $stateParams.codigo2 + '/' + $stateParams.pasta2;
        } else if ($stateParams.pasta1 && !$stateParams.pasta2 && !$stateParams.pasta3 && !$stateParams.pasta4) {
            $rootScope.estruturaPasta = [];
            $rootScope.estruturaPasta = [
                {pasta: $stateParams.raiz, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz}, type: 0},
                {pasta: $stateParams.pasta1, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz, pasta1: $stateParams.pasta1, codigo1: $stateParams.codigo1}, type: 1}
            ];
            $rootScope.codpai = $stateParams.codigo1;
            $rootScope.url = $stateParams.codigoRaiz + '/' + $stateParams.raiz + '/' + $stateParams.codigo1 + '/' + $stateParams.pasta1;
        } else {
            $rootScope.estruturaPasta = [];
            $rootScope.estruturaPasta = [
                {pasta: $stateParams.raiz, link: {codigoRaiz: $stateParams.codigoRaiz, raiz: $stateParams.raiz}, type: 0}
            ];
            $rootScope.codpai = $stateParams.codigoRaiz;
            $rootScope.url = $stateParams.codigoRaiz + '/' + $stateParams.raiz;
        }
    };
});
gedApp.service('InicializarArvore', function ($rootScope, arvore, InicializarMenu, crud) {
    this.start = function (cod) {
        var raiz = ($rootScope.usuario.empresaMaster) ? 0 : $rootScope.usuario.codarvore;
        var arvoreC = {codpai: raiz};
        function startCompartilhado(f) {
            crud.listar('ged_vw_arquivocompartilhado', {codusuario: $rootScope.usuario.codusuario}).success(function (result) {
                if (typeof result.error === 'undefined' && typeof result.msg === 'undefined') {
                    return f(result);
                } else {
                    return f(false);
                    console.log(result.error + result.msg);
                }
            });
        }
        arvore.listar(arvoreC).success(function (result) {
            var retorno = (Array.isArray(result)) ? result : [result];
            startCompartilhado(function (result) {
                if (result) {
                    for (x in retorno) {
                        if (typeof retorno[x].itens !== 'undefined') {
                            if (retorno[x].codempresa === $rootScope.usuario.codempresa) {
                                retorno[x].itens.push({codarvore: "0", codpai: "0", codempresa: $rootScope.usuario.codempresa, titulo: "Compartilhados"});
                            }
                        }
                    }
                }
                if (!$rootScope.menu['2']) {
                    $rootScope.menu.push({titulo: 'Arquivos', icone: 'fa-folder', itens: retorno});
                    InicializarMenu.start();
                } else {
                    $rootScope.menu['2'].itens = retorno;
                    InicializarMenu.start();
                }
            });
        });
    };
});
gedApp.service('crud', function ($http) {
    var classe = 'CrudRegistro';
    var url = "ged/php/rest.php";
    var method = "POST";
    this.listar = function (tabela, where, dif) {
        where = (typeof where === 'object') ? where : null;
        dif = (dif) ? true : null;
        return $http({
            url: url,
            method: method,
            data: {_c: classe, _f: 'listar', _t: tabela, _w: where, _p: dif}
        });
    },
    this.update = function (tabela, listaObjetos, multWhere) {
        return $http({
            url: url,
            method: method,
            data: {_t: tabela, _c: classe, _f: 'atualizar', _p: listaObjetos, _w: multWhere}
        });
    },
    this.delete = function (tabela, deleteObjetos) {
        return $http({
            url: url,
            method: method,
            data: {_t: tabela, _c: classe, _f: 'deletar', _p: deleteObjetos}
        });
    },
    this.insert = function (tabela, insertObjetos) {
        return $http({
            url: url,
            method: method,
            data: {_t: tabela, _c: classe, _f: 'salvar', _p: insertObjetos}
        });
    },
    this.executar = function (classe, funcao, objeto, parametroMetodo) {
        return $http({
            url: url,
            method: 'POST',
            data: {_c: classe, _f: funcao, _p: objeto, _w: parametroMetodo}
        });
    };
});
gedApp.service('InicializarMenu', function ($rootScope) {
    $rootScope.itemMenu = (Array.isArray($rootScope.itemMenu)) ? $rootScope.itemMenu : [];
    this.start = function () {
        for (var x = 0; x < $rootScope.menu.length; x++) {
            $rootScope.itemMenu.push({ativo: false});
            for (var t = 0; t < $rootScope.menu[x].itens.length; t++) {
                if (typeof $rootScope.menu[x].itens[t].itens !== 'undefined') {
                    $rootScope.itemMenu[x][t] = (typeof $rootScope.itemMenu[x][t] === 'undefined') ? {ativo: false} : $rootScope.itemMenu[x][t];
                    for (var y = 0; y < $rootScope.menu[x].itens[t].itens.length; y++) {
                        $rootScope.itemMenu[x][t][y] = (typeof $rootScope.itemMenu[x][t][y] === 'undefined') ? {ativo: false} : $rootScope.itemMenu[x][t][y];
                    }
                }
            }
        }
    }
});
gedApp.service('Icone', function () {
    var tipo = [
        {ext: ['pdf'], icone: 'fa fa-file-pdf-o'},
        {ext: ['png', 'jpg', 'gif', 'jpeg', 'bmp'], icone: 'fa fa-file-image-o'},
        {ext: ['doc', 'docx', 'odt'], icone: 'fa fa-file-word-o'},
        {ext: ['xls', 'xlsx', 'ods'], icone: 'fa fa-file-excel-o'},
        {ext: ['raz', 'zip', '7zip'], icone: 'fa fa-file-archive-o'},
        {ext: ['txt'], icone: 'fa fa-file-text-o'},
        {ext: ['wmv', 'avi', '3gp', 'mov', 'mp4', 'rmvb', 'mkv'], icone: 'fa fa-file-video-o'}
    ];
    var def = 'fa fa-file-o';
    this.get = function (nome) {
        var ext = nome.split(".")[1];
        for (x in tipo) {
            for (y in tipo[x].ext) {
                if (tipo[x].ext[y] === ext) {
                    return tipo[x].icone;
                }
            }
        }
        return def;
    }
});
gedApp.service('Helpers', function () {
    this.manterPosicoes = function (lista, manter) {
        var nLista = [];
        for (x in lista) {
            if (Array.isArray(manter)) {
                var obj = {};
                for (y in manter) {
                    obj[manter[y]] = lista[x][manter[y]];
                }
                nLista.push(obj);
            } else {
                nLista.push({[manter]: lista[x][manter]});
            }
        }
        return nLista;
    }
});
gedApp.service('Validar', function (alerta) {
    validarArrayObjeto = function (arrayObjeto, atributo, valor) {
        if (typeof arrayObjeto !== 'undefined' && typeof atributo !== 'undefined' && typeof valor !== 'undefined') {
            if (Array.isArray(arrayObjeto)) {
                for (var x in arrayObjeto) {
                    if (arrayObjeto[x][atributo] === valor) {
                        throw "Existem valores inválidos, favor checar os dados informados.";
                    }
                }
            } else {
                if (arrayObjeto[atributo] === valor) {
                    throw "Existem valores inválidos, favor checar os dados informados.";
                }
            }
        } else {
            console.log('parametros inválidos');
            return;
        }
    };
    this.form = function (idForm, f) {
        var form = document.getElementById(idForm);
        var input = form.getElementsByTagName("input");
        var select = form.getElementsByTagName("select");
        for (x in input) {
            if (typeof input[x] === "object" && input[x].getAttribute('data-require') === "true" && input[x].value === "") {
                alerta.show(input[x].getAttribute('data-label') + " é obrigatório.", 'error', 2500, 5);
                input[x].focus();
                return f(false);
            }
        }
        for (x in select) {
            if (typeof select[x] === "object" && select[x].getAttribute('data-require') === "true" && select[x].value === "") {
                alerta.show(select[x].getAttribute('data-label') + " é obrigatório.", 'error', 2500, 5);
                select[x].focus();
                return f(false);
            }
        }
        return f(true);
    };

});
gedApp.service('LocalStorage', function () {
    var temp = '';
    this.setInfo = function (desc, valor) {
        if (this.getInfo()) {
            var obj = this.getInfo();
            obj[desc] = valor;
            temp = JSON.stringify(obj);
        } else {
            var obj = {};
            obj[desc] = valor;
            temp = JSON.stringify(obj);
        }
        return localStorage.setItem('localGed', temp);
    },
            this.getInfo = function () {
                var objR = localStorage.getItem('localGed');
                return JSON.parse(objR);
            },
            this.delInfo = function (desc) {
                if (this.getInfo()) {
                    var objD = this.getInfo();
                    delete objD[desc];
                    tempD = JSON.stringify(objD);
                    return localStorage.setItem('localGed', tempD);
                }
            };
});
gedApp.service('Visualizar', function ($rootScope) {
    return function (arquivo, caminho) {
        function isImage(nome, f) {
            var ext = ['png', 'jpg', 'gif', 'jpeg', 'bmp', 'PNG', 'JPG', 'JPEG', 'BMP'];
            var arr = nome.split('.');
            var qdt = arr.length;
            for (y in ext) {
                if (ext[y] == arr[qdt - 1]) {
                    return f(true);
                }
            }
            return f(false);
        }
        $('#modalViewArquivo').modal('show');
        $rootScope.arquivoViewNome = arquivo;
        isImage(arquivo, function (result) {
            $rootScope.imgView = result;
            if ($rootScope.imgView) {
                $rootScope.imgViewCaminho = caminho;
            } else {
                if (arquivo.split('.')[arquivo.split('.').length - 1] !== "pdf") {
                    $("#viewArquivo").load(caminho + arquivo);
                } else {
                    PDFObject.embed('ged/'+caminho + arquivo, "#viewArquivo");
                }
            }
        });
    };
});

gedApp.service('Compartilhar', function ($rootScope) {
    return function (arquivo, caminho) {        
        $('#modalCompartilhamento').modal('show');        
    };
});
