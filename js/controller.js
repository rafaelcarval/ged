var jViewCentro = false;

gedApp.controller('menuController', function ($scope, $rootScope, $state, $stateParams, $timeout, login, alerta, Sessao, empresa, arvore, Slugify, dataCustom, Estrutura, InicializarArvore) {
    $scope.pesquisa = ($stateParams.pesquisa) ? $stateParams.pesquisa : '';
    $scope.corrigirPalavra = function (palavras) {
        return Slugify.palavra(palavras);
    };
    Sessao.validar();
    $scope.usuario = $rootScope.usuario;
    $rootScope.menu = [
        {titulo: 'Home', icone: 'fa-home', itens: [{}], link: 'base.home'},
        {titulo: 'Cadastros', icone: 'fa-edit', itens: [
                {titulo: 'Empresas', link: "base.listar_empresa"},
                {titulo: 'Departamentos', link: "base.novo_departamento({acao: 'novo'})"},
                {titulo: 'Usuários', link: "base.listar_usuario"},
                {titulo: 'Tipos de Arquivo', link: "base.listar_tipoarquivo"}
            ]}
    ];
    var empresaN = {};
    if ($rootScope.usuario) {
        empresaN = {codempresa: $rootScope.usuario.codempresa};
    } else {
        empresaN = {codempresa: 102};
    }
    empresa.listar(empresaN).success(function (result) {
        $scope.empresa = result;
        $scope.empresa.logo = '/ged/upload/' + result.logo;
    });
    $scope.inicializarArvore = function () {
        InicializarArvore.start();
    };
    $scope.pesquisar = function (pesquisa) {
        if (pesquisa.length >= 1) {
            $state.go('base.pesquisa', {pesquisa: pesquisa});
        }
    };
    $rootScope.itemMenu = [];
    $scope.inicializarItemMenu = function () {
        for (var x = 0; x < $rootScope.menu.length; x++) {
            $rootScope.itemMenu.push({ativo: false});
            for (var t = 0; t < $rootScope.menu[x].itens.length; t++) {
                if (typeof $rootScope.menu[x].itens[t].itens !== 'undefined') {
                    $rootScope.itemMenu[x][t] = {ativo: false};
                    for (var y = 0; y < $rootScope.menu[x].itens[t].itens.length; y++) {
                        $rootScope.itemMenu[x][t][y] = {ativo: false};
                    }
                }
            }
        }
    };
    $scope.logout = function () {
        login.unsetSession().success(function (result) {
            if (result.logout === true) {
                //alerta.show("Sessão encerrada com sucesso!", 'success', 2000, 5);
                $rootScope.usuario = '';
                $rootScope.loginRoot = false;
				document.location.href= 'https://sistema.safetydocs.com.br/ged/';
            } else {
                //alerta.show("Falha ao encerrar a sessão!", 'error', 2000, 5);
				document.location.href= 'https://sistema.safetydocs.com.br/ged/';
            }
        });
    };
    $scope.toggleFullScreen = function () {
        if (!document.fullscreenElement && // alternative standard method
                !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {  // current working methods
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            } else if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
    };
    $scope.novaPasta = function (p) {
        alerta.input('Insira um nome para a pasta:', {ok: 'Criar Pasta'}, function (result) {
            if (result) {
                var arvoreCadastro = {descricao: result, codpai: p, codusuario: $rootScope.usuario.codusuario};
                arvore.addPasta(arvoreCadastro).success(function (result) {
                    alerta.show('Pasta criada com sucesso', 'success', 2000);
                    setTimeout(InicializarArvore.start(), 5000);
                });
            }
        });
    };
    $scope.exibirOcultarItem = function (item, subItem, subSubItem) {
        if (typeof subItem === 'undefined') {
            for (var x = 0; x < $scope.itemMenu.length; x++) {
                if (x == item) {
                    $scope.itemMenu[x]['ativo'] = !$scope.itemMenu[x]['ativo'];
                } else {
                    $scope.itemMenu[x]['ativo'] = false;
                }
            }
        } else if (typeof subSubItem === 'undefined') {
            for (var x in $scope.itemMenu[item]) {
                if (x != 'ativo') {
                    if (x == subItem) {
                        $scope.itemMenu[item][x]['ativo'] = !$scope.itemMenu[item][x]['ativo'];
                    } else {
                        //$scope.itemMenu[item][x]['ativo'] = false;
                    }
                }
            }
        } else {
            for (var x in $scope.itemMenu[item][subItem]) {
                if (x != 'ativo') {
                    if (x == subSubItem) {
                        $scope.itemMenu[item][subItem][x]['ativo'] = !$scope.itemMenu[item][subItem][x]['ativo'];
                    } else {
                        // $scope.itemMenu[item][subItem][x]['ativo'] = false;
                    }
                }
            }
        }
    };
    $scope.login = function (loginN) {
		
		//console.log(loginN);
		//return;
        if (typeof loginN.email !== 'undefined' && typeof loginN.senha !== 'undefined') {
            login.validarAcesso(loginN).success(function (result) {
                if (typeof result.msg === 'undefined') {
                    alerta.show("Bem Vindo(a) " + result.nome, 'success', 2000);
                    console.log(result);
                    $rootScope.usuario = result;
                    $rootScope.loginRoot = true;
                    if ($rootScope.loginRoot) {
                        empresaN = {codempresa: $rootScope.usuario.codempresa};
                    } else {
                        empresaN = {codempresa: 1};
                    }
                    empresa.listar(empresaN).success(function (result) {
                        $scope.empresa = result;
                        $scope.empresa.logo = 'upload/' + result.logo;
                        InicializarArvore.start();
						document.location.href = 'https://sistema.safetydocs.com.br/ged/home';
                    });
                } else {
                    alerta.show(result.msg, 'error', 2000);
                }
            });
        } else {
            alerta.show("Favor Informar email e senha válidos.", 'error', 2000);
        }
    };
    $scope.callFunction = function (funcao, p) {
        if (typeof funcao !== 'undefined' && typeof $scope[funcao] === 'function') {
            if (p) {
                return $scope[funcao](p);
            } else {
                return $scope[funcao]();
            }
        }
    };
	
	$scope.login_automatico = function(email, senha){
		$scope.login({email:email , senha: senha});
	};
	
});
gedApp.controller('listarArquivos', function ($scope, $rootScope, $stateParams, LocalStorage, arvore, Estrutura, alerta, Slugify, Validar, InicializarArvore, arquivo, crud, Sessao, Icone, usuario, tipoArquivo, Helpers, Validar, $http, $location) {
    $scope.classe = 'listarArquivos';
    $scope.lista = [];
    $scope.selectFaIndex = 0;
    $scope.SelectedAvailItems = [];
    $scope.SelectedSelectedListItems = [];
    $scope.SelectedListItems = [];
    $scope.urlView = '';
    $scope.classRowList = 'row';
    $scope.classList = 'item';
    $scope.classIconSize = 'fa-3x';
    $scope.classUnique = '';
    $scope.linkDownload = '';
    $scope.erroDownload = '';
    $scope.arquivoDownload = '';
    $scope.classFloatList = '';
    $scope.styleExibe = 'display:none';
    $scope.colStyleExibe = '12';
    $scope.tipoListagem = localStorage.getItem("tipoListagem");
    if (typeof ($scope.tipoListagem) == "undefined") {
        localStorage.setItem("tipoListagem", "item");
    }
    var arquivo = new arquivo();
    arquivo.upload($scope);
    Estrutura.get();
    $scope.corrigirPalavra = function (palavras) {
        return Slugify.palavra(palavras);
    };
    $scope.listarArvore = function (cod) {
        arvore.listar({codpai: cod}).success(function (result) {
            $scope.pastas = result;
        });
    };
    $scope.setGradeLista = function () {
        $scope.classRowList = 'row';
        $scope.classList = 'item';
        $scope.classIconSize = 'fa-3x';
        $scope.classUnique = '';
        $scope.classFloatList = '';
        $scope.styleExibe = 'display:none';
        $scope.colStyleExibe = '12';
        localStorage.setItem("tipoListagem", "item");
    };
    $scope.setTableLista = function () {
        $scope.classRowList = 'list-group';
        $scope.classList = '';
        $scope.classIconSize = 'fa-1x padding-right-10';
        $scope.classUnique = 'list-group-item';
        $scope.classFloatList = 'pull-left';
        $scope.styleExibe = '';
        $scope.colStyleExibe = '4';
        localStorage.setItem("tipoListagem", "list-group");
    };
    if ($scope.tipoListagem == "item") {
        $scope.setGradeLista();
    } else {
        $scope.setTableLista();
    }
    crud.listar('ged_vw_tipoarquivo', {codarvore: $rootScope.base}).success(function (result) {
        if (Array.isArray(result)) {
            $scope.listaTipoArquivo = result;
        } else {
            $scope.listaTipoArquivo = [result];
        }
    });
    $scope.listarArquivosDiretorio = function () {
        var classe = '';
        var obj = {};        
        if ($rootScope.codpai > 0) {
            classe = 'ged_vw_arquivo';
            obj = {codarvore: $rootScope.codpai, codusuarioCadastro: $rootScope.usuario.codusuario};
            if ($rootScope.usuario.codtipousuario >= 3) {
                delete obj.codusuarioCadastro;
            }
        } else {
            classe = 'ged_vw_arquivocompartilhado';
            obj = {codusuario: $rootScope.usuario.codusuario};
        }
        crud.listar(classe, obj).success(function (result) {
            if (typeof result.msg === 'undefined') {
                $scope.arquivos = (Array.isArray(result)) ? result : [result];
            }
        });
    };
    $scope.getIcone = function (nome) {
        return Icone.get(nome);
    };
    $scope.inicializaArquivosEditar = function (index) {
        if (index && $scope.arquivosEditar.length > 1) {
            $scope.arquivosEditar.splice(index, 1);
        } else {
            crud.executar('Upload', 'listarArquivos').success(function (result) {
                $scope.arquivosEditar = result;
            });
        }
    };
    $scope.carregarListaPreAprovados = function (codarquivo) {
        $scope.getTipoArquivo(codarquivo);
        $scope.getPreAprovacao($rootScope.codpai);
        $scope.getAprovacao(codarquivo);
    };
    $scope.getTipoArquivo = function (codarquivo) {
        tipoArquivo.getTipoAquivo(codarquivo).success(function (result) {
            if (Array.isArray(result)) {
                $scope.tipoArquivos = result;
            } else {
                $scope.tipoArquivos = [result];
            }
        });
    };
    $scope.getPreAprovacao = function (codempresa) {
        usuario.listarPreAprovacao(codempresa).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.usuarioempresa = result;
            } else {
                $scope.lista.usuarioempresa = [result];
            }
        });
    };
    $scope.getUsuariosCompartilhar = function () {
        crud.listar('ged_vw_usuario', {codempresa: $rootScope.usuario.codempresa}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.usuarioCompartilhar = result;
            } else {
                $scope.lista.usuarioCompartilhar = [result];
            }
        });
    };
    $scope.getDepartamentosCompartilhar = function () {
        crud.listar('ged_vw_departamento_select', {codempresa: $rootScope.usuario.codempresa}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.departamentoCompartilhar = result;
            } else {
                $scope.lista.departamentoCompartilhar = [result];
            }
        });
    };
    $scope.getUsuariosCompartilharPasta = function () {
        crud.listar('ged_vw_usuario', {codempresa: $rootScope.usuario.codempresa}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.usuarioCompartilharPasta = result;
            } else {
                $scope.lista.usuarioCompartilharPasta = [result];
            }
        });
    };
    $scope.getDepartamentosCompartilharPasta = function () {
        crud.listar('ged_vw_departamento_select', {codempresa: $rootScope.usuario.codempresa}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.departamentoCompartilharPasta = result;
            } else {
                $scope.lista.departamentoCompartilharPasta = [result];
            }
        });
    };
    $scope.getUsuariosCompartilhados = function (codarquivo) {
        crud.listar('ged_vw_arquivocompartilhado_usuarios', {codarquivo: codarquivo}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.compartilhado = result;
            } else {
                $scope.lista.compartilhado = [result];
            }
            if (!$scope.lista.compartilhado[0].msg) {
                var c = $scope.lista.usuarioCompartilhar.filter(function (item) {
                    return !$scope.lista.compartilhado.some(function (other) {
                        if ((item.codusuario === other.codusuario)) {
                            return item;
                        }
                    });
                });
                $scope.lista.usuarioCompartilhar = c;
            } else {
                $scope.lista.compartilhado = [];
            }
        });
    };
    $scope.getDepartamentosCompartilhados = function (codarquivo) {
        crud.listar('ged_vw_arquivocompartilhado_departamento', {codarquivo: codarquivo}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.deptocompartilhado = result;
            } else {
                $scope.lista.deptocompartilhado = [result];
            }
            if (!$scope.lista.deptocompartilhado[0].msg) {
                var c = $scope.lista.departamentoCompartilhar.filter(function (item) {
                    return !$scope.lista.deptocompartilhado.some(function (other) {
                        if ((item.coddepartamento === other.coddepartamento)) {
                            return item;
                        }
                    });
                });
                $scope.lista.departamentoCompartilhar = c;
            } else {
                $scope.lista.deptocompartilhado = [];
            }
        });
    };
    $scope.getUsuariosCompartilhadosPasta = function (codarvore) {
        crud.listar('ged_vw_arvorecompartilhada_usuarios', {codarvore: codarvore}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.compartilhadoPasta = result;
            } else {
                $scope.lista.compartilhadoPasta = [result];
            }
            if (!$scope.lista.compartilhadoPasta[0].msg) {
                var c = $scope.lista.usuarioCompartilharPasta.filter(function (item) {
                    return !$scope.lista.compartilhadoPasta.some(function (other) {
                        if ((item.codusuario === other.codusuario)) {
                            return item;
                        }
                    });
                });
                $scope.lista.usuarioCompartilharPasta = c;
            } else {
                $scope.lista.compartilhadoPasta = [];
            }
        });
    };
    $scope.getDepartamentosCompartilhadosPasta = function (codarvore) {
        crud.listar('ged_vw_arvorecompartilhada_departamento', {codarvore: codarvore}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.deptocompartilhadoPasta = result;
            } else {
                $scope.lista.deptocompartilhadoPasta = [result];
            }
            if (!$scope.lista.deptocompartilhadoPasta[0].msg) {
                var c = $scope.lista.departamentoCompartilharPasta.filter(function (item) {
                    return !$scope.lista.deptocompartilhadoPasta.some(function (other) {
                        if ((item.coddepartamento === other.coddepartamento)) {
                            return item;
                        }
                    });
                });
                $scope.lista.departamentoCompartilharPasta = c;
            } else {
                $scope.lista.deptocompartilhadoPasta = [];
            }
        });
    };
    $scope.inicializarCompartilhamento = function (codarquivo) {
        $scope.getUsuariosCompartilhar();
        $scope.getUsuariosCompartilhados(codarquivo);
        $scope.getDepartamentosCompartilhar();
        $scope.getDepartamentosCompartilhados(codarquivo);
    };
    $scope.inicializarCompartilhamentoPasta = function (codarvore) {
        $scope.getUsuariosCompartilharPasta();
        $scope.getUsuariosCompartilhadosPasta(codarvore);
        $scope.getDepartamentosCompartilharPasta();
        $scope.getDepartamentosCompartilhadosPasta(codarvore);
    };
    /*$scope.removerArquivo = function (codarquivo) {
     crud.delete('ged_arquivos', {codarquivo: codarquivo}).success(function (result) {
     alerta.show('Arquivo:' + result.nome + ' excluído com sucesso.', 'success', 2000, 5);
     $scope.listarArvore($rootScope.codpai);
     $scope.listarArquivosDiretorio();
     });
     };*/
    $scope.getAprovacao = function (codarquivo) {
        tipoArquivo.listaVwAprovacao(codarquivo).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.aprovacao = result;
            } else {
                $scope.lista.aprovacao = [result];
            }
            if (!$scope.lista.aprovacao[0].msg) {
                var c = $scope.lista.usuarioempresa.filter(function (item) {
                    return !$scope.lista.aprovacao.some(function (other) {
                        if ((item.codigo === other.codigo && other.codtipoarquivo === codarquivo)) {
                            return item;
                        }
                    });
                });
                $scope.lista.usuarioempresa = c;
            } else {
                $scope.lista.add = [];
            }
        });
    };
    $scope.visualizarArquivo = function (caminho, arquivo) {
		
		jViewCentro = window.open('ged/php/viewdocs.php?c='+caminho + arquivo, 'jViewCentro', 'width=900, height=675, scrollbars=yes, top=50, left=50');
		
		return;
		
		
		$('#modalViewArquivo').modal('show');
		console.log(caminho + arquivo);
        $rootScope.arquivoViewNome = arquivo;
        $scope.isImage(arquivo, function (result) {
            $rootScope.imgView = result;
            if ($rootScope.imgView) {
                $rootScope.imgViewCaminho = caminho;
            } else {
				console.log('oi 3');
                if (arquivo.split('.')[arquivo.split('.').length - 1] !== "pdf") {
					console.log('oi 4');
                    $("#viewArquivo").load(caminho + arquivo);
					$('#modalViewArquivo').modal('show');
					console.log('oi 6');
                } else {
					console.log('oi 5');
                    //PDFObject.embed(caminho + arquivo, "#viewArquivo");
					$('#modalViewArquivo').modal('hide');
					$('.modal-backdrop').hide();

                    jViewCentro = window.open('ged/php/viewdocs.php?c='+caminho + arquivo, 'jViewCentro', 'width=900, height=675, scrollbars=yes, top=50, left=50');
					
					$('#modalViewArquivo').modal('hide');
					$('.modal-backdrop').hide();

                }
            }
        });
    };
    $scope.isImage = function (nome, f) {
        var ext = ['png', 'jpg', 'gif', 'jpeg', 'bmp', 'PNG', 'JPG', 'JPEG', 'BMP'];
        var arr = nome.split('.');
        var qdt = arr.length;
        for (y in ext) {
            if (ext[y] == arr[qdt - 1]) {
                return f(true);
            }
        }
        return f(false);
    };
    $scope.btnRight = function (SelectedAvailItems) {
        angular.forEach(SelectedAvailItems, function (value, key) {
            // this.push(value);
        },
                $scope.lista.aprovacao[$scope.selectFaIndex]);
        angular.forEach(SelectedAvailItems, function (value, key) {
            for (var i = $scope.lista.usuarioempresa.length - 1; i >= 0; i--) {
                if ($scope.lista.usuarioempresa[i].codigo === value.codigo) {
                    $scope.lista.aprovacao.push($scope.lista.usuarioempresa[i]);
                    $scope.lista.usuarioempresa.splice(i, 1);
                }
            }
        });
    };
    $scope.btnLeft = function (SelectedSelectedListItems) {
        //move selected.
        console.log(SelectedSelectedListItems);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.usuarioempresa[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.aprovacao.length - 1; i >= 0; i--) {
                if ($scope.lista.aprovacao[i].codigo === value.codigo) {
                    $scope.lista.usuarioempresa.push($scope.lista.aprovacao[i]);
                    $scope.lista.aprovacao.splice(i, 1);
                }
            }
        });
    };
    $scope.btnAddUsuario = function (listaUsuario) {
        angular.forEach(listaUsuario, function (value, key) {
            // this.push(value);
        },
                $scope.lista.compartilhado[$scope.selectFaIndex]);
        angular.forEach(listaUsuario, function (value, key) {
            for (var i = $scope.lista.usuarioCompartilhar.length - 1; i >= 0; i--) {
                if ($scope.lista.usuarioCompartilhar[i].codusuario === value.codusuario) {
                    $scope.lista.compartilhado.push($scope.lista.usuarioCompartilhar[i]);
                    $scope.lista.usuarioCompartilhar.splice(i, 1);
                }
            }
        });
    };
    $scope.btnRemUsuario = function (SelectedSelectedListItems) {
        //move selected.
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.usuarioCompartilhar[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.compartilhado.length - 1; i >= 0; i--) {
                if ($scope.lista.compartilhado[i].codusuario === value.codusuario) {
                    $scope.lista.usuarioCompartilhar.push($scope.lista.compartilhado[i]);
                    $scope.lista.compartilhado.splice(i, 1);
                }
            }
        });
    };
    $scope.btnAddDepartamento = function (listaDepartamento) {
        angular.forEach(listaDepartamento, function (value, key) {
            // this.push(value);
        },
                $scope.lista.deptocompartilhado[$scope.selectFaIndex]);
        angular.forEach(listaDepartamento, function (value, key) {
            for (var i = $scope.lista.departamentoCompartilhar.length - 1; i >= 0; i--) {
                if ($scope.lista.departamentoCompartilhar[i].coddepartamento === value.coddepartamento) {
                    $scope.lista.deptocompartilhado.push($scope.lista.departamentoCompartilhar[i]);
                    $scope.lista.departamentoCompartilhar.splice(i, 1);
                }
            }
        });
    };
    $scope.btnRemDepartamento = function (SelectedSelectedListItems) {
        //move selected.
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.departamentoCompartilhar[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.deptocompartilhado.length - 1; i >= 0; i--) {
                if ($scope.lista.deptocompartilhado[i].coddepartamento === value.coddepartamento) {
                    $scope.lista.departamentoCompartilhar.push($scope.lista.deptocompartilhado[i]);
                    $scope.lista.deptocompartilhado.splice(i, 1);
                }
            }
        });
    };
    $scope.btnAddUsuarioPasta = function (listaUsuario) {
        angular.forEach(listaUsuario, function (value, key) {
            // this.push(value);
        },
                $scope.lista.compartilhadoPasta[$scope.selectFaIndex]);
        angular.forEach(listaUsuario, function (value, key) {
            for (var i = $scope.lista.usuarioCompartilharPasta.length - 1; i >= 0; i--) {
                if ($scope.lista.usuarioCompartilharPasta[i].codusuario === value.codusuario) {
                    $scope.lista.compartilhadoPasta.push($scope.lista.usuarioCompartilharPasta[i]);
                    $scope.lista.usuarioCompartilharPasta.splice(i, 1);
                }
            }
        });
    };
    $scope.btnRemUsuarioPasta = function (SelectedSelectedListItems) {
        //move selected.
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.usuarioCompartilharPasta[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.compartilhadoPasta.length - 1; i >= 0; i--) {
                if ($scope.lista.compartilhadoPasta[i].codusuario === value.codusuario) {
                    $scope.lista.usuarioCompartilharPasta.push($scope.lista.compartilhadoPasta[i]);
                    $scope.lista.compartilhadoPasta.splice(i, 1);
                }
            }
        });
    };
    $scope.btnAddDepartamentoPasta = function (listaDepartamento) {
        angular.forEach(listaDepartamento, function (value, key) {
            // this.push(value);
        },
                $scope.lista.deptocompartilhadoPasta[$scope.selectFaIndex]);
        angular.forEach(listaDepartamento, function (value, key) {
            for (var i = $scope.lista.departamentoCompartilharPasta.length - 1; i >= 0; i--) {
                if ($scope.lista.departamentoCompartilharPasta[i].coddepartamento === value.coddepartamento) {
                    $scope.lista.deptocompartilhadoPasta.push($scope.lista.departamentoCompartilharPasta[i]);
                    $scope.lista.departamentoCompartilharPasta.splice(i, 1);
                }
            }
        });
    };
    $scope.btnRemDepartamentoPasta = function (SelectedSelectedListItems) {
        //move selected.
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.departamentoCompartilharPasta[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.deptocompartilhadoPasta.length - 1; i >= 0; i--) {
                if ($scope.lista.deptocompartilhadoPasta[i].coddepartamento === value.coddepartamento) {
                    $scope.lista.departamentoCompartilharPasta.push($scope.lista.deptocompartilhadoPasta[i]);
                    $scope.lista.deptocompartilhadoPasta.splice(i, 1);
                }
            }
        });
    };
    $scope.listarArvore($rootScope.codpai);
    $scope.listarArquivosDiretorio();
    $scope.carregarArquivos = function () {
        if (typeof $scope.lista.aprovacao !== 'undefined' && $scope.lista.aprovacao.length > 0) {
            $scope.listaaprovadore = [];
            angular.forEach($scope.lista.aprovacao, function (value, key) {
                $scope.listaaprovadore.push(value.codigo);
            });
        } else {
            alerta.show('Favor informar os devidos aprovadores', 'error', 2000, 5);
            return;
        }
        if (typeof $scope.uploader.queue[0] !== 'undefined') {
            arquivo.carregar({
                _c: 'Upload',
                _f: 'carregar',
                _p: {codtipoarquivo: $scope.cadastro.codtipoarquivo, codusuariocadastro: $rootScope.usuario.codusuario, codusuarioautor: $rootScope.usuario.codusuario},
                _w: {codraiz: $rootScope.base, codpai: $rootScope.codpai, listaaprovadores: $scope.listaaprovadore}
            }, function (result) {
                if (typeof result.error === 'undefined') {
                    alerta.show('Arquivo:' + result.nome + ' carregados com sucesso.', 'success', 2000, 5);
                    $scope.inicializaArquivosEditar();
                } else {
                    alerta.show('Falha ao carregar arquivos.</br> ERRO:' + result.error, 'success', 2000, 5);
                }
            });
        } else {
            alerta.show('Favor selecionar os arquivos.', 'error', 2000, 5);
            return;
        }
    };
    $scope.atualizarArquivos = function (listaObjetos) {
        Validar.form('editFile', function (r) {
            if (r) {
                if (Array.isArray(listaObjetos)) {
                    crud.update('ged_arquivo', listaObjetos).success(function (result) {
                        if (typeof result.error === 'undefined') {
                            console.log(result);
                            alerta.show('Arquivo(s) atualizado(s) com sucesso.', 'success', 2000);
                            Sessao.delItensUpload({nome: 'codarquivoupload', valor: ''});
                            $scope.listarArquivosDiretorio();
                            $('#modalUpload').modal('hide');
                            delete $scope.arquivosEditar;
                        } else {
                            alerta.show('Falha ao atualizar arquivos. Error: ' + result.error, 'error', 2000);
                        }
                    });
                } else {
                    console.log('parametro incorreto');
                }
            }
        });
    };
    $scope.novaPasta = function () {
        alerta.input('Insira um nome para a pasta:', {ok: 'Criar Pasta'}, function (result) {
            if (result) {
                var arvoreCadastro = {descricao: result, codpai: $rootScope.codpai, codusuario: $rootScope.usuario.codusuario};
                arvore.addPasta(arvoreCadastro).success(function (result) {
                    alerta.show('Pasta criada com sucesso', 'success', 2000);
                    setTimeout(InicializarArvore.start(), 5000);
                    $scope.listarArvore($rootScope.codpai);
                    $scope.listarArquivosDiretorio($rootScope.codpai);
                });
            }
        });
    };
    $scope.removerArquivo = function (codarquivo, index) {
        alerta.confirm('Deseja remover o arquivo?', function (result) {
            if (result) {
                Sessao.delItensUpload({nome: 'codarquivoupload', valor: codarquivo});
                crud.delete('ged_arquivo', {codarquivo: codarquivo}).success(function (result) {
                    if (typeof result.msg === 'undefined') {
                        $scope.inicializaArquivosEditar(index);
                        alerta.show('Arquivo removido com sucesso! ', 'success', 2000);
                        $scope.listarArquivosDiretorio();
                    } else {
                        alerta.show('Falha ao remover arquivo. Erro: ' + result.msg, 'error', 2000);
                    }
                });
            } else {
                alerta.show('Arquivo não removido', 'error', 2000);
            }
        });
    };
    $scope.compartilharArquivo = function () {
        if (typeof $scope.arquivoCompartilhar != 'undefined') {
            var remove = $scope.lista.usuarioCompartilhar;
            var inclui = $scope.lista.compartilhado;
            var arquivoCompartilhar = [];
            var arquivoRemover = [];
            var removeDepto = $scope.lista.departamentoCompartilhar;
            var incluiDepto = $scope.lista.deptocompartilhado;
            var arquivoCompartilharDepto = [];
            var arquivoRemoverDepto = [];
            if (Array.isArray(remove)) {
                for (x in remove) {
                    arquivoRemover.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, codusuario: remove[x].codusuario});
                }
            } else {
                arquivoRemover.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, codusuario: remove.codusuario});
            }
            if (Array.isArray(inclui)) {
                for (x in inclui) {
                    if (typeof inclui[x].codarquivo == 'undefined' && typeof inclui[x].codusuario != 'undefined') {
                        arquivoCompartilhar.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, codusuario: inclui[x].codusuario, codpermissao: 1});
                    }
                }
            } else {
                if (typeof inclui.codarquivo == 'undefined' && typeof inclui.codusuario != 'undefined') {
                    arquivoCompartilhar.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, codusuario: inclui.codusuario, codpermissao: 1});
                }
            }
            if (Array.isArray(removeDepto)) {
                for (x in removeDepto) {
                    arquivoRemoverDepto.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, coddepartamento: removeDepto[x].coddepartamento});
                }
            } else {
                arquivoRemoverDepto.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, coddepartamento: removeDepto.coddepartamento});
            }
            if (Array.isArray(incluiDepto)) {
                for (x in incluiDepto) {
                    if (typeof incluiDepto[x].codarquivo == 'undefined' && typeof incluiDepto[x].coddepartamento != 'undefined') {
                        arquivoCompartilharDepto.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, coddepartamento: incluiDepto[x].coddepartamento});
                    }
                }
            } else {
                if (typeof incluiDepto.codarquivo == 'undefined' && typeof incluiDepto.coddepartamento != 'undefined') {
                    arquivoCompartilharDepto.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, coddepartamento: incluiDepto.coddepartamento});
                }
            }
            crud.delete('ged_arquivocompartilhado', arquivoRemover);
            crud.delete('ged_arquivocompartilhado_dep', arquivoRemoverDepto);
            console.log(arquivoCompartilharDepto);
            crud.insert('ged_arquivocompartilhado_dep', arquivoCompartilharDepto);
            crud.insert('ged_arquivocompartilhado', arquivoCompartilhar).success(function (result) {
                if (typeof result.error == 'undefined') {
                    alerta.show('Arquivo compartilhado com sucesso!', 'success', 2000);
                    $('#modalCompartilhamento').modal('hide');
                } else {
                    alerta.show(result.error, 'success', 2000);
                }
            });
        }
    };
    $scope.compartilharPasta = function () {
        if (typeof $scope.pastaCompartilhar != 'undefined') {
            var remove = $scope.lista.usuarioCompartilharPasta;
            var inclui = $scope.lista.compartilhadoPasta;
            var pastaCompartilhar = [];
            var pastaRemover = [];
            var removeDepto = $scope.lista.departamentoCompartilharPasta;
            var incluiDepto = $scope.lista.deptocompartilhadoPasta;
            var pastaCompartilharDepto = [];
            var pastaRemoverDepto = [];
            if (Array.isArray(remove)) {
                for (x in remove) {
                    pastaRemover.push({codarvore: $scope.pastaCompartilhar.codarvore, codusuario: remove[x].codusuario});
                }
            } else {
                pastaRemover.push({codarvore: $scope.pastaCompartilhar.codarvore, codusuario: remove.codusuario});
            }
            if (Array.isArray(inclui)) {
                for (x in inclui) {
                    if (typeof inclui[x].codarvore == 'undefined' && typeof inclui[x].codusuario != 'undefined') {
                        pastaCompartilhar.push({codarvore: $scope.pastaCompartilhar.codarvore, codusuario: inclui[x].codusuario, codpermissao: 1});
                    }
                }
            } else {
                if (typeof inclui.codarvore == 'undefined' && typeof inclui.codusuario != 'undefined') {
                    pastaCompartilhar.push({codarvore: $scope.pastaCompartilhar.codarvore, codusuario: inclui.codusuario, codpermissao: 1});
                }
            }
            if (Array.isArray(removeDepto)) {
                for (x in removeDepto) {
                    pastaRemoverDepto.push({codarvore: $scope.pastaCompartilhar.codarvore, coddepartamento: removeDepto[x].coddepartamento});
                }
            } else {
                pastaRemoverDepto.push({codarvore: $scope.pastaCompartilhar.codarvore, coddepartamento: removeDepto.coddepartamento});
            }
            if (Array.isArray(incluiDepto)) {
                for (x in incluiDepto) {
                    if (typeof incluiDepto[x].codarvore == 'undefined' && typeof incluiDepto[x].coddepartamento != 'undefined') {
                        pastaCompartilharDepto.push({codarvore: $scope.pastaCompartilhar.codarvore, coddepartamento: incluiDepto[x].coddepartamento});
                    }
                }
            } else {
                if (typeof incluiDepto.codarvore == 'undefined' && typeof incluiDepto.coddepartamento != 'undefined') {
                    pastaCompartilharDepto.push({codarvore: $scope.pastaCompartilhar.codarvore, coddepartamento: incluiDepto.coddepartamento});
                }
            }
            crud.delete('ged_arvorecompartilhada', pastaRemover);
            crud.delete('ged_arvorecompartilhada_dep', pastaRemoverDepto);
            crud.insert('ged_arvorecompartilhada_dep', pastaCompartilharDepto);
            crud.insert('ged_arvorecompartilhada', pastaCompartilhar).success(function (result) {
                if (typeof result.error == 'undefined') {
                    alerta.show('Pasta compartilhada com sucesso!', 'success', 2000);
                    $('#modalCompartilhamentoPasta').modal('hide');
                } else {
                    alerta.show(result.error, 'success', 2000);
                }
            });
        }
    };
    $scope.getLinkDownload = function () {
        var codigo = $stateParams.codigo1;
        arquivo.linkDownload(codigo).then(function (result) {
            if (result.data.tipo == "erro") {
                $scope.erroDownload = result.data.msg;
                $scope.arquivoDownload = result.data.arquivo;
            } else {
                $scope.linkDownload = "reload";
                $scope.arquivoDownload = result.data.arquivo;
                setTimeout(InicializarArvore.start(), 5000);
                document.location.href = "php/download.php?_arq=" + btoa(result.data.codarquivo);
            }
        });
    };
    $scope.menuOptionsRaiz = [
        ['<i class="fa fa-plus-circle"></i> Novo', [
                ['<i class="fa fa-folder"></i> Pasta', function ($itemScope) {
                        $scope.novaPasta();
                    }],
                null, ['<i class="fa fa-file"></i> Arquivo(s)', function ($itemScope) {
                        $('#modalUpload').modal('show');
                    }]
            ], Sessao.permissao()],
        null,
        ['<i class="fa fa-refresh"></i> Atualizar', function ($itemScope) {
                setTimeout(InicializarArvore.start(), 5000);
                $scope.listarArvore($rootScope.codpai);
                $scope.listarArquivosDiretorio($rootScope.codpai);
            }]
    ];
    $scope.menuOptionsFile = [
        ['<i class="fa fa fa-eye"></i> Visualizar', function ($itemScope) {
                if ($itemScope.arquivo.codstatusarquivo != 2) {
                    $scope.visualizarArquivo($itemScope.arquivo.caminho, $itemScope.arquivo.nome);
                } else {
                    alerta.show('Não é possível visualizar arquivo reprovado.', 'error', 2500);
                }
            }, Sessao.permissao()],
        null,
        ['<i class="fa fa-share-square-o"></i> Compartilhar', function ($itemScope) {
                if ($itemScope.arquivo.codstatusarquivo == 3) {
                    $scope.arquivoCompartilhar = $itemScope.arquivo;
                    $('#modalCompartilhamento').modal('show');
                    $scope.inicializarCompartilhamento($itemScope.arquivo.codarquivo);
                } else {
                    alerta.show('Apenas arquivos aprovados podem ser compartilhados', 'error', 2000);
                }
            }, Sessao.permissao()],
        null,
        ['<i class="fa fa-close"></i> Excluir', function ($itemScope) {
                $scope.removerArquivo($itemScope.arquivo.codarquivo);
                /*alerta.confirm('Deseja excluir o arquivo: ' + $itemScope.arquivo.nome + "?", function (result) {
                 if (result) {
                 $scope.removerArquivo($itemScope.arquivo.codarquivo);
                 }
                 });*/
            }, Sessao.permissao()],
        null,
        ['<i class="fa fa-send"></i> Enviar', function ($itemScope) {
                alerta.input('Insira um e-mail para enviar o arquivo "' + $itemScope.arquivo.nome + '":', {ok: 'Enviar arquivo'}, function (result) {
                    if (result) {
                        alerta.confirm('Deseja enviar o arquivo para: ' + result + "?", function (result2) {
                            if (result2) {
                                $scope.arquivoCompartilhar = $itemScope.arquivo;
                                var geraemail = {arquivo: $itemScope.arquivo, tipoMsg: "compartilhado", destinatario: [{nome: "Mateus"}]};
                                crud.executar("GeraEmail", "gerar", geraemail).success(function (result3) {
                                    var email = {msg: result3.msg, assunto: result3.assunto, remetenteNome: "Teste", remetenteEmail: "mateus@proxy.com.br", destinatario: [{email: result, nome: "Mateus"}]};
                                    crud.executar("GeraEmail", "enviar", email).success(function (result4) {
                                        if (result4 == "true") {
                                            alerta.show("E-mail enviado com sucesso!", 'success', 3000, 5);
                                        } else {
                                            alerta.show('Falha ao enviar e-mail. ' + result4.error, 'error', 3000);
                                        }
                                    });
                                });
                            }
                        });
                    }
                }, 1, 2);
            }, Sessao.permissao()]
    ];
    $scope.menuOptionsFolder = [
        ['<i class="fa fa-share-square-o"></i> Compartilhar', function ($itemScope) {
                $scope.pastaCompartilhar = $itemScope.pasta;
                $('#modalCompartilhamentoPasta').modal('show');
                $scope.inicializarCompartilhamentoPasta($itemScope.pasta.codarvore);
            }, Sessao.permissao()],
        null,
        ['<i class="fa fa-close"></i> Excluir', function ($itemScope) {
                var pasta = $itemScope.pasta;
                alerta.confirm("Deseja excluir a pasta: " + pasta.titulo + "?", function (result) {
                    if (result) {
                        crud.delete('ged_arvore', pasta).success(function (result) {
                            if (typeof result.error == 'undefined') {
                                alerta.show('Pasta removida com sucesso!', 'success', 2000);
                                setTimeout(InicializarArvore.start(), 5000);
                                $scope.listarArvore($rootScope.codpai);
                            } else {
                                alerta.show('Falha ao remover pasta. ' + result.error, 'error', 2000);
                            }
                        });
                    }
                });
            }, Sessao.permissao()],
        null,
        ['<i class="fa fa-i-cursor"></i> Renomear', function ($itemScope) {
                alerta.input('Novo nome.', {ok: 'Renomear'}, function (result) {
                    var pasta = {codarvore: $itemScope.pasta.codarvore};
                    if (result) {
                        pasta.descricao = result;
                        crud.update('ged_arvore', pasta).success(function (result) {
                            if (typeof result.error === 'undefined') {
                                alerta.show("Pasta renomeada", 'success', 2000, 5);
                                setTimeout(InicializarArvore.start(), 5000);
                                $scope.listarArvore($rootScope.codpai);
                            }
                        });
                    }
                });
            }, Sessao.permissao()]
    ];
});
gedApp.controller('pesquisa', function ($scope, $rootScope, $stateParams, LocalStorage, $state, Tabela, crud, Sessao, Visualizar,alerta, Compartilhar, arquivo) {
    if (typeof $stateParams.pesquisa === 'undefined' || $stateParams.pesquisa.length <= 0) {
        $state.go('base.home');
    }
    $scope.classe = 'listarArquivos';
    $scope.lista = [];
    $scope.selectFaIndex = 0;
    $scope.SelectedAvailItems = [];
    $scope.SelectedSelectedListItems = [];
    $scope.SelectedListItems = [];
    $scope.urlView = '';
    $scope.classRowList = 'row';
    $scope.classList = 'item';
    $scope.classIconSize = 'fa-3x';
    $scope.classUnique = '';
    $scope.linkDownload = '';
    $scope.erroDownload = '';
    $scope.arquivoDownload = '';
    $scope.classFloatList = '';
    $scope.styleExibe = 'display:none';
    $scope.colStyleExibe = '12';
    $scope.tipoListagem = localStorage.getItem("tipoListagem");
    if (typeof ($scope.tipoListagem) == "undefined") {
        localStorage.setItem("tipoListagem", "item");
    }
    var arquivo = new arquivo();
    arquivo.upload($scope);

    $scope.iconeTitulo = "fa fa-search";
    $scope.titulo = "Pesquisar: ";
    $scope.subTitulo = $stateParams.pesquisa;
    $scope.classe = 'pesquisaArquivo';
    var columns = [
        //{title: "Cod.", data: "codarquivo"},
        {title: "Arquivo", data: "nome"},
        {title: "Descrição", data: "descricao"},
        {title: "Caminho",
            data: "caminhoVirtual",
            render: function (data, type, full, meta) {
                //console.log(JSON.stringify(full));
                //var arquivo = JSON.stringify(full);
                return '<a title="' + data + '" href="ged/' + full.link + '"  class="remove" context-menu="contextmenu({codarquivo:' + "'" + full.codarquivo + "'" + ' , codarvore:' + "'" + full.codarvore + "'" + ' , codstatusarquivo:' + "'" + full.codstatusarquivo + "'" + ' , codusuariocadastro:' + "'" + full.codusuariocadastro + "'" + ' , nome:' + "'" + full.nome + "'" + ' , caminho:' + "'" + full.caminho + "'" + ' , descricao:' + "'" + full.descricao + "'" + ' , palavrachave:' + "'" + full.palavrachave + "'" + ' , pesquisa:' + "'" + full.pesquisa + "'" + ' , dtcadastro:' + "'" + full.dtcadastro + "'" + ' , codempresa:' + "'" + full.codempresa + "'" + ' , fantasia:' + "'" + full.fantasia + "'" + ' , caminhoVirtual:' + "'" + full.caminhoVirtual + "'" + ' , caminhoTruncate:' + "'" + full.caminhoTruncate + "'" + ' , link:' + "'" + full.link + "'" + '})">' + full.caminhoTruncate + '</a>';
            }
        },
        {title: "Dt. Cadastro", data: "dtcadastro"},
        {
            title: "",
            data: "nome",
            orderable: false,
            render: function (data, type, full, meta) {
                return '<i class="fa fa-eye btn" ng-click="visualizar({arquivo:' + "'" + full.nome + "'" + ' ,caminho:' + "'" + full.caminho + "'" + '})"></i>';
            }
        },
        {
            title: "",
            data: "nome",
            orderable: false,
            render: function (data, type, full, meta) {
                return '<i class="fa fa-download btn" ng-click="download({arquivo:' + "'" + full.nome + "'" + ' ,caminho:' + "'" + full.caminho + "'" + '})"></i>';
            }
        }
    ];
    $scope.visualizar = function (elemento) {
        Visualizar(elemento.arquivo, elemento.caminho);
    };
    $scope.download = function (elemento) {        
        window.open('ged/'+elemento.caminho+"/"+elemento.arquivo);
    };
    $scope.contextmenu = function (itemScope){
        $rootScope.itemScope = itemScope;
        console.log($rootScope.itemScope);
        return $scope.menuOptionsFile = [
            ['<i class="fa fa fa-download"></i> Download', function (itemScope) {
                    //console.log($rootScope.itemScope)
                    alerta.show('Em breve', 'warning', 2500);
                    //$scope.downloadArquivo($itemScope);
                }, Sessao.permissao()],
            null,
            ['<i class="fa fa fa-eye"></i> Visualizar', function (itemScope) {
                    if ($rootScope.itemScope.codstatusarquivo != 2) {
                        $scope.visualizarArquivo($rootScope.itemScope.caminho, $rootScope.itemScope.nome);
                    } else {
                        alerta.show('Não é possível visualizar arquivo reprovado.', 'error', 2500);
                    }
                }, Sessao.permissao()],
            null,
            ['<i class="fa fa-share-square-o"></i> Compartilhar', function (itemScope) {
                    //console.log($rootScope.itemScope)
                    if ($rootScope.itemScope.codstatusarquivo == 3) {
                        $scope.arquivoCompartilhar = $rootScope.itemScope;
                        // $('#modalCompartilhamento').modal('show');
                        Compartilhar($rootScope.itemScope.arquivo, $rootScope.itemScope.caminho);
                        $scope.inicializarCompartilhamentoPesquisa($rootScope.itemScope.codarquivo);
                    } else {
                        alerta.show('Apenas arquivos aprovados podem ser compartilhados!', 'error', 2000);
                    }
                }, Sessao.permissao()],
            null,
            ['<i class="fa fa-close"></i> Excluir', function (itemScope) {
                    $scope.removerArquivo($rootScope.itemScope.codarquivo);
                    /*alerta.confirm('Deseja excluir o arquivo: ' + $rootScope.itemScope.nome + "?", function (result) {
                     if (result) {
                     $scope.removerArquivo($rootScope.itemScope.codarquivo);
                     }
                     });*/
                }, Sessao.permissao()],
            null,
            ['<i class="fa fa-send"></i> Enviar', function ($itemScope) {
                alerta.input('Insira um e-mail para enviar o arquivo "' + $rootScope.itemScope.nome + '":', {ok: 'Enviar arquivo'}, function (result) {
                    if (result) {
                        alerta.confirm('Deseja enviar o arquivo para: ' + result + "?", function (result2) {
                            if (result2) {
                                $scope.arquivoCompartilhar = $rootScope.itemScope;
                                var geraemail = {arquivo: $rootScope.itemScope, tipoMsg: "compartilhado", destinatario: [{nome: "Mateus"}]};
                                crud.executar("GeraEmail", "gerar", geraemail).success(function (result3) {
                                    var email = {msg: result3.msg, assunto: result3.assunto, remetenteNome: "Teste", remetenteEmail: "noreply@safetydocs.com.br", destinatario: [{email: result, nome: "Mateus"}]};
                                    crud.executar("GeraEmail", "enviar", email).success(function (result4) {
                                        if (result4 == "true") {
                                            alerta.show("E-mail enviado com sucesso!", 'success', 3000, 5);
                                        } else {
                                            alerta.show('Falha ao enviar e-mail. ' + result4.error, 'error', 3000);
                                        }
                                    });
                                });
                            }
                        });
                    }
                }, 1, 2);
            }, Sessao.permissao()]
        ];
    }
    
    $scope.inicializarTBody = function () {
        crud.executar('PesquisarArquivo', 'pesquisar', $stateParams.pesquisa).success(function (result) {
            if (typeof result.msg === 'undefined') {
                $scope.tbody = result;
            } else {
                $scope.tbody = '';
            }
            var table = new Tabela("#datatable", $scope);
            table.setLinhas($scope.tbody);
            table.setColunas(columns);
            table.getTabela();
        });
    };
    $scope.visualizarArquivo = function (caminho, arquivo) {
        console.log(caminho, arquivo);
        Visualizar(arquivo, caminho);
        /*$('#modalViewArquivo').modal('show');
        $rootScope.arquivoViewNome = arquivo;
        $scope.isImage(arquivo, function (result) {
            $rootScope.imgView = result;
            if ($rootScope.imgView) {
				console.log('log: 0');
                $rootScope.imgViewCaminho = caminho;
            } else {

                if (arquivo.split('.')[arquivo.split('.').length - 1] !== "pdf") {
					console.log('log: 1');
                    $("#viewArquivo").load(caminho + arquivo);
                } else {
					console.log('log: 2');
                    $('#modalViewArquivo').modal('hide');
					$('.modal-backdrop').hide();
                    //PDFObject.embed(caminho + arquivo, "#viewArquivo");
                    jViewCentro = window.open('ged/php/viewdocs.php?s=1&c='+caminho + arquivo, 'jViewCentro', 'width=900, height=675, scrollbars=yes, top=50, left=50');
                }
            }
        });*/
    };
    $scope.inicializarCompartilhamentoPesquisa = function (codarquivo) {
        $scope.getUsuariosCompartilharPesquisa();
        $scope.getUsuariosCompartilhadosPesquisa(codarquivo);
        $scope.getDepartamentosCompartilharPesquisa();
        $scope.getDepartamentosCompartilhadosPesquisa(codarquivo);
    };
    $scope.getUsuariosCompartilharPesquisa = function () {
        crud.listar('ged_vw_usuario', {codempresa: $rootScope.usuario.codempresa}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.usuarioCompartilhar = result;
            } else {
                $scope.lista.usuarioCompartilhar = [result];
            }
        });
    };
    $scope.getUsuariosCompartilhadosPesquisa = function (codarquivo) {
        crud.listar('ged_vw_arquivocompartilhado_usuarios', {codarquivo: codarquivo}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.compartilhado = result;
            } else {
                $scope.lista.compartilhado = [result];
            }
            if (!$scope.lista.compartilhado[0].msg) {
                var c = $scope.lista.usuarioCompartilhar.filter(function (item) {
                    return !$scope.lista.compartilhado.some(function (other) {
                        if ((item.codusuario === other.codusuario)) {
                            return item;
                        }
                    });
                });
                $scope.lista.usuarioCompartilhar = c;
            } else {
                $scope.lista.compartilhado = [];
            }
        });
    };
    $scope.getDepartamentosCompartilharPesquisa = function () {
        crud.listar('ged_vw_departamento_select', {codempresa: $rootScope.usuario.codempresa}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.departamentoCompartilhar = result;
            } else {
                $scope.lista.departamentoCompartilhar = [result];
            }
        });
    };
    $scope.getDepartamentosCompartilhadosPesquisa = function (codarquivo) {
        crud.listar('ged_vw_arquivocompartilhado_departamento', {codarquivo: codarquivo}).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.deptocompartilhado = result;
            } else {
                $scope.lista.deptocompartilhado = [result];
            }
            if (!$scope.lista.deptocompartilhado[0].msg) {
                var c = $scope.lista.departamentoCompartilhar.filter(function (item) {
                    return !$scope.lista.deptocompartilhado.some(function (other) {
                        if ((item.coddepartamento === other.coddepartamento)) {
                            return item;
                        }
                    });
                });
                $scope.lista.departamentoCompartilhar = c;
            } else {
                $scope.lista.deptocompartilhado = [];
            }
        });
    };
    $scope.inicializaArquivosEditar = function (index) {
        if (index && $scope.arquivosEditar.length > 1) {
            $scope.arquivosEditar.splice(index, 1);
        } else {
            crud.executar('Upload', 'listarArquivos').success(function (result) {
                $scope.arquivosEditar = result;
            });
        }
    };
    $scope.listarArquivosDiretorio = function () {
        var classe = '';
        var obj = {};        
        if ($rootScope.codpai > 0) {
            classe = 'ged_vw_arquivo';
            obj = {codarvore: $rootScope.codpai, codusuarioCadastro: $rootScope.usuario.codusuario};
            if ($rootScope.usuario.codtipousuario >= 3) {
                delete obj.codusuarioCadastro;
            }
        } else {
            classe = 'ged_vw_arquivocompartilhado';
            obj = {codusuario: $rootScope.usuario.codusuario};
        }
        crud.listar(classe, obj).success(function (result) {
            if (typeof result.msg === 'undefined') {
                $scope.arquivos = (Array.isArray(result)) ? result : [result];
            }
        });
    };
    $scope.removerArquivo = function (codarquivo, index) {
        alerta.confirm('Deseja remover o arquivo?', function (result) {
            if (result) {
                Sessao.delItensUpload({nome: 'codarquivoupload', valor: codarquivo});
                crud.delete('ged_arquivo', {codarquivo: codarquivo}).success(function (result) {
                    if (typeof result.msg === 'undefined') {
                        
                        alerta.show('Arquivo removido com sucesso! ', 'success', 2000);
                        $scope.inicializarTBody();
                    } else {
                        alerta.show('Falha ao remover arquivo. Erro: ' + result.msg, 'error', 2000);
                    }
                });
            } else {
                alerta.show('Arquivo não removido', 'error', 2000);
            }
        });
    };
    $scope.compartilharArquivo = function () {
        if (typeof $scope.arquivoCompartilhar != 'undefined') {
            var remove = $scope.lista.usuarioCompartilhar;
            var inclui = $scope.lista.compartilhado;
            var arquivoCompartilhar = [];
            var arquivoRemover = [];
            var removeDepto = $scope.lista.departamentoCompartilhar;
            var incluiDepto = $scope.lista.deptocompartilhado;
            var arquivoCompartilharDepto = [];
            var arquivoRemoverDepto = [];
            if (Array.isArray(remove)) {
                for (x in remove) {
                    arquivoRemover.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, codusuario: remove[x].codusuario});
                }
            } else {
                arquivoRemover.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, codusuario: remove.codusuario});
            }
            if (Array.isArray(inclui)) {
                for (x in inclui) {
                    if (typeof inclui[x].codarquivo == 'undefined' && typeof inclui[x].codusuario != 'undefined') {
                        arquivoCompartilhar.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, codusuario: inclui[x].codusuario, codpermissao: 1});
                    }
                }
            } else {
                if (typeof inclui.codarquivo == 'undefined' && typeof inclui.codusuario != 'undefined') {
                    arquivoCompartilhar.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, codusuario: inclui.codusuario, codpermissao: 1});
                }
            }
            if (Array.isArray(removeDepto)) {
                for (x in removeDepto) {
                    arquivoRemoverDepto.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, coddepartamento: removeDepto[x].coddepartamento});
                }
            } else {
                arquivoRemoverDepto.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, coddepartamento: removeDepto.coddepartamento});
            }
            if (Array.isArray(incluiDepto)) {
                for (x in incluiDepto) {
                    if (typeof incluiDepto[x].codarquivo == 'undefined' && typeof incluiDepto[x].coddepartamento != 'undefined') {
                        arquivoCompartilharDepto.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, coddepartamento: incluiDepto[x].coddepartamento});
                    }
                }
            } else {
                if (typeof incluiDepto.codarquivo == 'undefined' && typeof incluiDepto.coddepartamento != 'undefined') {
                    arquivoCompartilharDepto.push({codarquivo: $scope.arquivoCompartilhar.codarquivo, coddepartamento: incluiDepto.coddepartamento});
                }
            }
            crud.delete('ged_arquivocompartilhado', arquivoRemover);
            crud.delete('ged_arquivocompartilhado_dep', arquivoRemoverDepto);
            console.log(arquivoCompartilharDepto);
            crud.insert('ged_arquivocompartilhado_dep', arquivoCompartilharDepto);
            crud.insert('ged_arquivocompartilhado', arquivoCompartilhar).success(function (result) {
                if (typeof result.error == 'undefined') {
                    alerta.show('Arquivo compartilhado com sucesso!', 'success', 2000);
                    $('#modalCompartilhamento').modal('hide');
                } else {
                    alerta.show(result.error, 'success', 2000);
                }
            });
        }
    };

    $scope.btnRight = function (SelectedAvailItems) {
        angular.forEach(SelectedAvailItems, function (value, key) {
            // this.push(value);
        },
                $scope.lista.aprovacao[$scope.selectFaIndex]);
        angular.forEach(SelectedAvailItems, function (value, key) {
            for (var i = $scope.lista.usuarioempresa.length - 1; i >= 0; i--) {
                if ($scope.lista.usuarioempresa[i].codigo === value.codigo) {
                    $scope.lista.aprovacao.push($scope.lista.usuarioempresa[i]);
                    $scope.lista.usuarioempresa.splice(i, 1);
                }
            }
        });
    };
    $scope.btnLeft = function (SelectedSelectedListItems) {
        //move selected.
        console.log(SelectedSelectedListItems);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.usuarioempresa[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.aprovacao.length - 1; i >= 0; i--) {
                if ($scope.lista.aprovacao[i].codigo === value.codigo) {
                    $scope.lista.usuarioempresa.push($scope.lista.aprovacao[i]);
                    $scope.lista.aprovacao.splice(i, 1);
                }
            }
        });
    };
    $scope.btnAddUsuario = function (listaUsuario) {
        angular.forEach(listaUsuario, function (value, key) {
            // this.push(value);
        },
                $scope.lista.compartilhado[$scope.selectFaIndex]);
        angular.forEach(listaUsuario, function (value, key) {
            for (var i = $scope.lista.usuarioCompartilhar.length - 1; i >= 0; i--) {
                if ($scope.lista.usuarioCompartilhar[i].codusuario === value.codusuario) {
                    $scope.lista.compartilhado.push($scope.lista.usuarioCompartilhar[i]);
                    $scope.lista.usuarioCompartilhar.splice(i, 1);
                }
            }
        });
    };
    $scope.btnRemUsuario = function (SelectedSelectedListItems) {
        //move selected.
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.usuarioCompartilhar[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.compartilhado.length - 1; i >= 0; i--) {
                if ($scope.lista.compartilhado[i].codusuario === value.codusuario) {
                    $scope.lista.usuarioCompartilhar.push($scope.lista.compartilhado[i]);
                    $scope.lista.compartilhado.splice(i, 1);
                }
            }
        });
    };
    $scope.btnAddDepartamento = function (listaDepartamento) {
        angular.forEach(listaDepartamento, function (value, key) {
            // this.push(value);
        },
                $scope.lista.deptocompartilhado[$scope.selectFaIndex]);
        angular.forEach(listaDepartamento, function (value, key) {
            for (var i = $scope.lista.departamentoCompartilhar.length - 1; i >= 0; i--) {
                if ($scope.lista.departamentoCompartilhar[i].coddepartamento === value.coddepartamento) {
                    $scope.lista.deptocompartilhado.push($scope.lista.departamentoCompartilhar[i]);
                    $scope.lista.departamentoCompartilhar.splice(i, 1);
                }
            }
        });
    };
    $scope.btnRemDepartamento = function (SelectedSelectedListItems) {
        //move selected.
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.departamentoCompartilhar[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.deptocompartilhado.length - 1; i >= 0; i--) {
                if ($scope.lista.deptocompartilhado[i].coddepartamento === value.coddepartamento) {
                    $scope.lista.departamentoCompartilhar.push($scope.lista.deptocompartilhado[i]);
                    $scope.lista.deptocompartilhado.splice(i, 1);
                }
            }
        });
    };
    $scope.btnAddUsuarioPasta = function (listaUsuario) {
        angular.forEach(listaUsuario, function (value, key) {
            // this.push(value);
        },
                $scope.lista.compartilhadoPasta[$scope.selectFaIndex]);
        angular.forEach(listaUsuario, function (value, key) {
            for (var i = $scope.lista.usuarioCompartilharPasta.length - 1; i >= 0; i--) {
                if ($scope.lista.usuarioCompartilharPasta[i].codusuario === value.codusuario) {
                    $scope.lista.compartilhadoPasta.push($scope.lista.usuarioCompartilharPasta[i]);
                    $scope.lista.usuarioCompartilharPasta.splice(i, 1);
                }
            }
        });
    };
    $scope.btnRemUsuarioPasta = function (SelectedSelectedListItems) {
        //move selected.
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.usuarioCompartilharPasta[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.compartilhadoPasta.length - 1; i >= 0; i--) {
                if ($scope.lista.compartilhadoPasta[i].codusuario === value.codusuario) {
                    $scope.lista.usuarioCompartilharPasta.push($scope.lista.compartilhadoPasta[i]);
                    $scope.lista.compartilhadoPasta.splice(i, 1);
                }
            }
        });
    };
    $scope.btnAddDepartamentoPasta = function (listaDepartamento) {
        angular.forEach(listaDepartamento, function (value, key) {
            // this.push(value);
        },
                $scope.lista.deptocompartilhadoPasta[$scope.selectFaIndex]);
        angular.forEach(listaDepartamento, function (value, key) {
            for (var i = $scope.lista.departamentoCompartilharPasta.length - 1; i >= 0; i--) {
                if ($scope.lista.departamentoCompartilharPasta[i].coddepartamento === value.coddepartamento) {
                    $scope.lista.deptocompartilhadoPasta.push($scope.lista.departamentoCompartilharPasta[i]);
                    $scope.lista.departamentoCompartilharPasta.splice(i, 1);
                }
            }
        });
    };
    $scope.btnRemDepartamentoPasta = function (SelectedSelectedListItems) {
        //move selected.
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.departamentoCompartilharPasta[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.deptocompartilhadoPasta.length - 1; i >= 0; i--) {
                if ($scope.lista.deptocompartilhadoPasta[i].coddepartamento === value.coddepartamento) {
                    $scope.lista.departamentoCompartilharPasta.push($scope.lista.deptocompartilhadoPasta[i]);
                    $scope.lista.deptocompartilhadoPasta.splice(i, 1);
                }
            }
        });
    };

});