gedApp.controller('cadastroAtualizacaoEmpresaController', function ($scope, $rootScope, $http, $stateParams, cep, cnpj, empresa, dataCustom, arquivo, alerta, Validar) {
    var anexo = new arquivo();
    anexo.upload($scope);
    $scope.radio = {listaSimNao: [
            {codigo: "0", nome: "Não"},
            {codigo: "1", nome: "Sim"}
        ], listaCores: [
            {codigo: '"padrao"', nome: "Padrão"},
            {codigo: '"tema-1"', nome: "Tema 1"},
            {codigo: '"tema-2"', nome: "Tema 2"},
            {codigo: '"tema-3"', nome: "Tema 3"},
            {codigo: '"tema-4"', nome: "Tema 4"}
        ]};
    $scope.buscaCidade = function (uf) {
        var uf = (uf) ? uf : $scope.cadastro.uf;
        cep.getCidades(uf).success(function (result) {
            if (result && uf != 'DF') {
                $scope.lista.cidades = result;
            } else {
                $scope.lista.cidades = [{codigo: 'Distrito Federal', nome: 'Distrito Federal'}];
            }
        });
    };
    $scope.inicializarEmpresa = function (company) {
        console.log(company);
        empresa.listar(company).success(function (result) {
            console.log(result);
            $scope.buscaCidade(result.uf);
            $scope.cadastro = result;
        });
    };
    $scope.cadastro = {};
    $scope.lista = {};
    $scope.trocarCor = function (cor) {
        var temp = cor.replace('"', '').replace('"', '');
        $rootScope.usuario.cor = temp;
    };
    var codUsuario = parseInt($rootScope.usuario.codusuario);
    $scope.cadastro.codusuariocadastro = codUsuario;
    $scope.subTitulo = "Empresa";
    $scope.tamanho = 12;
    $scope.classe = "cadastro";
    cep.getEstados().success(function (result) {
        $scope.lista.estados = result;
        $scope.conf = {
            input: [
                {type: 'text', tamanho: '10', model: "fantasia", label: 'Fantasia', disabled: false, quebraLinha: true, require: true},
                {type: 'text', tamanho: '10', model: "razao", label: 'Razão', disabled: false, quebraLinha: true, require: true},
                {type: 'tel', tamanho: '4', model: "cnpj", label: 'CNPJ', disabled: false, quebraLinha: false, ngblur: 'validarCnpj', ngmask: '99.999.999/9999-99', require: false},
                {type: 'text', tamanho: '3', model: "ie", label: 'I.E.', disabled: false, quebraLinha: false},
                {type: 'text', tamanho: '3', model: "cep", label: 'CEP', disabled: false, quebraLinha: true, ngblur: 'consultarCep', require: false, ngmask: '99999-999'},
                {type: 'text', tamanho: '8', model: "endereco", label: 'Endereço', disabled: false, quebraLinha: false, require: false},
                {type: 'text', tamanho: '2', model: "numero", label: 'Número', disabled: false, quebraLinha: true ,require: false},
                {type: 'text', tamanho: '6', model: "complemento", label: 'Complemento', disabled: false, quebraLinha: false},
                {type: 'text', tamanho: '4', model: "bairro", label: "Bairro", disabled: false, quebraLinha: true, require: false},
                {type: 'select', tamanho: '2', model: "uf", label: "Estado", disabled: false, quebraLinha: false, require: false, list: "estados", default: "- Escolha -", ngchange: "buscaCidade"},
                {type: 'select', tamanho: '4', model: "cidade", label: "Cidade", disabled: false, quebraLinha: false, require: false, list: "cidades", default: "- Escolha -"},                
                {type: 'tel', tamanho: '2', model: "telefone", label: 'Telefone', disabled: false, quebraLinha: false, ngmask: '(99) 9999-9999?9'},
                {type: 'tel', tamanho: '2', model: "celular", label: 'Celular', disabled: false, quebraLinha: true, ngmask: '(99) 9?9999-9999'},
                {type: 'file', tamanho: '10', label: "Logo", disabled: false, quebraLinha: true, ngmultiple: true, require: false},
                {type: 'radio', tamanho: '2', model: "ativo", label: 'Ativo', disabled: false, quebraLinha: false, radio: "listaSimNao"},
                {type: 'radio', tamanho: '8', model: "cor", label: 'Perfil de Cores', disabled: false, quebraLinha: false, radio: "listaCores", ngclick: "trocarCor", require: false}
            ]};
    });
    //eleva o nível para botoes específicos
    $scope.nivelUp = function () {
        return 1;
    };
    $scope.lista.cidades = [{codigo: 0, nome: "Escolha um estado antes..."}];
    if ($stateParams.id) {
        $scope.titulo = $stateParams.acao;
        $scope.botao = [
            {label: 'ATUALIZAR', type: 'button', classe: "btn-success", icone: '', ngclick: 'atualizarForm'},
            {label: 'VOLTAR', type: 'reset', classe: "btn-danger", icone: '', ngclick: 'resetForm', link: "/ged/cadastros/empresa/listar"}
        ];
        var getEmpresa = {codempresa: $stateParams.id};
        $scope.inicializarEmpresa(getEmpresa);
    } else {
        $scope.titulo = "Cadastro";
        $scope.botao = [
            {label: 'INCLUIR', type: 'button', classe: "btn-success", icone: '', ngclick: 'saveForm'},
            {label: 'VOLTAR', type: 'reset', classe: "btn-danger", icone: '', ngclick: 'resetForm', link: "/ged/cadastros/empresa/listar"}
        ];
    }
    $scope.obrigatorio = {}
    $scope.carregarArquivo = function () {
        $pasta = 'pastaz';
        console.log($scope.uploader);
        arquivo.carregar($pasta);
    };
    $scope.verificarArquivos = function () {
        return !anexo.naoCarregados();
    };
    $scope.inicializarObrigatorio = function () {
        for (p in $scope.conf.input) {
            if ($scope.conf.input[p].require) {
                $scope.obrigatorio[p] = $scope.conf.input[p].model;
                $scope.cadastro[$scope.conf.input[p].model] = '';
            }
        }
    };
    $scope.inicializarCadastro = function () {
        for (p in $scope.conf.input) {
            $scope.cadastro[$scope.conf.input[p].model] = '';
        }
    };
    //$scope.inicializarCadastro();
    $scope.resetForm = function () {
        $scope.cadastro = {};
    };
    $scope.consultarCep = function () {
        var cepOk = function (result) {
            if (typeof result.logradouro != 'undefined') {
                $scope.cadastro.endereco = result.logradouro;
                $scope.cadastro.bairro = result.bairro;
                $scope.cadastro.uf = result.uf;
                $scope.buscaCidade(result.uf);
                $scope.cadastro.cidade = result.localidade;
                $scope.cadastro.cep = result.cep;
            } else {
                alerta.show("CEP Inválido ou não encontrado", 'info', 2000);
                return;
            }
        };
        if (typeof $scope.cadastro.cep != 'undefined') {
            cep.consultar($scope.cadastro.cep).success(cepOk);
        }
    };
    $scope.validarCnpj = function () {
        if (typeof $scope.cadastro.cnpj != 'undefined') {
            if (!cnpj.consultar($scope.cadastro.cnpj)) {
                alerta.show("CNPJ inválido", 'error', 2000);
                $scope.cadastro.cnpj = '';

            }

        }
    };
    $scope.saveForm = function () {
        Validar.form('addEdit', function (r) {
            if (r) {
                empresa.cadastrar($scope.cadastro).success(function (result) {
                    if (typeof result.error == 'undefined') {
                        var codempresa = parseInt(result);
                        var pasta = "empresas/" + codempresa;
                        anexo.carregar(pasta, function (result) {
                            if (result.nome != '') {
                                var empresaC = {codempresa: codempresa, logo: pasta + "/" + result.nome};
                                empresa.updateImagem(empresaC).success(function (result) {
                                    if (result) {
                                        alerta.show("Empresa cadastrada.", 'success', 2000);
                                    } else {
                                        alerta.show("Falha ao vincular logo à empresa cadastrada. Erro: " + result.error, 'info', 2000);
                                    }
                                });
                            } else {
                                alerta.show("Empresa cadastrada com falha no carregamento do logo. Erro: " + result.error, 'info', 2000);
                            }
                        });
                    } else {
                        alerta.show("Falha ao cadastrar empresa. Erro: " + result.error, 'error', 2000);
                    }
                });
            }
        });
    };
    $scope.atualizarForm = function () {
        Validar.form('addEdit', function (r) {
            if (r) {
                $scope.cadastro.codusuariocadastro = codUsuario;
                var logoAntigo = ($scope.cadastro.logo) ? $scope.cadastro.logo : false;
                empresa.atualizar($scope.cadastro).success(function (result) {
                    if (typeof result.error == 'undefined') {
                        var codempresa = $scope.cadastro.codempresa;
                        var pasta = "empresas/" + codempresa;
                        if ($scope.uploader.queue.length) {
                            var parametrosArquivo = {_c: 'Upload',
                                _f: 'carregarImagem',
                                _p: {pasta: pasta, codempresa: codempresa}};
                            anexo.carregar(parametrosArquivo, function (resultArquivo) {
                                if (result.nome != '') {
                                    if (result) {
                                        if (logoAntigo) {
                                            anexo.remover(logoAntigo).success(function (result) {
                                                console.log(result);
                                            });
                                        }
                                        $scope.inicializarEmpresa(getEmpresa);
                                        alerta.show("Empresa atualizada.", 'success', 2000);
                                        
                                    } else {
                                        alerta.show("Falha ao vincular logo à empresa cadastrada. Erro: " + result.error, 'info', 2000);
                                    }
                                } else {
                                    alerta.show("Empresa cadastrada com falha no carregamento do logo. Erro: " + result.error, 'info', 2000);
                                }
                            });
                        } else {
                            alerta.show("Empresa atualizada.", 'success', 2000);
                            
                        }
                    } else {
                        alerta.show("Falha ao atualizar empresa. Erro: " + result.error, 'error', 2000);
                    }
                });
            }
        });
    };
    $scope.callFunction = function (funcao, p) {
        if (typeof funcao !== 'undefined' && typeof $scope[funcao] === 'function') {
            if (p) {
                return $scope[funcao](p);
            } else {
                return $scope[funcao]();
            }
        }
    };
});
gedApp.controller('cadastroAtualizacaoUsuaioController', function ($scope, $rootScope, $http, $stateParams, departamento, empresa, dataCustom, alerta, tipousuario, usuario, Validar) {
    $scope.lista = [{}];
    $scope.inicializarLista = function () {
        tipousuario.listar().success(function (result) {
            var lista = [];
            for (x in result) {
                if ($rootScope.usuario.codtipousuario !== 2 && result[x].codigo <= $rootScope.usuario.codtipousuario) {
                    lista.push(result[x]);
                } else if ($rootScope.usuario.codtipousuario === 2) {
                    lista.push(result[x]);
                }
            }
            $scope.radio = {listaTipo: lista};
        });
    };
    empresa.select().success(function (result) {
        if (Array.isArray(result)) {
            $scope.lista.empresas = result;
        } else {
            $scope.lista.empresas = [result];
        }
    });
    $scope.listarDepartamento = function (codempresa) {
        var codempresa = (codempresa) ? codempresa : $scope.cadastro.codempresa;
        departamento.listarSelect(codempresa).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.departamentos = result;
            } else {
                $scope.lista.departamentos = [result];
            }
        });
    };
    $scope.inicializarLista();
    $scope.cadastro = {};
    if ($stateParams.id) {
        $scope.titulo = $stateParams.acao;
        $scope.botao = [
            {label: 'ATUALIZAR', type: 'button', classe: "btn-success", icone: '', ngclick: 'atualizarForm'},
            {label: 'VOLTAR', type: 'reset', classe: "btn-danger", icone: '', ngclick: 'resetForm', link: "/ged/cadastros/usuario/listar"}
        ];
        usuario.getUsuario($stateParams.id).success(function (result) {
            $scope.cadastro = result;
            $scope.listarDepartamento(result.codempresa);
            delete $scope.cadastro.senha;
            $scope.cadastro.codusuariocadastro = $rootScope.usuario.codusuario;
        });
    } else {
        $scope.titulo = "Cadastro";
        $scope.botao = [
            {label: 'INCLUIR', type: 'button', classe: "btn-success", icone: '', ngclick: 'saveForm'},
            {label: 'VOLTAR', type: 'reset', classe: "btn-danger", icone: '', ngclick: 'resetForm', link: "/ged/cadastros/usuario/listar"}
        ];
        $scope.listarDepartamento($scope.cadastro.codempresa);
        $scope.cadastro.codtipousuario = 1;
        $scope.cadastro.codusuariocadastro = $rootScope.usuario.codusuario;
    }
    $scope.subTitulo = "Usuario";
    $scope.tamanho = 12;
    $scope.classe = "cadastro";
    $scope.conf = {
        input: [
            {type: 'text', tamanho: '10', model: "nome", label: 'Nome', disabled: false, quebraLinha: true, require: true},
            {type: 'email', tamanho: '5', model: "email", label: 'Email', disabled: false, quebraLinha: false, require: true},
            {type: 'password', tamanho: '5', model: "senha", label: 'Senha', disabled: false, quebraLinha: false, require: ($stateParams.id) ? false : true},
            {type: 'select', tamanho: '5', model: "codempresa", label: 'Empresa', list: "empresas", disabled: false, quebraLinha: false, ngchange: 'listarDepartamento', require: true},
            {type: 'select', tamanho: '5', model: "coddepartamento", label: 'Departamento', list: "departamentos", disabled: false, quebraLinha: true, require: true},
            {type: 'radio', tamanho: '10', model: "codtipousuario", label: 'Tipo Usuário', disabled: false, quebraLinha: true, require: true, radio: "listaTipo"},
        ]};
    $scope.obrigatorio = {};
    $scope.inicializarObrigatorio = function () {
        for (var p in $scope.conf.input) {
            if ($scope.conf.input[p].require) {
                $scope.obrigatorio[p] = $scope.conf.input[p].model;
                $scope.cadastro[$scope.conf.input[p].model] = '';
            }
        }
    };
    $scope.inicializarCadastro = function () {
        for (var p in $scope.conf.input) {
            $scope.cadastro[$scope.conf.input[p].model] = '';
        }
    };
    //$scope.inicializarCadastro();
    $scope.resetForm = function () {
        $scope.cadastro = {};
    };
    $scope.saveForm = function () {
        Validar.form('addEdit', function (r) {
            if (r) {
                usuario.cadastrar($scope.cadastro).success(function (result) {
                    if (typeof result.error == 'undefined') {
                        alerta.show("Usuário cadastrado com sucesso.", 'success', 2000);
                    } else {
                        alerta.show("Falha ao cadastrar usuário. Erro: " + result.error, 'error', 2000);
                    }
                });
            }
        });
    };
    $scope.atualizarForm = function () {
        Validar.form('addEdit', function (r) {
            if (r) {
                usuario.atualizar($scope.cadastro).success(function (result) {
                    if (typeof result.error == 'undefined') {
                        alerta.show("Usuário atualizado com sucesso.", 'success', 2000);
                    } else {
                        alerta.show("Falha ao atualizar usuário. Erro: " + result.error, 'error', 2000);
                    }
                });
            }
        });
    };
    $scope.callFunction = function (funcao) {
        if (typeof funcao != 'undefined' && typeof $scope[funcao] == 'function') {
            return $scope[funcao]();
        }
    }
});
gedApp.controller('cadastroAtualizacaoDepartamentoController', function ($scope, $rootScope, $stateParams, departamento, empresa, dataCustom, alerta, $filter, ngTableParams, Validar) {
    $scope.editItem = "/ged/cadastros/departamento/editar/";
    $scope.tbody = [{}];
    var qdtZeros = "0000";
    var qdtItens = 5;
    $scope.lista = {};
    empresa.select().success(function (result) {
        if (Array.isArray(result)) {
            $scope.lista.empresas = result;
        } else {
            $scope.lista.empresas = [result];
        }
    });
    $scope.inicializarTBody = function () {
        var empresaOk = function (result) {
            if (typeof result.msg === 'undefined') {
                $scope.tbody = result;
                if (Array.isArray(result)) {
                    $scope.tbody = result;
                } else {
                    $scope.tbody = [result];
                }
                for (var x in  $scope.tbody) {
                    $scope.tbody[x].coddepartamento = (qdtZeros + $scope.tbody[x].coddepartamento).slice(-4);
                    $scope.tbody[x].editar = $scope.tbody[x].coddepartamento;
                    $scope.tbody[x].excluir = $scope.tbody[x].coddepartamento;
                }
                $scope.colunas = Object.keys($scope.tbody[0]);
                var auxiliar = [];
                angular.forEach($scope.colunas, function (value, key) {
                    if (value !== 'editar' && value !== 'excluir') {
                        auxiliar.push({title: value, field: value, filter: {value: 'text'}, show: true, sortable: value});
                    } else {
                        auxiliar.push({title: value, field: value, show: true});
                    }
                });
                $scope.cols = auxiliar;
                $scope.ParamTable = new ngTableParams({
                    page: 1,
                    count: qdtItens
                }, {
                    counts: ($scope.tbody.length > qdtItens ? [qdtItens, $scope.tbody.length] : []),
                    total: $scope.tbody.length,
                    getData: function ($defer, params) {
                        $scope.data = params.sorting() ? $filter('orderBy')($scope.tbody, params.orderBy()) : $scope.tbody;
                        $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        $defer.resolve($scope.data);
                    }
                });
            } else {
                $scope.error = result;
            }
        };
        departamento.listarVw().success(empresaOk);
    };
    $scope.cadastro = {};
    $scope.cadastro.codusuariocadastro = $rootScope.usuario.codusuario;
    $scope.cadastro.ativo = 1;
    if ($stateParams.id) {
        $scope.titulo = $stateParams.acao;
        $scope.botao = [
            {label: 'ATUALIZAR', type: 'button', classe: "btn-success", icone: '', ngclick: 'atualizarForm'},
            {label: 'VOLTAR', type: 'reset', classe: "btn-danger", icone: '', ngclick: 'resetForm', link: "/ged/cadastros/departamento/novo"}
        ];
        $scope.btNovo = {link: "/ged/cadastros/departamento/novo", nome: "Novo"};
        departamento.listar($stateParams.id).success(function (result) {
            $scope.cadastro = result;
        });
    } else {
        $scope.titulo = "Cadastro";
        $scope.botao = [
            {label: 'INCLUIR', type: 'button', classe: "btn-success", icone: '', ngclick: 'saveForm'},
            {label: 'VOLTAR', type: 'reset', classe: "btn-danger", icone: '', ngclick: 'resetForm'}
        ];

    }
    $scope.subTitulo = "Departamento";
    $scope.tamanho = 12;
    $scope.classe = "cadastroListarDepartamento";
    $scope.conf = {
        input: [
            {type: 'text', tamanho: '10', model: "nome", label: 'Nome', disabled: false, quebraLinha: true, require: true},
            {type: 'select', tamanho: '10', model: "codempresa", label: 'Empresa', disabled: false, quebraLinha: false, require: true, list: 'empresas'}
        ]};
    $scope.obrigatorio = {};
    $scope.inicializarObrigatorio = function () {
        for (var p in $scope.conf.input) {
            if ($scope.conf.input[p].require) {
                $scope.obrigatorio[p] = $scope.conf.input[p].model;
                $scope.cadastro[$scope.conf.input[p].model] = '';
            }
        }
    };
    $scope.excluir = function (codigo) {
        alerta.confirm("Deseja remover o departamento? ", function (result) {
            if (result) {
                departamento.excluir(codigo).success(function () {
                    if (typeof result.error === 'undefined') {
                        alerta.show("Departamento removido com sucesso.", 'success', 2000);
                        $scope.inicializarTBody();
                    } else {
                        alerta.show("Falha ao remover o departamento. Erro: " + result.error, 'error', 2000);
                        $scope.inicializarTBody();
                    }
                });
            }
        });
    };
    $scope.inicializarCadastro = function () {
        for (var p in $scope.conf.input) {
            $scope.cadastro[$scope.conf.input[p].model] = '';
        }
    };
    //$scope.inicializarCadastro();
    $scope.resetForm = function () {
        $scope.cadastro = {};
    };
    $scope.saveForm = function () {
        Validar.form('addEdit', function (r) {
            if (r) {
                departamento.cadastrar($scope.cadastro).success(function (result) {
                    if (typeof result.error === 'undefined') {
                        alerta.show("Departamento cadastrado com Sucesso.", 'success', 2000);
                        $scope.inicializarTBody();
                        $scope.cadastro = {};  
                        $scope.cadastro.codusuariocadastro = $rootScope.usuario.codusuario;
                        $scope.cadastro.ativo = 1;                        
                    } else {
                        alerta.show("Falha ao cadastrar departamento. Erro: " + result.error, 'error', 2000);
                    }
                });
            }
        });
    };
    $scope.atualizarForm = function () {
        Validar.form('addEdit', function (r) {
            if (r) {
                departamento.atualizar($scope.cadastro).success(function (result) {
                    if (typeof result.error === 'undefined') {
                        alerta.show("Departamento atualizado com sucesso.", 'success', 2000);
                        $scope.inicializarTBody();
                    } else {
                        alerta.show("Falha ao atualizar departamento. Erro: " + result.error, 'error', 2000);
                    }
                });
            }
        });
    };
    $scope.callFunction = function (funcao) {
        if (typeof funcao !== 'undefined' && typeof $scope[funcao] === 'function') {
            return $scope[funcao]();
        }
    };
});
gedApp.controller('cadastroAtualizacaoTipoArquivoController', function ($scope, $rootScope, $stateParams, tipoArquivo, empresa, dataCustom, alerta, usuario, crud, Validar) {
    $scope.editItem = "/ged/cadastros/tipoarquivo/editar/";
    $scope.tbody = [];
    $scope.lista = {};
    $scope.lista.aprovacao = [];
    $scope.selectFaIndex = 0;
    $scope.SelectedAvailItems = [];
    $scope.SelectedSelectedListItems = [];
    $scope.SelectedListItems = [];
    $scope.listaPreAprovacao = function (codempresa) {
        usuario.listarPreAprovacao(codempresa).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.usuarioempresa = result;
            } else {
                $scope.lista.usuarioempresa = [result];
            }
        });
    };
    $scope.listaAprovacao = function (codarquivo) {
        tipoArquivo.listaVwAprovacao(codarquivo).success(function (result) {
            if (Array.isArray(result)) {
                $scope.lista.aprovacao = result;
            } else {
                $scope.lista.aprovacao = [result];
            }
            if (!$scope.lista.aprovacao[0].msg) {
                var c = $scope.lista.usuarioempresa.filter(function (item) {
                    return !$scope.lista.aprovacao.some(function (other) {
                        if ((item.codigo === other.codigo && other.codtipoarquivo === codarquivo)) {
                            return item;
                        }
                    });
                });
                $scope.lista.usuarioempresa = c;
            } else {
                $scope.lista.aprovacao = [];
            }
        });
    };
    $scope.btnRight = function (SelectedAvailItems) {
        angular.forEach(SelectedAvailItems, function (value, key) {
            // this.push(value);
        },
                $scope.lista.aprovacao[$scope.selectFaIndex]);
        angular.forEach(SelectedAvailItems, function (value, key) {
            for (var i = $scope.lista.usuarioempresa.length - 1; i >= 0; i--) {
                if ($scope.lista.usuarioempresa[i].codigo === value.codigo) {
                    $scope.lista.aprovacao.push($scope.lista.usuarioempresa[i]);
                    $scope.lista.usuarioempresa.splice(i, 1);
                }
            }
        });
    };
    $scope.btnLeft = function (SelectedSelectedListItems) {
        //move selected.
        console.log(SelectedSelectedListItems);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
        },
                $scope.lista.usuarioempresa[$scope.selectFaIndex]);
        angular.forEach(SelectedSelectedListItems, function (value, key) {
            for (var i = $scope.lista.aprovacao.length - 1; i >= 0; i--) {
                if ($scope.lista.aprovacao[i].codigo === value.codigo) {
                    $scope.lista.usuarioempresa.push($scope.lista.aprovacao[i]);
                    $scope.lista.aprovacao.splice(i, 1);
                }
            }
        });
    };
    empresa.select().success(function (result) {
        if (Array.isArray(result)) {
            $scope.lista.empresas = result;
        } else {
            $scope.lista.empresas = [result];
        }
    });
    $scope.cadastro = {};
    $scope.cadastro.codusuariocadastro = $rootScope.usuario.codusuario;
    $scope.cadastro.ativo = 1;
    if ($stateParams.id) {
        $scope.titulo = $stateParams.acao;
        $scope.botao = [
            {label: 'ATUALIZAR', type: 'button', classe: "btn-success", icone: '', ngclick: 'atualizarForm'},
            {label: 'VOLTAR', type: 'reset', classe: "btn-danger", icone: '', ngclick: 'resetForm', link: "/ged/cadastros/tipoarquivo/listar"}
        ];
        tipoArquivo.getTipoAquivo($stateParams.id).success(function (result) {
            console.log(result);
            $scope.cadastro = result;
            $scope.cadastro.codempresa = result.codempresa;
            $scope.iscodempresa = (result.codempresa > 0);
            $scope.cadastro.codtipoarquivo = result.codtipoarquivo;
            $scope.listaPreAprovacao(result.codempresa);
            $scope.listaAprovacao(result.codtipoarquivo);
            $scope.conf = {
                input: [
                    {type: 'text', tamanho: '10', model: "descricao", label: 'Tipo de Arquivo', disabled: false, quebraLinha: true, require: true},
                    {type: 'select', tamanho: '10', model: "codempresa", label: 'Empresa', disabled: true, quebraLinha: true, ngchange: "listaPreAprovacao", list: 'empresas'},
                    {type: 'depara', tamanho: '10', quebraLinha: true, listaA: 'usuarioempresa', listaB: 'aprovacao', ngclick: 'moveUser'},
                    {type: 'checkbox', tamanho: '10', model: "fixo", label: 'Fixo', disabled: false, quebraLinha: true}
                ]};
            $scope.cadastro.codusuariocadastro = $rootScope.usuario.codusuario;
        });
    } else {
        $scope.conf = {
            input: [
                {type: 'text', tamanho: '10', model: "descricao", label: 'Tipo de Arquivo', disabled: false, quebraLinha: true, require: true},
                {type: 'select', tamanho: '10', model: "codempresa", label: 'Empresa', disabled: false, quebraLinha: true, require: true, ngchange: "listaPreAprovacao", list: 'empresas'},
                {type: 'depara', tamanho: '10', quebraLinha: true, listaA: 'usuarioempresa', listaB: 'aprovacao', ngclick: 'moveUser'},
                {type: 'checkbox', tamanho: '10', model: "fixo", label: 'Fixo', disabled: false, quebraLinha: true}
            ]};
        $scope.titulo = "Cadastro";
        $scope.botao = [
            {label: 'INCLUIR', type: 'button', classe: "btn-success", icone: '', ngclick: 'saveForm'},
            {label: 'VOLTAR', type: 'reset', classe: "btn-danger", icone: '', ngclick: 'resetForm', link: "/ged/cadastros/tipoarquivo/listar"}
        ];

    }
    $scope.subTitulo = "Tipo de Arquivo";
    $scope.tamanho = 12;
    $scope.classe = "cadastroListarTipoArquivo";
    $scope.botao_duallist = [
        {label: 'add', type: 'button', classe: "btn btn-default btn-sm move-right", icone: 'glyphicon glyphicon-chevron-right', ngclick: 'addLista'},
        {label: 'remove', type: 'button', classe: "btn btn-default btn-sm move-left", icone: 'glyphicon glyphicon-chevron-left', ngclick: 'resetForm', link: "/ged/cadastros/tipoarquivo/listar"}
    ];
    $scope.obrigatorio = {};
    $scope.inicializarObrigatorio = function () {
        for (var p in $scope.conf.input) {
            if ($scope.conf.input[p].require) {
                $scope.obrigatorio[p] = $scope.conf.input[p].model;
                $scope.cadastro[$scope.conf.input[p].model] = '';
            }
        }
    };
    $scope.inicializarCadastro = function () {
        for (var p in $scope.conf.input) {
            $scope.cadastro[$scope.conf.input[p].model] = '';
        }
    };
    $scope.resetForm = function () {
        $scope.cadastro = {};
    };
    $scope.saveForm = function () {
        Validar.form('addEdit', function (r) {
            if (r) {
                tipoArquivo.cadastrar($scope.cadastro).success(function (result) {
                    $scope.cadastro = {};  
                    $scope.cadastro.codusuariocadastro = $rootScope.usuario.codusuario;
                    $scope.cadastro.ativo = 1;
                    if (typeof result.error == 'undefined') {
                        var codtipoarquivo = parseInt(result);
                        alerta.show("Tipo de arquivo cadastrado!", 'success', 2000);
                        if (typeof $scope.lista.aprovacao !== 'undefined' && $scope.lista.aprovacao.length > 0) {
                            
                            angular.forEach($scope.lista.aprovacao, function (value, key) {
                                var preAprovacao = {codusuario: value.codigo, codtipoarquivo: codtipoarquivo};
                                tipoArquivo.cadastrar_preaprovacao(preAprovacao, function (result) {
                                    //if (result) {
                                        
                                    //} else {
                                      //  alerta.show("Falha ao vincular o tipo de arquivo nos usuarios de pré aprovação. Erro: " + result.error, 'info', 2000);
                                    //}
                                });
                            });
                        } else {
                            //alerta.show("Tipo de arquivo cadastrado.", 'success', 2000);
                        }
                    } else {
                        alerta.show("Falha ao cadastrar o tipo de arquivo. Erro: " + result.error, 'error', 2000);
                    }
                });
            }
        });
    };
    $scope.atualizarForm = function () {
        Validar.form('addEdit', function (r) {
            if (r) {
                var qtd = $scope.lista.aprovacao.length;
                var count = 1;
                tipoArquivo.atualizar($scope.cadastro).success(function (result) {
                    if (typeof result.error === 'undefined') {
                        var codtipoarquivo = $scope.cadastro.codtipoarquivo;
                        var excluiArquivo = {codtipoarquivo: codtipoarquivo};
                        tipoArquivo.excluir(excluiArquivo).success(function (removido) {
                            angular.forEach($scope.lista.aprovacao, function (value, key) {
                                var preAprovacao = {codusuario: value.codigo, codtipoarquivo: codtipoarquivo};
                                count++;
                                tipoArquivo.cadastrar_preaprovacao(preAprovacao).success(function (result) {
                                    if (qtd == count) {
                                        if (result) {
                                            alerta.show("Tipo de arquivo atualizado.", 'success', 2000);
                                        } else {
                                            alerta.show("Falha ao vincular o tipo de arquivo nos usuarios de pré aprovação. Erro: " + result.error, 'info', 2000);
                                        }
                                    }
                                });
                            });
                            if (result) {
                                alerta.show("Tipo de arquivo atualizado.", 'success', 2000);
                            } else {
                                alerta.show("Falha ao vincular tipo de arquivo nos usuarios de pré aprovação. Erro: " + result.error, 'info', 2000);
                            }
                        });
                    } else {
                        alerta.show("Falha ao atualizar o tipo de arquivo. Erro: " + result.error, 'error', 2000);
                    }
                });
            }
        });
    };
    $scope.callFunction = function (funcao, p) {
        if (typeof funcao !== 'undefined' && typeof $scope[funcao] === 'function') {
            if (p) {
                return $scope[funcao](p);
            } else {
                return $scope[funcao]();
            }
        }
    };
});