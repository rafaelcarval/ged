<?php
header('Content-Type: text/html; charset=utf-8');

$login_auto = "";

if (isset($_GET['usr'])){
	$conn = mysqli_connect("144.22.86.151", "root", "Co@;-sbzWlIQ+l7fkFugi%@z<8wZjK8m", 'db_ged');
	
	$md5= $_GET['usr'];
	$sql = "SELECT * FROM `ged_usuario` WHERE MD5(email) = '$md5'";
	$stmt = mysqli_query($conn, $sql);
	$rs = mysqli_fetch_object($stmt);
	
	
	$login_auto = "ng-init=\"login_automatico('$rs->email','$rs->senha')\"";;
}

print_r($_SESSION);

?>

<html lang="pt-br" ng-app="gedApp" ng-cloak>
    <head>
        <base href="/">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title ng-controller="menuController" >GED | {{empresa.razao}} </title>
        <link href="ged/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="ged/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="ged/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="ged/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="ged/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="ged/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="ged/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="ged/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="ged/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="ged/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="ged/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="ged/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link rel='stylesheet' href='ged/vendors/angular/loading-bar/loading-bar.min.css' type='text/css' media='all' />
        <!-- Custom Theme Style -->
        <link href="ged/build/css/custom.css" rel="stylesheet">
        <link href="ged/build/css/ng-animation.css" rel="stylesheet">
        <link href="ged/build/css/toastr.css" rel="stylesheet">
        <link href="ged/build/css/ng-table.min.css" rel="stylesheet">
    </head>
    <body class="nav-md {{usuario.cor}}">
        <div class="container body">
            <div class="main_container">
                <div ng-if="loginRoot" class="bouncy-slide-down">
                    <div ui-view="menu_lateral"></div>
                    <div ui-view="nav_bar"></div>
                    <div class="right_col" role="main" style="overflow: auto;">
                        <div ui-view="palco" class="flip-in"></div>
                    </div>
                    <div ui-view="footer"></div>
                </div>
                <div ng-if="!loginRoot" class="bouncy-slide-down">
                    <div class="login" ng-controller="menuController" <?=$login_auto?> >
                        <a class="hiddenanchor" id="signup"></a>
                        <a class="hiddenanchor" id="signin"></a>
                        <div class="login_wrapper">
                            <div class="animate form login_form">
                                <div class="panel panel-default sombra top-radius bottom-radius">
                                    <div class="panel-heading gradiente-cinza top-radius">
                                        <img ng-src="{{empresa.logo}}" alt="{{empresa.razao}}" width="300px"/>
                                        <h2>Entrar</h2>
                                    </div>
                                    <div class="panel-body">
                                        <form>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon  padding-icone"><i class="fa fa-user" aria-hidden="true"></i></div>
                                                    <input type="text" class="form-control" placeholder="Usuário" required="" ng-model="loginN.email" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></div>
                                                    <input type="password" class="form-control" placeholder="Senha" required="" ng-model="loginN.senha" ng-enter="login(loginN)" />
                                                </div>
                                            </div>
                                            <small class="pull-left"><small>(Acesso restrito e monitorado)</small></small>
                                            <button type="submit" id="bt-entrar" class="btn btn-primary pull-right" ng-disabled="!loginN.email && !loginN.senha" ng-click="login(loginN)">Entrar</button>
                                        </form>
                                    </div>
                                    <div class="panel-footer bottom-radius gradiente-azul">
                                        GED - Gestão Eletrônica de Documentos<br/>
                                        by <a href="http://www.safetydocs.com.br/" target="_blank">Safety Docs</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <!-- jQuery -->
        <script type="text/javascript" src="ged/vendors/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="ged/vendors/PDFObject/pdfobject.min.js" ></script>
        <!-- Bootstrap -->
        <script src="ged/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="ged/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="ged/vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="ged/vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="ged/vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="ged/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="ged/vendors/iCheck/icheck.min.js"></script>
        <!-- Skycons -->
        <script src="ged/vendors/skycons/skycons.js"></script>
        <!-- Flot -->
        <script src="ged/vendors/Flot/jquery.flot.js"></script>
        <script src="ged/vendors/Flot/jquery.flot.pie.js"></script>
        <script src="ged/vendors/Flot/jquery.flot.time.js"></script>
        <script src="ged/vendors/Flot/jquery.flot.stack.js"></script>
        <script src="ged/vendors/Flot/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="ged/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="ged/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="ged/vendors/flot.curvedlines/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="ged/vendors/DateJS/build/date.js"></script>
        <!-- JQVMap -->
        <script src="ged/vendors/jqvmap/dist/jquery.vmap.js"></script>
        <script src="ged/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="ged/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="ged/vendors/moment/min/moment.min.js"></script>
        <script src="ged/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- Custom Theme Scripts -->
        <!-- Angular -->
        <script src="ged/vendors/angular/angular.min.js"></script>
        <script src="ged/vendors/angular/angular-animate.min.js" type="text/javascript"></script>
        <script src="ged/vendors/angular/angular-ui-router.min.js"></script>
        <script src="ged/vendors/angular/angular-file-upload.min.js" type="text/javascript"></script>
        <script src="ged/vendors/angular/angular-sanitize.js" type="text/javascript"></script>
        <script src="ged/vendors/angular/ngDialog.min.js" type="text/javascript"></script>
        <script src="ged/vendors/angular/ngMask.min.js" type="text/javascript"></script>
        <script src="ged/vendors/angular/mask.js" type="text/javascript"></script>
        <script src="ged/vendors/angular/toastr.js" type="text/javascript"></script>
        <script type='text/javascript' src='ged/vendors/angular/loading-bar/loading-bar.min.js'></script>
        <!--<script src="vendors/angular/ng-table.min.js" type="text/javascript"></script>-->    
        <script src="ged/js/bootbox.min.js" type="text/javascript"></script>
        <script src="ged/js/app.js" type="text/javascript"></script>
        <script src="ged/js/factory.js?v1" type="text/javascript"></script>
        <script src="ged/js/service.js" type="text/javascript"></script>
        <script src="ged/js/directives.js" type="text/javascript"></script>
        <script src="ged/js/controller.js?v1" type="text/javascript"></script>
        <script src="ged/js/controllerAddEdit.js" type="text/javascript"></script>
        <script src="ged/js/controllerListar.js" type="text/javascript"></script>
        <script src="ged/vendors/angular/ng-table_novo.min.js" type="text/javascript"></script>
        <script src="ged/vendors/angular/ng-table-to-csv.min.js" type="text/javascript"></script>
        <!-- Custom Theme Scripts -->
        <script src="ged/build/js/custom-teste.js"></script>
        <!-- iCheck -->
        <script src="ged/vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="ged/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="ged/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="ged/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="ged/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="ged/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="ged/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="ged/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="ged/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="ged/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="ged/vendors/jszip/dist/jszip.min.js"></script>
        <script src="ged/vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="ged/vendors/pdfmake/build/vfs_fonts.js"></script>
        <script src="ged/vendors/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="ged/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="ged/vendors/nprogress/nprogress.js"></script>
        <!-- FastClick -->
        <script src="ged/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="ged/vendors/nprogress/nprogress.js"></script>
        <script src="ged/vendors/angular/ui-contextMenu/contextMenu.js" type="text/javascript"></script>
    </body>
</html>