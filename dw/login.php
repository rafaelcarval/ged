<?php
session_start();

$usuario = array(
            ['codusuario'] => 39,
            ['codempresa'] => 103,
            ['coddepartamento'] => 16,
            ['nome'] => 'Talita Bidim',
            ['email'] => 'talita.bidim@safetydocs.com.br',
            ['dtcadastro'] => date("Y-m-d H:i:s"),
            ['dtalteracao'] => date("Y-m-d H:i:s"),
            ['codtipousuario'] => 4,
            ['codusuariocadastro'] => 33,
            ['ativo'] => 1,
            ['codarvore'] => 137,
            ['cor'] => 'tema-4',
            ['empresaMaster'] => 0
        );


$_SESSION['seesaoUsuario'] = (object)$usuario;

header("Location: http://ged.safetydocs.com.br/home");
exit;