<?php
//require_once("../conexao.php");
include_once('../php/conexoes/config/ConfigMysql.php');
include_once("./downloadfileclass.inc");

$link = mysqli_connect(ConfigMysql::getServer(), ConfigMysql::getUser(), ConfigMysql::getPass(), ConfigMysql::getData());

if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$f = $_GET['f'];

$dt = date("Ymd");

$sql = "SELECT e.codenvio, f.* FROM `ged_arquivoenviado` e inner join ged_arquivo f on f.codarquivo = e.codarquivo  WHERE MD5(CONCAT('$dt#', codenvio)) = '$f'";
$stmt = mysqli_query($link, $sql);
$rs   = mysqli_fetch_object($stmt);

if ($rs->codenvio == 0){
    die("Link já expirado!");
}

$m_arquivo  = "../".$rs->caminho . "/".$rs->nome;

$downloadfile = new DOWNLOADFILE($m_arquivo, $rs->nome);

//echo "<hr>".$m_arquivo."<hr>";

if (!$downloadfile->df_download()) echo "Desculpe, houve um erro no download deste item. Por favor tente mais tarde.";

