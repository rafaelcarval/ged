<?php

require __DIR__ . '/src/BuscaViaCEP_inc.php';

class ConsultarCep {

    private $cep = 0, $array_tipos = array();

    public function __construct($cep) {
        $this->cep = $cep;
        $this->array_tipos = ['Querty', 'Piped', 'JSON', 'JSONP', 'XML'];
    }

    public function getEndereco() {
        $consultaCep = new Jarouche\ViaCEP\BuscaViaCEPJSONP();
        $retorno = $consultaCep->retornaCEP($this->cep);
        return $retorno;
    }

}
