<?php

class ListarDepartamentos {

    private $listar;

    public function __construct() {
        $this->listar = new CrudRegistro(null, 'ged_vw_departamento');
    }

    public function listar($where = null) {
        $where = (is_object($where)) ? $where : null;
        
       
        @$usuario = Sessao::getSession();        
        if ($usuario->codtipousuario != 4){
            if ($where == null){
                $where = (object) array("codempresa"=> $usuario->codempresa);
            }else{
                $where->codempresa =  $usuario->codempresa;
            }
        }

        
        $lista = $this->listar->listar($where);
        $listaDepto = array();
        if (is_object($lista)) {
            $listaDepto = array($lista);
        } else {
            $listaDepto = $lista;
        }
        
        
        
        

        return Helpers::removerItemObjeto($listaDepto, 'codempresa');
    }

}
