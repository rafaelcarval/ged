<?php

# The following example builds the X-SMTPAPI headers and adds them to swiftmailer. [Swiftmailer](http://swiftmailer.org/) then sends the email through SendGrid. You can use this same code in your application or optionally you can use [sendgrid-php](http://github.com/sendgrid/sendgrid-php).


require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/lib/Smtpapi.php';
require_once dirname(__DIR__) . '/lib/Smtpapi/Header.php';

use Smtpapi\Header;

if (is_array($from)) {
    $from_name = $from[1];
    $from = $from[0];
} else {
    $from_name = $from;
}

if (!is_array($to)) {
    $to = array($to);
}

if ($bcc) {
    if (!is_array($bcc)) {
        $bcc = array($bcc);
    }
}

$transport = \Swift_SmtpTransport::newInstance('smtp.sendgrid.net', 587);

$sendgrid_username = 'proxyerp2016'; //getenv('SENDGRID_USERNAME');
$sendgrid_password = 'Pbrfah822jnHO61W63zi'; //getenv('SENDGRID_PASSWORD');


$transport->setUsername($sendgrid_username);
$transport->setPassword($sendgrid_password);

$mailer = \Swift_Mailer::newInstance($transport);

$message = new \Swift_Message();
//$message->setTo(array('hugo@proxy.com.br', 'pedro@proxy.com.br', 'cesar@proxy.com.br',  'rodrigo.vieira@proxy.com.br', 'andre@proxy.com.br', 'lucas@proxy.com.br', 'mateus@proxy.com.br'));
//$message->setFrom('financeiro@proxy.com.br');
$message->setTo($to);
// $message->addTo($to, $to);

if (is_array($bcc)){
    $message->setBcc($bcc);
}

//$message->setFrom($from);
$message->addFrom($from, $from);
$message->setSubject($subject);
$message->setContentType('text/html');
////$message->setBody('<HR>TESTANDO E-MAIL DO proxy.com.br<br><br><br>Respondam APENAS para o remetente!!!!<HR>'); 
$message->setBody($msg);

if (!empty($anexo)) {
    if (is_array($anexo)) {

        for ($i = 0; $i < count($anexo); $i++) {
            $anexo_arquivo = $anexo[$i];
            $message->attach(Swift_Attachment::fromPath($anexo_arquivo));
        }
    } else {
        $message->attach(Swift_Attachment::fromPath($anexo));
    }
}




$header = new Header();
//$header->addSubstitution('%how%', array('WTf'));

$header->addCategory($categoria);

$message_headers = $message->getHeaders();
$message_headers->addTextHeader(HEADER::NAME, $header->jsonString());

try {
    $response = $mailer->send($message);
    //print_r($response);
} catch (\Swift_TransportException $e) {
	//echo 'bad connection';
    //print_r($e);
}