<?php

class Arvore {

    private $crud,
            $crud2,
            $raiz = array();

    public function __construct($arvore) {
        if (is_object($arvore)) {
            $this->crud = new CrudRegistro(null, 'ged_vw_arvore');
            $obj = new stdClass();
            $obj->codusuario = 10;
            $this->crud2 = new CrudRegistro($obj, 'ged_vw_arvore');
            $raiz = array();
            if ($arvore->codpai == 0) {
                $listaRaizEmpresa = $this->crud->listar($arvore, null, null, null, "titulo");
                $listaRaizEmpresa = (is_array($listaRaizEmpresa)) ? $listaRaizEmpresa : array($listaRaizEmpresa);
                $raiz = array();
                foreach ($listaRaizEmpresa as $key => $raizEmpresa) {
                    $this->montarRaiz((object) array('codpai' => $raizEmpresa->codarvore), $raiz[]);
                }
                $this->raiz = $raiz;
            } else {
                $this->montarRaiz($arvore, $this->raiz);
            }
        }
    }

    public function listar() {
        return (is_array($this->raiz)) ? $this->raiz : array($this->raiz);
    }

    private function montarRaiz($codpai, &$raiz = false) {
        $pai = $this->crud->listar((object) array('codarvore' => $codpai->codpai), null, null, null, "titulo");
        if (is_object($pai)) {
            $raiz = (array) $pai;
            $jj = $this->crud->listar($codpai, null, null, null, "titulo");
            $filhos = (is_array($jj)) ? $jj : array($jj);
            foreach ($filhos as $f) {
                if (is_object($f)) {
                    $this->montarRaiz((object) array('codpai' => $f->codarvore), $raiz['itens'][]);
                }
            }
        }
    }

}
