<?php

extract($_GET);
include("AutoLoad.php");
require_once("download/downloadfileclass.inc");

if ($_SERVER["SERVER_ADDR"] != "127.0.0.1") {
    define('sCaminhoNormal', "/opt/bitnami/apache2/htdocs/ged//");
} else {
    define('sCaminhoNormal', "/Users/mateusbueno/Sites/ged/");
}

$iCod = base64_decode($_arq);
$crud = new CrudRegistro(null, 'ged_arquivo');
$filtro = (object) array('codarquivo' => (int) $iCod);
$oRet = $crud->listar($filtro);

$sFile = sCaminhoNormal . $oRet->caminho . $oRet->nome;

/** Instancia o objeto para executar o download. */
$oDF = new DOWNLOADFILE($sFile, $sMimetype, "attachment", $sTitle);

/** Se o download falhar, exibe mensagem de erro. */
if (!$oDF->df_download()) {
    $sHtml .= '<h4>Falha no download</h4>';
    $sHtml .= '<p>Desculpe, houve um erro no download deste item. Caso o problema persista, entre em contato conosco.</p>';
    $sHtml .= '<p style="font-weight:bold; color:#036;">';
    $sHtml .= 'Arquivo: ' . sCaminhoNormal . $oRet->caminho . $oRet->nome;
    if ("S" == $_REQUEST['_showdir']) {
        $sHtml .= '<br />File: ' . $sFile;
    }
    $sHtml .= '</p>';
    echo $sHtml;
}

exit;
