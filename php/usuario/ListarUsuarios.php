<?php

class ListarUsuarios {

    private $listar;

    public function __construct() {
        $this->listar = new CrudRegistro(null, 'ged_vw_usuario');
    }

    public function listar($where = null) {
        $where = (is_object($where)) ? $where : null;
        
        
        @$usuario = Sessao::getSession();
        if ($usuario->codtipousuario != 4){
            if ($where == null){
                $where = (object) array("codempresa"=> $usuario->codempresa);
            }else{
                $where->codempresa =  $usuario->codempresa;
            }
        }
        
        $lista = $this->listar->listar($where);
        $listaUsuario = array();
        if (is_object($lista)) {
            $listaUsuario = array($lista);
        } else {
            $listaUsuario = $lista;
        }
        
        
        
        

        return Helpers::removerItemObjeto($listaUsuario, 'codempresa');
    }

}
