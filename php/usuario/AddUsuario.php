<?php

class AddUsuario {

    private $usuario;

    public function __construct($usuario) {
        $this->usuario = $usuario;
        $this->sql = new ComandosSql();
    }

    public function salvar() {
		
        if (is_array($this->usuario)) {
            foreach ($this->usuario as $usuario) {
                $saida = $this->criarPessoa($usuario);
            }
        } else {
            $saida = $this->criarPessoa($this->usuario);
        }
        return $saida;
    }

    private function criarPessoa($usuario) {
        $colunasPessoa = array();
        $colunasValoresPessoa = array();
        foreach ($this->usuario as $key => $dado) {
            $colunasPessoa[] = $key;
            $colunasValoresPessoa[] = $dado;
        }

        $criar = $this->sql->insert('ged_usuario', $colunasPessoa, $colunasValoresPessoa);
        if ($criar) {
            $saida = TRUE;
        } else {
            $saida = array('msg' => 'Falha ao cadastrar cargo');
        }
        return $saida;
    }

}
