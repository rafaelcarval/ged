<?php

/**
 * -------------------------------------------------------------------------------
 * Classe HtmlBuffer 2.0
 * 	Formatação e bufferização de estrutura HTML para PHP
 * -------------------------------------------------------------------------------
 * Criado em....: 07/08/2007
 * Alterado em..: 21/02/2018 por Mateus Bueno
 * -------------------------------------------------------------------------------
 * Changelog
 * 
 * 01/04/2008
 * 	- Foi adicionada uma nova funcionalidade para o método 'set'. Primeiramente
 * 	  ele aceitava dois parâmetros: o HTML e a indentação. Agora é possível
 * 	  informar mais parâmetros para serem utilizados com a função 'sprintf'.
 * 	  Isso quer dizer que, à partir do terceiro parâmetro, a função 'sprintf'
 * 	  será utilizada no HTML. Exemplos de uso:
 * 		- $oH->set('<a href="%s">%s</a>', 1, "http://www.google.com/", "Google");
 * 		- $oH->set('<a href="%s">%s</a>', 1, array("http://www.google.com/", "Google"));
 * 
 * 17/06/2008
 * 	- Adicionada uma alternativa para os 'replacements'. Agora é possível
 * 	  escolher se você deseja utilizar o 'sprintf' ou a função '_template':
 * 		- sprintf:
 * 		  $oH->set('WWW: %s Wide %s', 0, "World", "Web");
 * 		
 * 		- _template:
 * 		  $oH->set('WWW: #{0} Wide #{1}', 0, "World", "Web");
 * 		  $oH->set('WWW: #{w1} Wide #{w2}', 0, array("w1" => "World", "w2" => "Web"));
 * 
 * 21/02/2018
 *  - Acertos de identação
 *  - Criação da function __construct, seguindo os padrões das novas versões do PHP
 * -------------------------------------------------------------------------------
 */
define("REPLACE_MODE_TEMPLATE", "template");
define("REPLACE_MODE_SPRINTF", "sprintf");

class HtmlBuffer {

    var $sVersion;
    var $aHtml;
    var $aConfig;
    var $iCtID;

    public function __construct() {
        $this->sVersion = "2.0";
        $this->aHtml = array();
        $this->aConfig = array(
            "indent" => "  ",
            "superIndent" => "",
            "echoMode" => false,
            "compactHTML" => false,
            "replaceMode" => REPLACE_MODE_TEMPLATE
        );
        $this->iCtID = 0;
    }

    function getVersion() {
        return $this->sVersion;
    }

    function configure($sName, $sValue) {
        if (array_key_exists($sName, $this->aConfig)) {
            $this->aConfig[$sName] = $sValue;
        }
    }

    function configureMany($aOptions) {
        foreach ($aOptions as $sName => $sValue) {
            $this->configure($sName, $sValue);
        }
    }

    //---------------------------------------------------------------------------

    function set($sHtml, $mIndent = 0) {
        $sId = $this->getId($sHtml);
        $iIndent = (is_string($mIndent) && strlen($mIndent) > 0 && $this->hasElement($mIndent)) ? $this->getElementById($mIndent, "indent") : $mIndent;
        if (2 < func_num_args()) {
            if (3 == func_num_args() && is_array(func_get_arg(2))) {
                $aArgs = func_get_arg(2);
            } else {
                $aArgs = func_get_args();
                array_shift($aArgs);/** Remove o HTML. */
                array_shift($aArgs);/** Remove o índice de identação. */
            }
            if (REPLACE_MODE_TEMPLATE == $this->aConfig['replaceMode']) {
                $sHtml = _template($sHtml, $aArgs);
            } else {
                array_unshift($aArgs, $sHtml);/** Adiciona o HTML como primeiro elemento. */
                $sHtml = call_user_func_array("sprintf", $aArgs);
            }
        }
        $this->aHtml[$sId] = array("html" => $sHtml, "indent" => $iIndent);
    }

    function setMany($aElements) {
        foreach ($aElements as $aElement) {
            $this->set($aElement[0], $aElement[1]);
        }
    }

    function blank() {
        $this->aHtml[$this->getId()] = array("html" => "", "indent" => 0);
    }

    function get($aParser = array(), $bClear = true) {
        $sHtml = null;
        $aHtml = array();
        if ($this->aConfig['compactHTML']) {
            foreach ($this->aHtml as $aLine) {
                $aHtml[] = $aLine['html'];
            }
            $sHtml = implode("", $aHtml);
        } else {
            foreach ($this->aHtml as $aLine) {
                $aHtml[] = $this->getIndent($aLine['indent']) . $aLine['html'];
            }
            $sHtml = "\n" . implode("\n", $aHtml) . "\n";
        }

        if (0 < count($aParser)) {
            $sHtml = $this->parseHTML($sHtml, $aParser);
        }
        if ($bClear) {
            $this->clear();
        }
        if ($this->aConfig['echoMode']) {
            echo $sHtml;
        }
        return $sHtml;
    }

    function parseHTML($sHtml, $aVars) {
        foreach ($aVars as $sIndex => $sValue) {
            $sHtml = str_replace("#{{$sIndex}}", $sValue, $sHtml);
        }
        return $sHtml;
    }

    function clear() {
        $this->aHtml = array();
    }

    function viewSource($bBeautify = true) {
        $bEchoMode = $this->aConfig['echoMode'];
        $this->configure("echoMode", false);
        if ($bBeautify) {
            $sEvents = ' style="cursor:pointer;" onclick="with(this.childNodes[0].style){ color=\'#FFF\'; backgroundColor=\'#069\'; }" ondblclick="with(this.childNodes[0].style){ color=\'#999\'; backgroundColor=\'transparent\'; }" onmouseover="this.style.backgroundColor=\'#FFC\';" onmouseout="this.style.backgroundColor=\'transparent\';"';
        }
        $sQuadro = '<div style="font:normal 12px Courier New, serif; color:#000; background-color:#FAFAFA; border:1px solid #CCC; position:relative; width:auto; height:auto; padding: 10px; margin:15px 6px;"><b style="font-size:16px; border-bottom:1px solid #DFDFDF; display:block; margin-bottom:8px;">' . strtoupper(__CLASS__) . ' - View Source</b>%s</div>';
        $sHtml = $this->get(array(), false);
        $sHtml = str_replace(array(" ", "\t", "\r\n", "\n"), array("&nbsp;", "&nbsp;&nbsp;&nbsp;&nbsp;", "<br />", "<br />"), htmlentities($sHtml));
        $iCt = 0;
        $aHtml = explode("<br />", $sHtml);
        $sHtml = '<table border="0" cellpadding="0" cellspacing="0">';
        foreach ($aHtml as $sLine) {
            $iCt++;
            $sHtml .= '<tr' . $sEvents . '>';
            $sHtml .= '<td style="font:normal 12px Courier New, serif; color:#999; border-style:none; border-right:1px solid #DFDFDF; padding:0px 5px 0px 2px;">' . $iCt . '</td>';
            $sHtml .= '<td style="font:normal 12px Courier New, serif; color:#000; border-style:none; padding:0px 5px;">' . $sLine . '</td>';
            $sHtml .= '</tr>';
        }
        $sHtml .= '</table>';
        if ($bBeautify) {
            $sHtml = str_replace(array("&lt;", "&gt;"), array('<span style="color:#069;">&lt;', '&gt;</span>'), $sHtml);
        }
        $this->configure("echoMode", $bEchoMode);
        return sprintf($sQuadro, $sHtml);
    }

    //---------------------------------------------------------------------------

    function getId($sHtml = "") {
        if (!empty($sHtml) && preg_match("/\sid=\"(.[^\"]*)\"/", $sHtml, $aMatches)) {
            return ((!array_key_exists($aMatches[1], $this->aHtml)) ? $aMatches[1] : $this->getId());
        }
        $iIterations = 0;
        $sId = "";
        do {
            if (!array_key_exists($sId, $this->aHtml)) {
                break;
            }
            if (10 < $iIterations) {
                die("A classe não pôde gerar um ID para a string HTML!");
            }
            $sId = substr(time(), -6) + $this->iCtID;
            $iIterations++;
            $this->iCtID++;
        } while (0);
        return $sId;
    }

    function getElementById($sId, $sInfo) {
        return $this->aHtml[$sId][$sInfo];
    }

    function hasElement($sId) {
        return isset($this->aHtml[$sId]);
    }

    //---------------------------------------------------------------------------

    function getIndent($iIndent = 0) {
        $sIndent = $this->aConfig['superIndent'];
        for ($i = 0; $i < $iIndent; $i++) {
            $sIndent .= $this->aConfig['indent'];
        }
        return $sIndent;
    }

}
