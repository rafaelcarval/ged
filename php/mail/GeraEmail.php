<?php

function _getCharStrEncoded($str) {
    return mb_detect_encoding($str . 'x', 'UTF-8, ISO-8859-1');
}

function _decodeUTF8String($s) {
    $chr = _getCharStrEncoded($s);
    if ($chr == 'UTF-8') {
        $s = utf8_decode($s);
    }
    return $s;
}

function _encodeUTF8String($s) {
    $chr = _getCharStrEncoded($s);
    if ($chr != 'UTF-8') {
        $s = utf8_encode($s);
    }
    return $s;
}

function _emailSendGrid($from, $to, $subject, $msg, $anexo = "", $bcc = false, $auth = true, $categoria = false) {
    if (!$categoria) {
        $categoria = "GED";
    }
    $subject = _encodeUTF8String($subject);
    $msg = _encodeUTF8String($msg);
    include('smtpapi-php-sendgrid/app/swift_mailer.php');
}

class GeraEmail {

    private $dados;

    public function __construct($dados) {
        $this->dados = $dados;
        //
    }

    public function gerar() {

        // include_once("class.htmlbuffer.php");
        // $dados deve ser um objeto com as posições "nome", "email", "arquivo" e "tipomsg"
        // nome = nome do destinatário
        // email = e-mail do destinatário
        // tipomsg = Tipos de mensagem: compartilhado, recusado, aprovado
        $dados = $this->dados;
        $tipoMsg = $dados->tipoMsg;

        $_15Dias = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d") + 15, date("Y")));
        $lista[] = (object) array(
                    'codarquivo' => $dados->arquivo->codarquivo,
                    'codusuarioautor' => $dados->arquivo->codusuariocadastro,
                    'dtenvio' => date("Y-m-d H:i:s"),
                    'dtexpiracao' => $_15Dias
        );
        $crud = new CrudRegistro($lista, 'ged_arquivoenviado');
        $iCodEnvio = $crud->salvar();
        if (is_array($iCodEnvio)) {
            throw new Exceptions($iCodEnvio['error']);
        }

        $sLinkArquivo = md5(date("Ymd").'#'.$iCodEnvio);
        $aParser = array(
            "nomeArquivo" => $dados->arquivo->nome,
            "linkArquivo" => 'https://sistema.safetydocs.com.br/ged/' . "dw/?f=" . $sLinkArquivo
        );
        // var_dump($aParser);
        // die('bla');
        $aMsgs = array(
            'compartilhado' => array(
                "assunto" => "Arquivo compartilhado com você",
                "msg" => "<table align='center' border='1' cellpadding='0' cellspacing='0' width='600'><tbody>
        <tr><td align='center' border='0' cellpadding='0' cellspacing='0'><em><span style='color:#808080;'>Este &eacute; um e-mail autom&aacute;tico, n&atilde;o &eacute; necess&aacute;rio respond&ecirc;-lo.</span></em></td></tr>
        <tr><td align='center'><strong style='font-size:14px;color: #c3161c'>GED</strong></td></tr>
        <tr><td><table border='0' cellpadding='0' cellspacing='0'><tbody><tr><td width='40'>&nbsp;</td><td width='525'><br />
        <p>Ol&aacute;! O arquivo #{nomeArquivo} foi compartilhado com voc&ecirc;.</p>
        <p style='text-align: justify;'><a href='#{linkArquivo}'>Clique aqui</a> para visualizar este arquivo.</p>
        <p style='text-align: justify;'>&nbsp;</p></td></tr></tbody></table></td></tr></tbody></table>"
            ),
            'recusado' => array(
                "assunto" => "Arquivo recusado",
                "msg" => "<table align='center' border='1' cellpadding='0' cellspacing='0' width='600'><tbody>
        <tr><td align='center' border='0' cellpadding='0' cellspacing='0'><em><span style='color:#808080;'>Este &eacute; um e-mail autom&aacute;tico, n&atilde;o &eacute; necess&aacute;rio respond&ecirc;-lo.</span></em></td></tr>
        <tr><td align='center'><strong style='font-size:14px;color: #c3161c'>GED</strong></td></tr>
        <tr><td><table border='0' cellpadding='0' cellspacing='0'><tbody><tr><td width='40'>&nbsp;</td><td width='525'><br />
        <p>Ol&aacute;! O arquivo #{nomeArquivo} foi recusado.</p>
        <p style='text-align: justify;'><a href='#{linkArquivo}'>Clique aqui</a> para visualizar este arquivo.</p>
        <p style='text-align: justify;'>&nbsp;</p></td></tr></tbody></table></td></tr></tbody></table>"
            ),
            'aprovado' => array(
                "assunto" => "Arquivo aprovado",
                "msg" => "<table align='center' border='1' cellpadding='0' cellspacing='0' width='600'><tbody>
        <tr><td align='center' border='0' cellpadding='0' cellspacing='0'><em><span style='color:#808080;'>Este &eacute; um e-mail autom&aacute;tico, n&atilde;o &eacute; necess&aacute;rio respond&ecirc;-lo.</span></em></td></tr>
        <tr><td align='center'><strong style='font-size:14px;color: #c3161c'>GED</strong></td></tr>
        <tr><td><table border='0' cellpadding='0' cellspacing='0'><tbody><tr><td width='40'>&nbsp;</td><td width='525'><br />
        <p>Ol&aacute;! O arquivo #{nomeArquivo} foi aprovado.</p>
        <p style='text-align: justify;'><a href='#{linkArquivo}'>Clique aqui</a> para visualizar este arquivo.</p>
        <p style='text-align: justify;'>&nbsp;</p></td></tr></tbody></table></td></tr></tbody></table>"
            )
        );

        $oH = new HtmlBuffer();
        $oH->set($aMsgs[$tipoMsg]["msg"]);
        $msg = $oH->get($aParser);

        $aRet = array("msg" => $msg, "assunto" => $aMsgs[$tipoMsg]["assunto"]);
        return (object) $aRet;
    }

    public function enviar() {
        include_once("class.htmlbuffer.php");
        // $dados deve ser um objeto com as posições "nome", "email", "assunto" e "msg"
        // nome = nome do destinatário
        // email = e-mail do destinatário
        // assunto = assunto da mensagem
        // msg = mensagem que vem do editor HTML
        $iErros = 0;
        $dados = $this->dados;
        foreach ($dados->destinatario as $dadosDest) {
            $aParser = array(
                "nome" => $dadosDest->nome
            );
            $oH = new HtmlBuffer();
            $oH->set($dados->msg);
            $msg = $oH->get($aParser);

            _emailSendGrid('noreply@safetydocs.com.br', $dadosDest->email, 'Safetydocs GED - ' . $dados->assunto, $msg, "", $dados->remetenteEmail);
            
            /*$aDest = array("nome" => $dadosDest->nome, "email" => $dadosDest->email);
            $email = new EnviarEmail();
            $email->setAssunto('GED - ' . $dados->assunto);
            $email->setDestinatario($aDest);
            $email->setMensagem($msg);
            $email->setFromName($dados->remetenteNome);
            $email->setFrom(email_padrao);
            $email->setCopiaOculta($dados->remetenteEmail);
            if (!$email->enviar()) {
                $iErros++;
            }*/
        }
        if ($iErros > 0) {
            throw new Exceptions(57);
        }
        return true;
    }

}
