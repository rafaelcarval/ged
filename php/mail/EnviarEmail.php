<?php

/**
 * @autor Paulo Roberto Neves Filho
 * @date 09/11/2015
 * @modified por Mateus Bueno em 20/02/2018
 * @version 1.1
 */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/vendor/autoload.php';
include_once('ConfigEmail.php');

class EnviarEmail extends ConfigEmail {

    private
            $assunto,
            $destinatario,
            $mensagem,
            $imagem,
            $mail,
            $anexo,
            $copia,
            $copiaOculta,
            $fromName,
            $from;

    /**
     * Classe Responsável pelo envio de Emails.
     * 
     * @return Essa classe não tem nenhum retorno.
     */
    public function __construct() {
        //$this->mail = new PHPMailer;
        $this->mail = new PHPMailer(true);
    }

    /**
     * Este metodo server para inserir o Assunto do email.
     * </br></br>
     * @param $assunto Assunto para o email
     * @return 	Não possue nenhum retorno
     */
    public function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    /**
     * Este metodo server para inserir o destinatário do email. Quem receberá o email.
     * </br></br>
     * @param $destinatario Email para destinatário do email
     * @return 	Não possue nenhum retorno
     */
    public function setDestinatario($destinatario) {
        $this->destinatario = $destinatario;
    }

    /**
     * Este metodo server para inserir o destinatário que receberá uma cópia do email.
     * </br></br>
     * ex: <b>setCopia(array())</b>
     * @param $destinatario Email ou Lista para destinatário do email
     * @return 	Não possue nenhum retorno
     */
    public function setCopia($destinatario) {
        if (is_array($destinatario) && count($destinatario) > 0) {
            for ($x = 0; $x <= count($destinatario); $x++) {
                $this->mail->addBCC($destinatario[$x]);
            }
        } else if (!empty($destinatario)) {
            $this->mail->addBCC($destinatario);
        }
    }

    /**
     * Este metodo server para inserir o destinatário que receberá uma cópia oculta do email.
     * </br></br>
     * ex: <b>setCopiaOculta(array())</b>
     * @param $destinatario Email ou Lista para destinatário em cópia oculta do email
     * @return 	Não possue nenhum retorno
     */
    public function setCopiaOculta($destinatario) {
        if (is_array($destinatario) && count($destinatario) > 0) {
            for ($x = 0; $x <= count($destinatario); $x++) {
                $this->mail->addBCC($destinatario[$x]);
            }
        } else if (!empty($destinatario)) {
            $this->mail->addBCC($destinatario);
        }
    }

    /**
     * Este metodo server para inserir uma mensagem no email.
     * </br></br>
     * @param $mensagem Mensagem texto ou código html para envio via email
     * 
     * @return  possue nenhum retorno
     */
    public function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    /**
     * Este metodo server para inserir imagem no corpo do email.
     * O caminho da imagem deve ser subistituido por cid:alias ex: &lt;img src='cid:alias'&gt;.
     * </br></br>
     * <b>setImagem(array(valores), array(valores))</b>
     * @param $caminho array com caminho da(s) imagem(ns)
     * @param $alias array com referência(s) para imagem(ns)
     * @return 	Não possue nenhum retorno
     * 
     */
    public function setImagem($caminho, $alias) {
        for ($x = 0; $x <= count($caminho); $x++) {
            $this->mail->addEmbeddedImage($caminho[$x], $alias[$x]);
        }
    }

    /**
     * Metodo que define o anexo a ser enviado.
     * Sempre deve ser utilizado no metodo $_FILES[];
     * @param $_FILES[] $arquivo
     * 
     * @return Não possue nenhum retorno
     */
    public function setAnexo($arquivo) {
        for ($x = 0; $x <= count($arquivo); $x++) {
            $this->mail->addAttachment($arquivo['tmp_name'][$x], $arquivo['name'][$x]);
        }
    }

    public function setFromName($fromName) {
        $this->fromName = $fromName;
    }

    public function setFrom($from) {
        $this->from = $from;
    }

    /**
     * Metodo resopnsável pelo envio do email com os parametros previamente configurados
     * 
     * @return Retorna 	TRUE quando ouver sucesso no envio ou FALSE quando ouver falha.
     */
    public function enviar() {
        $this->mail->isSMTP();
        $this->mail->Host = $this->host;
        $this->mail->SMTPAuth = true;
        $this->mail->Username = $this->username;
        $this->mail->Password = $this->password;
        if (!empty($this->seguranca)) {
            $this->mail->SMTPSecure = $this->seguranca;
        }
        $this->mail->Port = $this->porta;
        $this->mail->CharSet = $this->padraoCaracter;
        $this->mail->setFrom($this->from, $this->fromName);
        $this->mail->Sender = $this->username;
        $this->mail->addAddress($this->destinatario["email"], $this->destinatario["nome"]);
        $this->mail->isHTML(true);

        $this->mail->Subject = $this->assunto;
        $this->mail->Body = $this->mensagem;
        $this->mail->AltBody = $this->mensagem;
        $this->mail->send();
        if ($this->mail->ErrorInfo) {
            throw new Exceptions($this->mail->ErrorInfo);
        }
        return true;
    }

}
