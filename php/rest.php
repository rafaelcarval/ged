<?php

/**
 * Arquivo responsável por interpretar as solicitaçãos que são enviadas pelo Angular,
 * estas devem respeitar alguns critérios, estes estão explicados no decorrer do arquivo.
 * 
 * @author Paulo Neves
 * @version 2
 */
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
// arquivo auto load para todas as Classes, as classes devem ter o mesmo nome em seu arquivo, não pode existir duas classes com o mesmo nome
include_once('AutoLoad.php');
//constantes para configuração da aplicação, podendo ser colocadas novas ou editadas as existentes
Configuracoes::constantes();

/**
 * solicitações que são recebidas via get
 * objetos podem ser passados, mas devem ser serializados antes
 */
if (count($_GET) > 0) {
    $postdata = $_GET['get'];
} else {
    $postdata = file_get_contents("php://input");
}
/**
 * solicitações recebidas diretamente via post
 * as solicitações devem conter os parametros que são checados
 * cada parametro tem sua devida importancia
 * "_t" -> Quando se realiza crud via angular, deve ser utilizado para passar a tabela para a CrudRegistro, ou utilizado para passar pametros no construtor das Classes PHP
 * "_c" -> deve ser utilizado para chamar uma classe do PHP
 * "_p" -> Quando se realiza crud via angular, deve ser passada o objeto contendo o espelho do banco para atualizar editar ou inserir. Ou então, você pode utilizar para passar parametros no construtor
 * "_f" -> deve ser utilizado para chamar um metodo do PHP
 * "_w" -> deve ser utilizado para passar parametros dentro do metodo.
 * 
 */
if (isset($postdata) && !empty($postdata)) {
    $entrada = json_decode($postdata);   
    $tabela = (isset($entrada->_t)) ? $entrada->_t : NULL;
    $objeto = (isset($entrada->_p)) ? $entrada->_p : NULL;
    $classe = (isset($entrada->_c)) ? $entrada->_c : NULL;
    $metodo = (isset($entrada->_f)) ? $entrada->_f : NULL;
    $where = (isset($entrada->_w)) ? $entrada->_w : NULL;
    $t = (isset($tabela) || isset($objeto) ) ? 2 : 1;
    if (!is_null($classe) && !is_null($metodo)) {
        switch ($t) {
            case 2:
                $process = new $classe($objeto, $tabela);
                break;
            case 1:
                $process = new $classe();
                break;
        }
        try {
            $postdata = (is_null($where)) ? $process->$metodo() : $process->$metodo($where);
        } catch (Exceptions $e) {
            $obj = new stdClass();
            $obj->error = $e->getMessage();
            $postdata = $obj;
        }
        Helpers::utf8_encode_array($postdata);
        $retorno = json_encode($postdata);
        echo $retorno;
        exit;
    } else {
        echo json_encode("falha ao processar solicitacao");
        exit;
    }
} else {
    exit;
}