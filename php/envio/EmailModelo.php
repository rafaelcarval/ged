<?php

class EmailModelo {

    private $de;
    private $mensagem;
    private $cidImagem;

    public function __construct($de, $mensagem, $cidImagem) {
        $this->de = $de;
        $this->mensagem = $mensagem;
        $this->cidImagem = $cidImagem;
    }

    public function getMail() {
        $mail = '
			<html lang="pt-br">
<head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <center>
<table id="Tabela_01" width="544" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="11" bgcolor="#1AB29D" width="543" height="27">
			</td>
		
	</tr>
	<tr>
		<td colspan="4" rowspan="2" width="106" height="64" bgcolor="#1AB29D">
			</td>
		<td width="13" height="20">
			<img src="cid:' . $this->cidImagem[0] . '" width="13" height="20" alt="canto"></td>
		<td width="306" height="20">
			<img src="cid:' . $this->cidImagem[1] . '" width="306" height="20" alt="sombra"></td>
		<td width="12" height="20">
			<img src="cid:' . $this->cidImagem[2] . '" width="12" height="20" alt="canto"></td>
		<td colspan="4" rowspan="2" bgcolor="#1AB29D" width="106" height="64"></td>
	
	</tr>
	<tr>
		<td width="13" height="44">
			<img src="cid:' . $this->cidImagem[3] . '" width="13" height="44" alt="lateral"></td>
		<td rowspan="4" width="306" height="92" style="text-align: center;vertical-align: top;">
			<img src="cid:' . $this->cidImagem[4] . '" style="width: 70%; alt="logo"></td>
		<td width="12" height="44">
			<img src="cid:' . $this->cidImagem[5] . '" width="12" height="44" alt="lateral"></td>
	
	</tr>
	<tr>
		<td rowspan="10" bgcolor="#1AB29D" width="12" ">
			</td>
		<td rowspan="2" width="10" height="11">
			
		<td colspan="3" rowspan="3" width="97" height="">
			</td>
		<td colspan="3" rowspan="3"  width="96" height="">
			</td>
		<td width="10" height="9">
			
		<td rowspan="10" width="12" bgcolor="#1AB29D">
			</td>
		
	</tr>
	<tr>
		<td width="10" height="2">
			</td>
	
	</tr>
	<tr>
		<td rowspan="7" width="10" height="">
			</td>
		<td rowspan="7" width="10" height="">
			</td>
		<td width="1" height="" >
			</td>
	</tr>
	<tr>
		<td colspan="7" width="499" height="16">
		</td>
		
	</tr>
	<tr>
		<td  width="80" height="19" style="font-size:14px">De:
			</td>
		<td colspan="5" width="347" height="19" style="border: solid 1px  #1ab29d; border-top:none; border-left: none;">
           ' . $this->de . '
			</td>
		<td  width="72" height="19">
			</td>
	
	</tr>
	<tr>
		<td width="80" height="10">
			</td>
		<td colspan="5" width="347" height="10" >
			</td>
		<td width="72" height="10">
			</td>
	
	</tr>
	<tr>
		<td width="80" height="19" style="font-size:14px">
			Mensagem:</td>
		<td colspan="5" rowspan="2" width="347" height="" style="vertical-align:top; border: solid 1px  #1ab29d; border-top:none; border-left: none;">' . $this->mensagem . '
			</td>
		<td width="72" height="19">
			</td>
		<td width="1" height="19">
			</td>
	</tr>
	<tr>
		<td width="80" height="" >
		</td>
		<td width="72" height="">
			</td>
	
	</tr>
	<tr>
		<td colspan="7" width="499" height="24">
			</td>

	</tr>
	<tr>
		<td width="10" height="10" bgcolor="#FFFFFF">
			<img src="cid:' . $this->cidImagem[6] . '" width="10" height="10" alt="canto"></td>
		<td colspan="7" width="499" height="10">
			</td>
		<td  width="10" height="10" bgcolor="#FFFFFF">
			<img src="cid:' . $this->cidImagem[7] . '" width="10" height="10" alt="canto"></td>
		
	</tr>
	<tr>
		<td colspan="11" width="543" height="73" style="text-align:center" bgcolor="#1AB29D">
    <span style="font-size:12px;">Serviço de mensages</span><br>
    		 <span style="font-size:10px;">Mensagem enviada através do formulário presente em seu site.</span>
			</td>

	</tr>
	<tr>
		<td width="12" height="1">
			</td>
		<td width="10" height="1">
            </td>
		<td width="80" height="1">
			</td>
		<td width="4" height="1">
		  </td>
		<td width="13" height="1">
			</td>
		<td width="306" height="1">
			</td>
		<td width="12" height="1">
			</td>
		<td width="12" height="1">
		    </td>
		<td width="72" height="1">
			</td>
		<td width="10" height="1">
		    </td>
	</tr>
</table>
        </center>
</body>
</html>
			
			';
        return $mail;
    }

}
