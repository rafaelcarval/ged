<?php

class Configuracoes {

    public function __construct() {
        
    }

    public static function constantes() {
        define('codempresa_gestao', 103);
        define('tabela_usuario_login', 'ged_vw_usuario_login');
        define('sessao_usuario', "seesaoUsuario");
        define('raiz_arquivos', __DIR__ . "/../upload/");
        define('raiz_arquivos_pub', "upload/");
        define('email_padrao', "noreply@safetydocs.com.br");
        if ($_SERVER["SERVER_ADDR"] != "127.0.0.1") {
            define("bPublicado", true);
            define('sEnderecoNormal', "https://sistema.safetydocs.com.br/ged");
            define('sCaminhoNormal', "/opt/bitnami/apache2/htdocs/ged/");
        } else {
            define("bPublicado", false);
            define('sEnderecoNormal', "http://ged.local/");
            define('sCaminhoNormal', "/Users/mateusbueno/Sites/ged/");
        }
    }

}
