<?php
$arquivo = $_GET['c'];

$sDir =   "../".$arquivo; 


if (!isset($_GET['close'])){
	echo "<script>
		opener.$('#modalViewArquivo').modal('hide');
		opener.$('.modal-backdrop').hide();		
		location.href='viewdocs.php?c=$arquivo&close=S';
	</script>";
	exit;	
}

if (!is_file($sDir)){
    die(utf8_encode("Arquivo nao encontrado!"));    
}

$extensao = explode(".", $arquivo);
$extensao = strtolower($extensao[count($extensao) - 1]);

switch ($extensao) {
    case "pdf":
        header('Content-Type: application/pdf');
        echo readfile($sDir, "r");
    break;


    case "gif":
    case "jpg":
    case "png":
    case "jpeg":
        header("Content-Type: image");
        readfile($sDir, "r");
    break;
    default:
        die(utf8_encode("Formato nao reconhecido!"));    
        break;
}
exit;
