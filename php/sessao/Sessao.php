<?php

class Sessao {

    private $usuario;

    public function __construct($login = null) {
        if (!is_null($login)) {
            $login = new ValidarLogin($login);
            if ($login->getRetorno()) {
                $this->usuario = $login->getRetorno();
            } else {
                $this->usuario = false;
            }
        }
    }

    public function setSession() {
        if (!is_array($this->usuario)) {
            session_start();
            $objeto = Helpers::removerItemObjeto($this->usuario, 'senha');
            $objeto->empresaMaster = ($objeto->codempresa == codempresa_gestao);
            $_SESSION[sessao_usuario] = $objeto;
            return $objeto;
        } else {
            return array('msg' => 'Usuário ou senha incorreto.');
        }
    }

    public function getSession() {
        session_start();
        if (!isset($_SESSION[sessao_usuario])) {
            throw new Exceptions(56);
        }
        return $_SESSION[sessao_usuario];
    }

    public function addItemSession($item, $valor) {
        session_start();
        if (isset($_SESSION[sessao_usuario])) {
            $objeto = $_SESSION[sessao_usuario];
            if (isset($objeto->$item) && is_array($objeto->$item)) {
                foreach ($valor as $v) {
                    $objeto->$item[] = $v;
                }
            } else if (isset($objeto->$item) && !is_array($objeto->$item)) {
                $objeto->$item = $valor;
            } else {
                $objeto->$item = $valor;
            }
            $_SESSION[sessao_usuario] = $objeto;
        } else {
            return array("msg" => "erro");
        }
    }

    public function delItemSession($objetoRemover) {
        session_start();
        $item = $objetoRemover->nome;
        if (isset($_SESSION[sessao_usuario]->$item)) {
            $objeto = $_SESSION[sessao_usuario];
            if ((is_array($objeto->$item)) && (count($objeto->$item) > 1) && !isset($objetoRemover->valor)) {
                foreach ($_SESSION[sessao_usuario]->$item as $k => $valor) {
                    if ($objetoRemover->valor == $valor) {
                        unset($_SESSION[sessao_usuario]->$item[$k]);
                    }
                }
            } else {
                unset($_SESSION[sessao_usuario]->$item);
            }
            $_SESSION[sessao_usuario] = $objeto;
            return array("msg" => "item removido da sessão");
        } else {
            return array("msg" => "item não encontrado na sessão");
        }
    }

    public function unsetSession() {
        session_start();
        if (isset($_SESSION)) {
            unset($_SESSION);
            session_unset();
            return array("logout" => true);
        } else {
            return array("erro");
        }
    }

}
