<?php

spl_autoload_register('carregar');
date_default_timezone_set('America/Sao_Paulo');

function carregar($name) {
    $name .= ".php";
    localizarClasse(__DIR__, $name);
}

function localizarClasse($pasta, $classe, $n = 0) {
    $limiteSubniveis = 2;
    if (is_dir($pasta) && $n <= $limiteSubniveis) {
        $dh = opendir($pasta);
        while (false !== ($arquivoPasta = readdir($dh))) {
            if (is_file($pasta . "/" . $arquivoPasta) && strstr($arquivoPasta, $classe)) {
                return include_once($pasta . "/" . $arquivoPasta);
            } else if (!strstr($arquivoPasta, '.')) {
                localizarClasse($pasta . "/" . $arquivoPasta, $classe, $n + 1);
            }
        }
    }
}
