<?php

class Upload {

    private $objArquivo;
    private $arrayArquivo;
    private $infoUpload;

    public function __construct($infoUpload = NULL) {
        Helpers::utf8_encode_array($_FILES['file']);
        $this->arrayArquivo = $_FILES['file'];
        $this->objArquivo = (object) $this->arrayArquivo;
        // $this->infoUpload = (is_string($infoUpload)) ? $this->objetoDeString($infoUpload) : $infoUpload;
        $this->infoUpload = $infoUpload;
    }

    private function objetoDeString($string) {
        $infoUpload = substr($string, 1, -1);
        $p = explode(",", $infoUpload);
        $infoUpload = array();
        foreach ($p as $registro) {
            $j = explode(':', $registro);
            $infoUpload[$j[0]] = str_replace(array("'"), "", $j[1]);
        }
        return (object) $infoUpload;
    }

    public function carregarImagem() {
        $arquivo = new Arquivo();
        $logo = (object) array('codempresa' => $this->infoUpload->codempresa, 'logo' => $this->infoUpload->pasta . "/" . $this->objArquivo->name);
        $carregar = $arquivo->uploadFile($this->arrayArquivo, null, $this->infoUpload->pasta);
        if ($carregar) {
            $crud = new CrudRegistro($logo, 'ged_empresa');
            $atualizar = $crud->atualizar();
            if (!$atualizar['msg']) {
                throw new Exceptions(61);
            }
            $answer = array('msg' => 'Arquivos carregados', "nome" => $this->objArquivo->name);
        } else {
            $answer = array('error' => 'Falha ao carregar os arquivos');
        }
        return $answer;
    }

    public function carregar($dados) {
        $outrosDados = $dados;
        $caminhoUpload = "arquivos/" . $outrosDados->codraiz . "/";
        $this->infoUpload->caminho = raiz_arquivos_pub . $caminhoUpload;
        Helpers::removerEspacos($caminhoUpload);
        Helpers::removerEspacos($this->infoUpload->caminho);
        $this->infoUpload->nome = $this->objArquivo->name;
        $this->infoUpload->tamanho = $this->objArquivo->size;
        if (!$outrosDados->listaaprovadores[0]) {
            $this->infoUpload->codstatusarquivo = 3;
        }
        $codarquivo = $this->criarRegistro($this->infoUpload);
        if (!is_array($codarquivo)) {
            $arquivo = new Arquivo();
            $carregar = $arquivo->uploadFile($this->arrayArquivo, null, $caminhoUpload . "/" . $codarquivo);
            if ($carregar) {
                $this->vincularArvoreArquivo($codarquivo, $outrosDados->codpai);
                $this->vincularAprovadores($outrosDados->listaaprovadores, $codarquivo);
                $sessao = new Sessao();
                $sessao->addItemSession('codarquivoupload', array($codarquivo));
                $answer = array('msg' => 'Arquivos carregados', "nome" => $this->objArquivo->name, "codarquivo" => $codarquivo);
            } else {
                $answer = array('error' => 'Falha ao carregar os arquivos');
            }
        } else {
            $answer = $codarquivo;
        }

        return $answer;
    }

    public function listarArquivos() {
        $sessao = new Sessao();
        $objeto = $sessao->getSession();
        if (is_object($objeto) && isset($objeto->codarquivoupload)) {
            $crud = new CrudRegistro(NULL, 'ged_vw_arquivo_atualizar');
            $arquivos = (object) array('codarquivo' => $objeto->codarquivoupload);
            $saida = $crud->listar($arquivos);
            if (is_object($saida)) {
                return array($saida);
            } else {
                return $saida;
            }
        }
        return false;
    }

    private function vincularArvoreArquivo($codarquivo, $codarvore) {
        $objeto = (object) array('codarvore' => $codarvore, 'codarquivo' => $codarquivo);
        $crud = new CrudRegistro($objeto, 'ged_arvorearquivo');
        return $crud->salvar();
    }

    private function vincularAprovadores($listaAprovadores, $codArquivo) {
        if (!$listaAprovadores[0]) {
            return false;
        }
		$oSql = new ComandosSql();
		$aEnviar = array();
        if (is_array($listaAprovadores)) {
            $lista = array();
            foreach ($listaAprovadores as $codAprovador) {
                $lista[] = (object) array('codarquivo' => $codArquivo, 'codusuario' => $codAprovador, 'codstatusarquivo' => 1);
				
				$sql = "select * from ged_usuario where codusuario = '$codAprovador'";
				$stmt = $oSql->mysqliQuery($sql);
				$rsU = $stmt->fetch_object();
				
				$aEnviar[] = $rsU->email;
				
            }
        }
        $crud = new CrudRegistro($lista, 'ged_aprovacaoarquivo');
        $vincular = $crud->salvar();
		
		$sql = "select * from vw_arquivos_pastas where codarquivo = '$codArquivo'";
		
        $stmt = $oSql->mysqliQuery($sql);
        $rs = $stmt->fetch_object();
		
		$msg ="Prezado Usuário, ";
		$msg.="<br><br>";
		$msg.="O arquivo <b>$rs->arquivo</b> requer sua aprovação no sistema GED na pasta <b>$rs->pasta</b> da empresa <b>$rs->empresa</b>. <br><br>";
		$msg.="<br>Arquivo Postado Por: <b>$rs->autor</b><br>";
		if (count($aEnviar) > 0){
			Helpers::emailSendGrid("noreplay@safetydocs.com.br", $aEnviar, "[GED] Arquivo para aprovacao", $msg );		
		}
 
        if (isset($vincular['error'])) {
            throw new Exceptions($vincular['error']);
        }
        return $vincular;
    }

    private function criarRegistro($dadosArquivo) {
        $crud = new CrudRegistro($dadosArquivo, 'ged_arquivo');
        return $crud->salvar();
    }

    private function setCodUploadSession($cod) {
        session_start();
        if (isset($_SESSION['upload'])) {
            $_SESSION[sessao_usuario][] = $cod;
        } else {
            $_SESSION[sessao_usuario] = array();
            $_SESSION['upload'][] = $cod;
        }
    }

}
