<?php

class ValidarLogin {

    private $retorno = '';

    public function __construct($login) {
        $crud = new CrudRegistro(null, tabela_usuario_login);
        $consulta = $crud->listar($login);
        if ($consulta) {
            $this->retorno = $consulta;
        } else {
            $this->retorno = FALSE;
        }
    }

    public function getRetorno() {
        return $this->retorno;
    }

}
