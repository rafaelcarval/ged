<?php

class Exceptions extends Exception {

    public function __construct($erro) {
        $default = false;
        switch ($erro) {
            case 50:
                $mensagem = "Falha ao acessar objeto. Apenas objetos devem ser passados.";
                break;
            case 51:
                $mensagem = "Apenas array deve ser passado.";
                break;
            case 52:
                $mensagem = "Nenhum Registro Encontrado.";
                break;
            case 53:
                $mensagem = "Apenas parametros Int.";
                break;
            case 54:
                $mensagem = "Apenas ARQUIVOS devem ser passados.";
                break;
            case 55:
                $mensagem = "Parametros incorretos, esperado outro tipo de dados.";
                break;
            case 56:
                $mensagem = "Falha ao retornar a Sessão.";
                break;
            case 57:
                $mensagem = "Falha ao enviar o e-mail.";
                break;
            case 58:
                $mensagem = "Você não tem autorização para apagar esse arquivo.";
                break;
            case 59:
                $mensagem = "Falha ao localizar arquivo.";
                break;
            case 60:
                $mensagem = "Falha ao carregar arquivos.";
                break;
            case 61:
                $mensagem = "Falha ao atualizar arquivos.";
                break;
            case 62:
                $mensagem = "Apenas String deve ser passado.";
                break;
            default:
                $default = true;
                $mensagem = "Erro ao executar a Tarefa! Erro: " . $erro;
        }
        $this->message = (!$default) ? "Arquivo: " . $this->getFile() . "\nLinha: " . $this->getLine() . "\nMensagem: " . $mensagem : $mensagem;
    }

}
