 <?php

/**
 * @author Paulo Roberto Neves Filho
 * 
 * @date 13/08/2014
 * @version 31/05/2017
 * @version 20/10/2017
 * @version by Mateus Bueno 16/04/2018
 * @class responsavel pelos comandos CRUD sql
 *
 */
class ComandosSql {

    private $mysqli;

    public function __construct() {
        $this->mysqli = ConectarMySql::getInstance();
    }

    /**
     * Metodo responsável pela construção da expressão de Select no banco de dados
     * @param type $tabela string com nome da tabela para consulta
     * @param type $coluna array contendo as colunas para where ou string
     * @param type $valor array contendo os valores para where
     * @param type $dif negar consulta
     * @param type $arrayColunas array contendo as colunas que deseja selecionar no banco
     * @param type $ordenador string para ordenar
     * @param type $customWhere string contendo expressão para where
     * @param type $die retorno string sql para debug
     * @return type object or Array of objects
     */
    public function select($tabela, $coluna = null, $valor = null, $dif = false, $arrayColunas = null, $ordenador = null, $customWhere = null, $die = null) {
        $sql = "SELECT ";
        $sql .= (!is_null($arrayColunas)) ? $this->concatenarSimples($arrayColunas) . " " : '* ';
        $sql .= "FROM $tabela ";
        $sql .= (is_null($customWhere)) ? $this->resolverWhere($coluna, $valor, $dif) : " " . $customWhere . " ";
        $sql .= (!is_null($ordenador)) ? " ORDER BY $ordenador" : "";
        if ($die) {
            die($sql);
        }
        $saida = $this->resolverSaida($sql);
        return $saida;
    }

    public function insert($tabela, $coluna, $valor, $die = null) {
        $sql = "INSERT INTO $tabela";
        $sql .= " (";
        if (is_array($coluna) && is_array($valor)) {
            $sql .= $this->concatenarSimples($coluna);
            $sql .= ",dtcadastro)VALUES(";
            $sql .= $this->concatenarComAspas($valor);
            $sql .= ",now())";
        } else if (is_string($coluna) && is_string($valor)) {
            $sql .= $coluna;
            $sql .= ",dtcadastro)VALUES(";
            $sql .= "'" . addslashes($valor) . "',now()";
            $sql .= ")";
        }
        if ($die) {
            die($sql);
        }
        $this->mysqliQuery($sql);
        $saida = $this->mysqli->insert_id;
        return (!$this->mysqli->error) ? $saida : array('error' => $this->mysqli->error);
    }

    public function update($tabela, $coluna, $valor, $dif = false, $colunaWhere, $valorWhere, $die = null) {        
        $sql = "UPDATE $tabela SET ";
        if (is_array($coluna) && is_array($valor)) {
            $sql .= " " . $this->concatenarComposto($coluna, $valor) . ", dtalteracao = now() ";
        } else {
            $sql .= " $coluna='$valor', dtalteracao = now() ";
        }
        $sql .= $this->resolverWhere($colunaWhere, $valorWhere, $dif);
        if ($die) {
            die($sql);
        }                
		//die($sql);
        $saida = $this->mysqliQuery($sql);
        return (!$this->mysqli->error) ? $saida : array('error' => $this->mysqli->error);
    }

    public function delete($tabela, $coluna, $valor) {
        $sql = "DELETE FROM $tabela WHERE ";
        if (is_array($coluna) && is_array($valor)) {
            $sql .= " " . $this->concatenarComAnd($coluna, $valor) . " ";
        } else {
            $sql .= " $coluna='$valor'";
        }
        $saida = $this->mysqliQuery($sql);
        return ($saida) ? $saida : array('error' => $this->mysqli->error);
    }

    public function nextId($tabela) {
        $sql = "SHOW TABLE STATUS LIKE '" . $tabela . "'";
        $resultado = $this->mysqliQuery($sql);
        $linha = $resultado->fetch_assoc();
        $next = $linha['Auto_increment'];
        return $next;
    }

    //metodo para concatenar ex. "coluna ='valor',"
    private function concatenarComposto($array, $array2) {
        if (count($array) == 0 && count($array2) == 0) {
            return;
        }
        for ($x = 0; $x <= count($array); $x++) {
            if ($x == 0) {
                $y = $array[$x] . "='" . addslashes($array2[$x]) . "', ";
            } elseif ($x >= count($array)) {
                $y = substr($y, 0, -2);
            } else {
                $y = $y . $array[$x] . "='" . addslashes($array2[$x]) . "', ";
            }
        }
        return $y;
    }

    //metodo para concatenar ex. "coluna ='valor' AND"
    private function concatenarComAnd($array, $array2) {
        if (!is_array($array) || count($array) <= 0 || !is_array($array2) || count($array2) <= 0) {

            die('Falha ao processar solicitação: linha 89 concatenarComAnd(); Parametros incorretos, esperado Array com valores, recebido ' . gettype($array));
        }
        for ($x = 0; $x <= count($array); $x++) {
            if ($x == 0) {
                $y = $array[$x] . "='" . addslashes($array2[$x]) . "' AND ";
            } elseif ($x >= count($array)) {
                $y = substr($y, 0, -5);
            } else {
                $y = $y . $array[$x] . "='" . addslashes($array2[$x]) . "' AND ";
            }
        }
        return $y;
    }

    //metodo para concatenar usando "," para separar os elementos de uma lista
    private function concatenarSimples($array) {

        for ($x = 0; $x <= count($array); $x++) {
            if ($x == 0) {
                $y = $array[$x] . ", ";
            } elseif ($x >= count($array)) {
                $y = substr($y, 0, -2);
            } else {
                $y = $y . $array[$x] . ", ";
            }
        }
        return $y;
    }

    //metodo para concatenar usando "," e "'" ex. " 'valor', 'valor2' "
    private function concatenarComAspas($array) {
        for ($x = 0; $x <= count($array); $x++) {
            if ($x == 0) {
                $y = "'" . addslashes($array[$x]) . "' , ";
            } elseif ($x >= count($array)) {
                $y = substr($y, 0, -2);
            } else {
                $y = $y . "'" . addslashes($array[$x]) . "' , ";
            }
        }
        return $y;
    }

    private function resolverWhere($coluna, $valor, $dif) {
        $sql = '';
        if (!is_null($coluna) && !is_null($valor)) {
            if (is_string($coluna) && (is_string($valor) OR is_int($valor))) {
                $sql = (!$dif) ? "WHERE $coluna='$valor'" : "WHERE $coluna<>'$valor'";
            } else if (is_string($coluna) && is_array($valor)) {
                $sql = (!$dif) ? "WHERE $coluna in (" . $this->concatenarComAspas($valor) . ") " : "WHERE $coluna not in (" . $this->concatenarComAspas($valor) . ") ";
            } else if (is_array($coluna) && is_array($valor)) {
                $sql = "WHERE " . $this->concatenarComAnd($coluna, $valor) . " ";
            }
        }
        return $sql;
    }

    private function resolverSaida($sql) {
        $resultado = array();
        if ($saida = $this->mysqliQuery($sql)) {
            if ($saida->num_rows > 1) {
                while ($obj = $saida->fetch_object()) {
                    $resultado[] = $obj;
                }
            } else {
                $resultado = $saida->fetch_object();
            }
        } else {
            $resultado = array('msg' => 'Nenhum registro encontrado.');
        }
        return $resultado;
    }

    public function mysqliQuery($sql) {        
        $query = $this->mysqli->query($sql);
        if (!$query) {
            return false;
        }
        return $query;
    }

}
