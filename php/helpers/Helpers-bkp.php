<?php

define("DATE_MYSQL", "mysql");
define("DATE_DATE", "data");
define("DATE_MYSQLTIME", "mysqlhora");
define("DATE_DATETIME", "datahora");
define("DATE_TIMESTAMP", "timestamp");
define("DATE_GMTTIMESTAMP", "gmt_timestamp");

class Helpers {

    public function __construct() {
        //
    }

    public static function utf8_encode_array(&$input) {
        if (is_string($input) && mb_detect_encoding($input . 'x', 'UTF-8, ISO-8859-1') != 'UTF-8') {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                self::utf8_encode_array($value);
            }
            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                self::utf8_encode_array($input->$var);
            }
        }
    }

    public static function utf8_decode_array(&$input) {
        if (is_string($input) && mb_detect_encoding($input . 'x', 'UTF-8, ISO-8859-1') == 'UTF-8') {
            $input = utf8_decode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                self::utf8_decode_array($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                self::utf8_decode_array($input->$var);
            }
        }
    }

    public static function removerItemObjeto($objeto, $ignoreColuna = NULL, $justColuna = NULL) {
        $valores = array();
        if (is_object($objeto)) {
            return self::removerItem($objeto, $ignoreColuna, $justColuna);
        } else {
            foreach ($objeto as $ob) {
                $valores[] = self::removerItem($ob, $ignoreColuna, $justColuna);
            }
            return $valores;
        }
    }

    private static function removerItem($objeto, $ignoreColuna = NULL, $justColuna = NULL) {
        $valores = array();
        foreach ($objeto as $key => $dado) {
            if (is_null($justColuna) && $key != $ignoreColuna) {
                $valores[$key] = utf8_decode($dado);
            } else if (!is_null($justColuna) && strstr($key, $justColuna)) {
                $valores[$key] = utf8_decode($dado);
                break;
            }
        }
        return (object) ($valores);
    }

    public static function removerEspacos(&$string) {
        if (!is_string($string)) {
            throw new Exceptions('Não é string, falha ao tentar remover espaços');
        }
        $string = str_replace(array(" "), "", $string);
    }

    public static function combinarArrays($array) {
        if (!is_array($array)) {
            throw new Exceptions('Apenas arrays');
        }
        $arrayS = array();
        for ($x = 0; $x < count($array); $x++) {
            if ($x == 0) {
                $arrayS = array_merge_recursive($array[$x], $array[++$x]);
            } else {
                $arrayS = array_merge_recursive($arrayS, $array[$x]);
            }
            echo $x;
        }
        return $arrayS;
    }
    
    public static function _date_format($sData, $sTipo = "mysql") {
        if (empty($sData) || "0000-00-00" == $sData || "0000-00-00 00:00:00" == $sData) {
            return "";
        }
        switch ($sTipo) {
            case DATE_MYSQL:
                if (preg_match("/[0-9]{8}/", $sData)){
                    $sData = substr($sData,0,2)."/".substr($sData,2,2)."/".substr($sData,4,4);
                }
                if (preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $sData)) {
                    return $sData;
                }
                $aData = explode("/", $sData);
                return "{$aData[2]}-{$aData[1]}-{$aData[0]}";
                
            case DATE_DATE:
                if (preg_match("/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/", $sData)) {
                    return $sData;
                }
                $aData = explode(" ", $sData);
                $aData = explode("-", $aData[0]);
                return "{$aData[2]}/{$aData[1]}/{$aData[0]}";

            case DATE_MYSQLTIME:
                $aData = explode(" ", $sData);
                $sHora = $aData[1];
                $aData = explode("/", $aData[0]);
                return "{$aData[2]}-{$aData[1]}-{$aData[0]} {$sHora}";

            case DATE_DATETIME:
                $aData = explode(" ", $sData);
                $sHora = $aData[1];
                $aData = explode("-", $aData[0]);
                $sHora = substr($sHora, 0, 5);
                return "{$aData[2]}/{$aData[1]}/{$aData[0]} {$sHora}";

            case DATE_TIMESTAMP:
            case DATE_GMTTIMESTAMP:
                $aData = explode(" ", $sData);
                $aVerData = explode("/", $aData[0]);
                if(count($aVerData) == 3) {
                    $aData[0] = _date_format($aData[0], DATE_MYSQL);
                }
                $aHora = (!empty($aData[1]) ) ? explode(":", $aData[1]) : array(0, 0, 0);
                $aData = explode("-", $aData[0]);
                for ($i = 0; $i < count($aData); $i++) {
                    $aData[$i] = (integer) $aData[$i];
                }
                for ($i = 0; $i < count($aHora); $i++) {
                    $aHora[$i] = (integer) $aHora;
                }
                if (DATE_GMTTIMESTAMP == $sTipo) {
                    return gmmktime($aHora[0], $aHora[1], $aHora[2], $aData[1], $aData[2], $aData[0]);
                }
                return mktime($aHora[0], $aHora[1], $aHora[2], $aData[1], $aData[2], $aData[0]);
        }

        return null;
    }

}
