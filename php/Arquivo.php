<?php

class Arquivo {

    private $raiz;

    public function __construct() {
        if (!isset($this->raiz)) {
            $this->raiz = raiz_arquivos;
        }
    }

    public function setRaiz($caminho) {
        $this->raiz = $caminho;
    }

    public function validarCaminho($caminho) {
        $pastaN = "";
        if (is_dir($this->raiz . $caminho)) {
            return TRUE;
        } else {
            $pasta = explode("/", $caminho);
            for ($x = 0; $x < count($pasta); $x++) {
                $pastaN = $pastaN . $pasta[$x] . "/";
                if (!is_dir($this->raiz . $pastaN)) {
                    $this->criarPasta($pastaN);
                }
            }
            if (is_dir($this->raiz . $pastaN)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function validarArquivo($arquivo) {
        $ok = is_file($arquivo);
        if (!$ok) {
            throw new Exceptions(59);
        }
        return $ok;
    }

    public function uploadFile($arquivo, $key = null, $pasta) {
        if (!is_null($key)) {
            $tmpName = $arquivo['tmp_name'][$key];
            $name = $arquivo['name'][$key];
        } else {
            $this->validarCaminho($pasta);
            $tmpName = $arquivo['tmp_name'];
            $name = $arquivo['name'];
        }
        $copiarArquivo = move_uploaded_file($tmpName, $this->raiz . $pasta . "/" . $name);
        if (!$copiarArquivo) {
            throw new Exceptions(54);
        }
        return true;
    }

    public function uploadMuliplesFiles($arquivos, $pasta) {
        $listaFalha = array();
        if ($this->validarCaminho($pasta)) {
            $qdt = count($arquivos['name']);
            for ($x = 0; $x < $qdt; $x++) {
                if ($this->uploadFile($arquivos, $x, $pasta)) {
                    return TRUE;
                } else {
                    $listaFalha[] = $arquivos['name'];
                }
            }
        } else {
            echo "Falha ao criar/validar o caminho informado!";
            exit;
        }
    }

    public function criarPasta($pasta) {
        //return mkdir($this->raiz.$pasta) or die("erro");
        return mkdir($this->raiz . $pasta, 0777);
    }

    public function removerPasta($pasta) {
        return rmdir($this->raiz . $pasta);
    }

    public function listarArquivos($pasta) {
        return scandir($this->raiz . $pasta);
    }

    public function removerArquivo($arquivo) {
        $arquivo = $this->raiz . '/' . $arquivo;
        if (is_file($arquivo)) {
            return unlink($arquivo);
        }
        return array('msg' => "Falha ao localizar o arquivo para ser removido");
    }

    public function getRaiz() {
        return $this->raiz;
    }

}
