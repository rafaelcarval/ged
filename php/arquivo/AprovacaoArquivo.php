<?php

class AprovacaoArquivo {

    private $arquivo;
    private $dadosAprovacao;

    public function __construct($arquivo) {
        if (!is_object($arquivo) && !is_array($arquivo)) {
            throw new Exceptions(55);
        }
        $this->arquivo = $arquivo;

        try {
            $this->setDadosAprovacao();
        } catch (Exceptions $e) {
            die($e->getMessage());
        }
    }

    private function setStatusArquivo($cod) {
        $dados = $this->dadosAprovacao;
        $primeiroAprovacao = $this->updateAprovacaoArquivo($cod);
        if ($dados->quantidade > 0 && $dados->qdtAprovado > 0 && $dados->qdtReprovado <= 0 || $dados->quantidade == $dados->qdtAprovado) {
            $segundaAprovacao = $this->updateArquivo($cod);
        } else {
            $segundaAprovacao = $this->updateArquivo(2);
        }

        return ($primeiroAprovacao && $segundaAprovacao) ? TRUE : FALSE;
    }

    public function aprovar() {
        return $this->setStatusArquivo(3);
    }

    public function reprovar() {
        return $this->setStatusArquivo(2);
    }

    private function getStatus() {
        
    }

    private function updateArquivo($codStatusArquivo) {
        $obj = (object) array('codstatusarquivo' => $codStatusArquivo);
        $where = (object) array('codarquivo' => $this->arquivo->codarquivo);
        $crud = new CrudRegistro($obj, 'ged_arquivo');
        return $crud->atualizar($where);
    }

    private function updateAprovacaoArquivo($codStatusArquivo) {
        $obj = (object) array('codstatusarquivo' => $codStatusArquivo);
        $sessao = new Sessao();
        $user = $sessao->getSession();
        $where = (object) array('codarquivo' => $this->arquivo->codarquivo, 'codusuario' => $user->codusuario);
        $crud = new CrudRegistro($obj, 'ged_aprovacaoarquivo');
        return $crud->atualizar($where);
    }

    private function setDadosAprovacao() {
        $arrayQdt = array();
        $crud = new CrudRegistro(null, 'ged_aprovacaoarquivo');
        $lista = $crud->listar((object) array('codarquivo' => $this->arquivo->codarquivo), array('codstatusarquivo'));
        $this->agruparEmArrayUnicaPosicao($lista, $arrayQdt);

        $quantidadeAprovado = (array_key_exists('3', $arrayQdt)) ? $arrayQdt['3'] : 0;
        $quantidadeReprovado = (array_key_exists('2', $arrayQdt)) ? $arrayQdt['2'] : 0;
        $quantidadeAndamento = (array_key_exists('1', $arrayQdt)) ? $arrayQdt['1'] : 0;
        $this->dadosAprovacao = (object) array('listaAprovacao' => $lista,
                    'quantidade' => count($lista),
                    'qdtAprovado' => $quantidadeAprovado,
                    'qdtReprovado' => $quantidadeReprovado, 'qdtAndamento' => $quantidadeAndamento);
    }

    private function agruparEmArrayUnicaPosicao(&$arrayObjetos, &$arrayQdt) {
        if (!is_array($arrayObjetos)) {
            throw new Exceptions(51);
        }
        $string = '';
        $array = array();
        foreach ($arrayObjetos as $objeto) {
            $objetoI = (array) $objeto;
            foreach ($objetoI as $key => $pos) {
                $array[] = $pos;
                $string .= $pos . " ";
            }
        }
        foreach (count_chars($string, 1) as $key => $qdt) {
            if ($key != 32) {
                $arrayQdt[chr($key)] = $qdt;
            }
        }
        $arrayObjetos = $array;
    }

}
