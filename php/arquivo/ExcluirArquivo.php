<?php

class ExcluirArquivo {

    private $usuario;

    public function __construct() {
        $sessao = new Sessao();
        $this->usuario = $sessao->getSession();
    }

    public function excluir($arquivo) {
        if ($this->usuario->codusuario != $arquivo->codusuariocadastro) {
            throw new Exceptions(58);
        }
        $arquivo->status = 0;
        $crud = new CrudRegistro($arquivo, 'ged_arquivo');
        $crud->atualizar();
    }

}
