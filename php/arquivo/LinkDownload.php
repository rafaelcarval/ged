<?php

class LinkDownload {

    //put your code here
    public function gerarLink($codigo) {
        $iCodEnvio = base64_decode($codigo);
        $crud = new CrudRegistro(null, 'ged_arquivoenviado');
        $filtro = (object) array('codenvio' => (int) $iCodEnvio);
        $oRet = $crud->listar($filtro);
        
        $crud2 = new CrudRegistro(null, 'ged_arquivo');
        $filtro = (object) array('codarquivo' => (int) $oRet->codarquivo);
        $oRet2 = $crud2->listar($filtro);
        
        if (date("Y-m-d H:i:s") > $oRet->dtexpiracao) {
            $sRet = Helpers::_date_format($oRet->dtexpiracao, DATE_DATETIME);
            return (object)["arquivo" => $oRet2->nome, "codarquivo" => $oRet2->codarquivo, "msg" => $sRet, "tipo" => "erro"];
        }
        $sRet = sEnderecoNormal . "arquivos/downloadlink/$oRet->codarquivo";
        return (object)["arquivo" => $oRet2->nome, "codarquivo" => $oRet2->codarquivo, "msg" => $sRet, "tipo" => "link"];
    }

}
