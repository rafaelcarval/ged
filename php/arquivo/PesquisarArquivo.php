<?php

/**
 * Description of PesquisarArquivo
 *
 * @author Paulo
 */
class PesquisarArquivo {

    private $usuario,
            $crud,
            $pesquisa,
            /*$group = "codarquivo, codarvore, codstatusarquivo, codusuariocadastro, nome, caminho, descricao, palavrachave, pesquisa, dtcadastro, codempresa, fantasia";*/
            // o array acima esta gerando o seguinte erro Error Code: 1038. Out of sort memory, consider increasing server sort buffer size
            $group = "codarquivo, codarvore, codstatusarquivo, codusuariocadastro, nome, caminho, pesquisa, dtcadastro, codempresa, fantasia";

    public function __construct($pesquisa) {
        if (!is_string($pesquisa)) {
            throw new Exceptions(62);
        }
        $this->pesquisa = $pesquisa;
        $usuario = new Sessao();
        $this->usuario = $usuario->getSession();
        $this->crud = new CrudRegistro(null, 'ged_vw_arvore');
    }

    public function pesquisar() {
        $crud = new CrudRegistro(null, 'ged_vw_pesquisa');
        $pesquisa = strtolower($this->pesquisa);
        $custom = $this->validarPesquisa($pesquisa);
        $arrayColunas = array('codarquivo', 'codarvore', 'codstatusarquivo', 'codusuariocadastro', 'nome', 'caminho', 'descricao', 'palavrachave', 'pesquisa', 'dtcadastro', 'codempresa', 'fantasia');
        $pesquisaR = $crud->listar(null, null, $custom, $arrayColunas);
        $retorno = array();
        if (is_object($pesquisaR)) {
            $retorno[] = $pesquisaR;
        } else {
            $retorno = array_unique($pesquisaR, SORT_REGULAR);
        }
        $retornoCorrigido = $this->unique_multidim_array($retorno, 'codarquivo');
        $this->raiz($retornoCorrigido);
        return $retornoCorrigido;
    }

    private function validarPesquisa($pesquisa) {
        $usuario = $this->usuario;
        $like = $this->validarLike($pesquisa);
        if ($usuario->empresaMaster && $usuario->codtipousuario >= 3) {
            $sql = "WHERE " . substr($like, 5) . " GROUP BY $this->group";
        } else if ($usuario->codtipousuario >= 3) {
            $sql = "WHERE codempresa=" . $usuario->codempresa . $like . " GROUP BY $this->group";
        } else {
            $sql = "WHERE codempresa=$usuario->codempresa " . $like . " GROUP BY $this->group";
        }
		//die($sql);
        return $sql;
    }

    private function validarLike($pesquisa): string {
        $array = explode(" ", $pesquisa);
        $stringSaida = '';
        if (count($array) > 1) {
            foreach ($array as $palavra) {
                if ($palavra) {
                    $stringSaida .= " AND pesquisa like '%" . $palavra . "%' ";
                }
            }
        } else {
            $stringSaida .= " AND pesquisa like '%" . $pesquisa . "%' ";
        }
        return $stringSaida;
    }

    private function raiz(&$listaArquivo) {
        foreach ($listaArquivo as $arquivo) {
            if (is_object($arquivo)) {
                $caminhoV = $this->montarRaiz($arquivo->codarvore);
                $arquivo->caminhoVirtual = $caminhoV->caminho;
                $arquivo->caminhoTruncate = $caminhoV->caminhoTruncate;
                $arquivo->link = $caminhoV->link;
            }
        }
    }

    private function montarRaiz($codPai, $links = '') {
        if (!is_object($links)) {
            $links = new stdClass();
            $links->caminho = '';
            $links->caminhoTruncate = '';
            $links->link = '';
        }
        if ($codPai > 0) {
            $obj = new stdClass();
            $obj->codarvore = $codPai;
            $r = $this->crud->listar($obj, null, null, null, "titulo");
            if (!is_object($r)) {
                die(print_r($r) . print_r($obj));
                throw new Exceptions(52);
            }
            $links->caminhoTruncate = $this->crud->_truncate($links->caminho, 30);
            $links->caminho = Slugify_class::slugify($r->titulo) . "/" . $links->caminho;
            $links->link = $r->codarvore . "/" . Slugify_class::slugify($r->titulo) . "/" . $links->link;
            if ($r->codpai) {
                unset($obj);
                return $this->montarRaiz($r->codpai, $links);
            } else {
                $links->link = 'arquivos/' . substr($links->link, 0, -1);
                return $links;
            }
        }
    }

    private function unique_multidim_array(&$array, $key) {
        $temp_array = array();
        $key_array = array();
        foreach ($array as $val) {
            if (is_object($val) && !in_array($val->$key, $key_array)) {
                $key_array[] = $val->$key;
                $temp_array[] = $val;
            }
        }
        return $temp_array;
    }

}
