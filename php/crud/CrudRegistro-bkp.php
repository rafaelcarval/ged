<?php

class CrudRegistro {

    private
            $registro,
            $tabela,
            $colunaCodigo = 'cod',
            $sql;

    public function __construct($registro, $tabela) {
        $this->tabela = $tabela;
        $this->registro = $registro;
        $this->sql = new ComandosSql();
    }

    public function salvar() {
        $saida = "";
        if (is_array($this->registro)) {
            foreach ($this->registro as $registro) {
                $subSaida = $this->criarEditarRegistro($registro);
                if (!is_array($subSaida)) {
                    $saida .= $subSaida;
                } else {
                    throw new Exceptions($subSaida['error']);
                }
            }
        } else {
            $saida = $this->criarEditarRegistro($this->registro);
        }
        return $saida;
    }

    public function atualizar($whereRegistro = null) {
        if (is_array($this->registro)) {
            foreach ($this->registro as $registro) {
                $saida = $this->criarEditarRegistro($registro, true);
            }
        } else {
            $saida = $this->criarEditarRegistro($this->registro, true, $whereRegistro);
        }
        if ($saida !== true) {
            throw new Exceptions($saida['error']);
        }
        return $saida;
    }

    public function deletar() {
        if (is_array($this->registro)) {
            foreach ($this->registro as $registro) {
                $saida = $this->removerRegistro($registro);
            }
        } else {
            $saida = $this->removerRegistro($this->registro);
        }
        return $saida;
    }

    public function listar($filtro = null, $colunas = null, $customWhere = null, $colunasBase = null, $order = null) {
        if (is_null($filtro) && is_null($customWhere)) {
            $select = (!is_null($colunas) && is_array($colunas)) ? $this->sql->select($this->tabela, null, null, false, $colunas, $order) : $this->sql->select($this->tabela, null, null, null, null, $order);
        } else if (!is_object($this->registro) && !is_array($this->registro) && is_null($customWhere)) {
            $dif = ($this->registro == true) ? true : false;
            $filtro = $this->resolverColunasRegistro($filtro);
            $select = (!is_null($colunas) && is_array($colunas)) ? $this->sql->select($this->tabela, $filtro->coluna, $filtro->valor, $dif, $colunas, $order) : $this->sql->select($this->tabela, $filtro->coluna, $filtro->valor, $dif, null, $order);
        } else if (is_object($this->registro) && is_null($customWhere)) {
            $whereUpSet = $this->resolverColunasRegistro($this->registro);
            $filtro = $this->resolverColunasRegistro($filtro);
            $select = $this->sql->select($this->tabela, Helpers::combinarArrays(array($whereUpSet->coluna, $filtro->coluna)), Helpers::combinarArrays(array($whereUpSet->valor, $filtro->valor)), false, null, $order, true);
        } else {
            $select = $this->sql->select($this->tabela, null, null, false, $colunasBase, $order, $customWhere);
        }
        if ($select) {
            $saida = $select;
        } else {
            $saida = array('msg' => 'Falha ao listar registros. Erro:' . $select);
        }
        return $saida;
    }

    private function removerRegistro($registro) {
        $whereDel = $this->resolverColunasRegistro($registro, null, $this->colunaCodigo);
        $deletar = $this->sql->delete($this->tabela, $whereDel->coluna, $whereDel->valor);
        if (isset($deletar['error'])) {
            throw new Exceptions($deletar['error']);
        }
        return true;
    }

    private function criarEditarRegistro($objeto, $editar = null, $multCondicao = null) {
        if (is_null($editar) && is_null($multCondicao)) {
            $valorUpSet = $this->resolverColunasRegistro($objeto);
            $executar = $this->sql->insert($this->tabela, $valorUpSet->coluna, $valorUpSet->valor, false);
        } else if (!is_null($editar) && is_null($multCondicao)) {
            $whereUpSet = $this->resolverColunasRegistro($objeto, "cor", $this->colunaCodigo);
            $valorUpSet = $this->resolverColunasRegistro($objeto, $whereUpSet->coluna);
            $executar = $this->sql->update($this->tabela, $valorUpSet->coluna, $valorUpSet->valor, false, $whereUpSet->coluna, $whereUpSet->valor);
        } else {
            $whereUpSet = $this->resolverColunasRegistro($multCondicao);
            $valorUpSet = $this->resolverColunasRegistro($objeto, $whereUpSet->coluna);
            $executar = $this->sql->update($this->tabela, $valorUpSet->coluna, $valorUpSet->valor, false, $whereUpSet->coluna, $whereUpSet->valor);
        }
        $saida = $executar;
        return $saida;
    }

    private function resolverColunasRegistro($objeto, $ignoreColuna = null, $justColuna = null) {
        $colunas = array();
        $valores = array();
        $obj = new stdClass();
        foreach ($objeto as $key => $dado) {
            if (is_array($objeto->$key)) {
                $colunas = $key;
                foreach ($objeto->$key as $v) {
                    $valores[] = $v;
                }
                break;
            }
            if (is_null($justColuna)) {
                if (!is_array($ignoreColuna) && $key != $ignoreColuna) {
                    $colunas[] = $key;
                    $valores[] = $dado;
                } else if (is_array($ignoreColuna) && !in_array($key, $ignoreColuna)) {
                    $colunas[] = $key;
                    $valores[] = $dado;
                }
            } else {
                if (strstr($key, $justColuna)) {
                    $colunas[] = $key;
                    $valores[] = $dado;
                }/* else {
                  $colunas = $key;
                  $valores = $dado;
                  break;
                  } */
            }
        }
        $obj->coluna = $colunas;
        $obj->valor = $valores;
        return $obj;
    }

    public function _truncate($sString, $iLength, $sSufix = "...", $bTitle = false) {
        if ($iLength < strlen($sString)) {
            $sNewString = substr($sString, 0, $iLength) . $sSufix;
            $sString = (!$bTitle) ? $sNewString : '<span title="' . $sString . '">' . $sNewString . '</span>';
        }
        return $sString;
    }

}
